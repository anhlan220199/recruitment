package com.project.recruitment.util;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;

import java.util.Optional;

public final class SecurityUtils {

    private SecurityUtils() {
    }

    public static Optional<String> getCurrentUserLogin() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication()).map(authentication -> {
            if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                return springSecurityUser.getUsername();
            } else if (authentication.getPrincipal() instanceof String) {
                return (String) authentication.getPrincipal();
            }
            return null;
        });
    }

    public static boolean isAuthenticated() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> authentication.getAuthorities().stream()
                        .noneMatch(grantedAuthority -> grantedAuthority.getAuthority()
                                .equals("ANONYMOUS_USER")))
                .orElse(false);
    }

    public static boolean isCurrentUserInRole(String authority) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional
                .ofNullable(securityContext.getAuthentication()).map(authentication -> authentication.getAuthorities()
                        .stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(authority)))
                .orElse(false);
    }

    // check authentication
    public static void check(Model model) {
        if (SecurityUtils.isAuthenticated()) {
            model.addAttribute("isAuthenticated", "true");
            Optional<String> currentUser = SecurityUtils.getCurrentUserLogin();
            model.addAttribute("username", currentUser.get());
            model.addAttribute("isAdmin", SecurityUtils.isCurrentUserInRole("ADMIN_SYSTEM"));
            model.addAttribute("isCandidate", SecurityUtils.isCurrentUserInRole("CANDIDATE"));
            model.addAttribute("isCompany", SecurityUtils.isCurrentUserInRole("ADMIN_COMPANY"));
        } else {
            model.addAttribute("isAuthenticated", "false");
        }
    }
}
