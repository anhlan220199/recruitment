package com.project.recruitment.config;

import com.project.recruitment.service.userdetails.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceImpl userDetailsService;

    public WebSecurityConfig(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/home").permitAll()
                .antMatchers("/job/add", "/job/edit/**").hasAnyAuthority("ADMIN_SYSTEM", "ADMIN_COMPANY")

                .antMatchers("/candidate/add").hasAnyAuthority("ADMIN_SYSTEM", "CANDIDATE")
                .antMatchers("/candidate/resume/**").hasAuthority("CANDIDATE")
                .antMatchers("/job/save/**").hasAnyAuthority("ADMIN_SYSTEM", "CANDIDATE","ADMIN_COMPANY")
                
                .antMatchers("/candidate/company/review/**").hasAnyAuthority( "CANDIDATE")
                .antMatchers("/candidate/list").hasAnyAuthority("ADMIN_SYSTEM", "ADMIN_COMPANY")
                .antMatchers("/candidate/dashboard/**").hasAuthority("CANDIDATE")
                .antMatchers("/apply/**").hasAnyAuthority( "CANDIDATE")

                .antMatchers("/company/delete/**", "/company/edit/**").hasAnyAuthority("ADMIN_SYSTEM")
                .antMatchers("/employer/dashboard/**").hasAnyAuthority("ADMIN_COMPANY")

                .antMatchers("/register/**").permitAll()
                    .and()
                .formLogin()
                .loginPage("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/")
                .failureUrl("/login?error")
                    .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET"))
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessUrl("/home")
                .permitAll()
        .and().exceptionHandling().accessDeniedPage("/403");
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
    }


    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/img/**", "/webfonts/**", "/fonts/**");
    }
}
