package com.project.recruitment.util;

import com.project.recruitment.dto.InterviewScheduleDTO;
import com.project.recruitment.dto.PostJobDTO;
import com.project.recruitment.dto.UpdateJobDTO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTimeUtils {

    public static String instantToString(Instant instantDate, String format) {
        LocalDateTime date = LocalDateTime.ofInstant(instantDate, ZoneOffset.UTC).plusHours(7);
        String formattedDate = DateTimeFormatter.ofPattern(format).format(date);
        return formattedDate;
    }

    public static LocalDateTime convertStringToDate(PostJobDTO jobDTO, String format) throws ParseException {
        DateFormat df = new SimpleDateFormat(format);
        Date date = df.parse(jobDTO.getDeadline());
        LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(),
                ZoneId.systemDefault()).plusHours(7);
        return ldt;
    }

    public static Date convertStringToDateUpdate(UpdateJobDTO updateJobDTO, String format) throws ParseException {
        DateFormat df = new SimpleDateFormat(format);
        Date date = df.parse(updateJobDTO.getDeadline());
        return date;
    }

    public static Instant convertStringToDateInterview(InterviewScheduleDTO interviewScheduleDTO, String format) throws ParseException {
        String[] strings = interviewScheduleDTO.getInterviewDate().split("T");
        String date = strings[0]+" "+strings[1];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
        return localDateTime.toInstant(OffsetDateTime.now().getOffset());
    }
}
