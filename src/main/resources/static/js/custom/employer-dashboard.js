$(window).on("load", function(){

    //Get the context of the Chart canvas element we want to select
    var jobPostedCtx = $("#job-posted-view-chart");
    var applicationCtx = $("#application-view-chart");

    // Chart Options
    var jobPostedChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom',
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Month',
                    padding: 5,
                    color: "#4ab021"
                },
                stacked: true
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Job Posted',
                    color: "#4ab021"

                },
                max: 30,
                min: 0,
                ticks: {
                    stepSize: 5
                },
                stacked: true
            }]
        },
        title: {
            display: true,
            text: 'JOB POSTED THROUGH MONTHS'
        }
    };
    var applicationChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom',
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Month',
                    padding: 5,
                    color: "#4ab021"
                },
                stacked: true
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Application Submit',
                    color: "#4ab021"

                },
                max: 200,
                min: 0,
                ticks: {
                    stepSize: 10
                },
                stacked: true
            }]
        },
        title: {
            display: true,
            text: 'APPLICATION SUBMIT THROUGH MONTHS'
        }
    };

    // Chart Data
    var jobPostedChartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "Total job posted",
            data: [10, 13, 16, 5, 8, 12, 14],
            lineTension: 0,
            fill: false,
            borderColor: "#FF7D4D",
            pointBorderColor: "#FF7D4D",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
        }]
    };
    var applicationChartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "Total application submit",
            data: [10, 20, 30, 25, 38, 19, 14],
            lineTension: 0,
            fill: false,
            borderColor: "#4ab021",
            pointBorderColor: "#4ab021",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
        }]
    };

    var jobPostedConfig = {
        type: 'line',

        // Chart Options
        options : jobPostedChartOptions,

        data : jobPostedChartData
    };
    var applicationConfig = {
        type: 'line',

        // Chart Options
        options : applicationChartOptions,

        data : applicationChartData
    };

    // Create the chart
    var jobPostedLineChart = new Chart(jobPostedCtx, jobPostedConfig);
    var applicationLineChart = new Chart(applicationCtx, applicationConfig);
});