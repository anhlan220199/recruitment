package com.project.recruitment.repository;

import com.project.recruitment.entity.CompanyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompanyTypeRepository extends JpaRepository<CompanyType, Integer> {
    Optional<List<CompanyType>> findAllByDeleteFlagIs(Integer deleteFlag);
    CompanyType findByName(String name);
    Optional<CompanyType> findByNameAndDeleteFlagIs(String name, Integer deleteFlag);
}
