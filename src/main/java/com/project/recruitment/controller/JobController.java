package com.project.recruitment.controller;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.constants.UrlConstant;
import com.project.recruitment.dto.*;
import com.project.recruitment.entity.*;
import com.project.recruitment.exception.*;
import com.project.recruitment.service.HelperService;
import com.project.recruitment.service.JobService;
import com.project.recruitment.util.SecurityUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

@Controller
@Validated
@RequestMapping("/job")
public class JobController {

    private final JobService jobService;
    private final HelperService helperService;
    private List<PositionDTO> lstPositionDTO = null;
    private List<JobType> lstJobType = null;
    private List<SkillDTO> lstSkillDTO = null;
    private List<Currency> lstCurrency = null;
    private List<Gender> lstGender = null;
    private List<JobLevelDTO> lstJobLevelDTO = null;

    public JobController(JobService jobService, HelperService helperService) {
        this.jobService = jobService;
        this.helperService = helperService;
        this.lstPositionDTO = helperService.getAllPosition();
        this.lstJobType = helperService.getAllJobType();
        this.lstSkillDTO = helperService.getAllSkill();
        this.lstCurrency = helperService.getAllCurrency();
        this.lstGender = helperService.getAllGender();
        this.lstJobLevelDTO = helperService.getAllJobLevel();
    }

    @GetMapping("/list")
    public String viewListJob(Model model,
                              @Pattern(regexp = "^-?[0-9]\\d*(\\d+)?$", message = "Page number is number only!")
                              @RequestParam(defaultValue = "1", value = "pageNumber", required = true) String pageNumber,
                              @RequestParam(value = "keyword", required = false) String keyword) throws CandidateNotFoundException {
        Page<ListJobDTO> page = jobService.listAll(keyword, pageNumber);

        model.addAttribute("page", page);
        model.addAttribute("isEmptyListJob", page.isEmpty());
        model.addAttribute("pageSize", CommonConstant.PAGE_SIZE);
        model.addAttribute("isFilter",false);
        model.addAttribute("keyword", keyword);
        model.addAttribute("listPosition", jobService.getAllJobPosition());
        model.addAttribute("listJobLevel", jobService.getAllJobLevel());
        model.addAttribute("listCity", jobService.getAllJobLocation());
        model.addAttribute("listCompany", jobService.getAllCompany());
        model.addAttribute("listSkill", jobService.getAllSkill());

        model.addAttribute("isJobSaved",false);

        model.addAttribute("listJobIdCurrentCompany", jobService.getListJobIdCurrentCompany());
        Optional<String> username = SecurityUtils.getCurrentUserLogin();
        if(!"anonymousUser".equals(username.get())) {
            model.addAttribute("currentUserRole", jobService.getCurrentUser().getRole().getName());
        } else {
            model.addAttribute("currentUserRole", "anonymous");
        }
        return "job/job-listing";
    }

    @GetMapping("/filter")
    public String filterJob(Model model,
                              @Pattern(regexp = "^-?[0-9]\\d*(\\d+)?$", message = "Page number is number only!")
                              @RequestParam(defaultValue = "1", value = "pageNumber", required = true) String pageNumber,
                              @RequestParam(value = "skill", required = true) String skill,
                              @RequestParam(value = "position", required = true) String position,
                              @RequestParam(value = "level", required = true) String jobLevel,
                              @RequestParam(value = "city", required = true) String city,
                              @RequestParam(value = "jobType", required = true) String jobType,
                              @RequestParam(value = "companyName", required = true) String companyName,
                              @RequestParam(value = "salaryMin", required = false) Integer salaryMin,
                              @RequestParam(value = "salaryMax", required = false) Integer salaryMax) throws CandidateNotFoundException {


        Page<ListJobDTO> page = jobService.filterJob(skill,jobLevel,position,city,jobType,companyName,salaryMin,salaryMax, pageNumber);
        model.addAttribute("page", page);
        model.addAttribute("isFilter",true);
        model.addAttribute("isEmptyListJob", page.isEmpty());
        model.addAttribute("pageSize", CommonConstant.PAGE_SIZE);
        model.addAttribute("listSkill", jobService.getAllSkill());
        model.addAttribute("listPosition", jobService.getAllJobPosition());
        model.addAttribute("listCity", jobService.getAllJobLocation());
        model.addAttribute("listJobLevel", jobService.getAllJobLevel());
        model.addAttribute("listCompany", jobService.getAllCompany());
        model.addAttribute("listJobType", jobService.getAllJobType());
        model.addAttribute("skill",skill);
        model.addAttribute("position",position);
        model.addAttribute("level",jobLevel);
        model.addAttribute("city",city);
        model.addAttribute("jobType",jobType);
        model.addAttribute("companyName",companyName);
        model.addAttribute("salaryMin",salaryMin);
        model.addAttribute("salaryMax",salaryMax);
        model.addAttribute("isJobSaved",false);

        Optional<String> username = SecurityUtils.getCurrentUserLogin();
        model.addAttribute("listJobIdCurrentCompany", jobService.getListJobIdCurrentCompany());
        if(!"anonymousUser".equals(username.get())) {
            model.addAttribute("currentUserRole", jobService.getCurrentUser().getRole().getName());
        } else {
            model.addAttribute("currentUserRole", "anonymous");
        }
        return "job/job-listing";
    }

    @GetMapping("/detail/{id:.+}")
    public String getJobDetailById(Model model, @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Job id is number only!") @PathVariable("id") String jobId) throws PageNotFoundException, JobNotFoundException, CandidateNotFoundException {
        JobDetailDTO jobDetail = jobService.getJobDetailById(Integer.parseInt(jobId));
        List<SimilarJobDTO> similarJobs = jobService.getSimilarJobs(jobDetail.getSkill());

        model.addAttribute("jobDetail", jobDetail);
        model.addAttribute("similarJobs", similarJobs);
        model.addAttribute("applyJobDTO", new CandidateJobDTO());
        model.addAttribute("isJobSaved",false);

        model.addAttribute("jobId", jobDetail.getJobId());
        model.addAttribute("isAuthen", SecurityUtils.isAuthenticated());

        List<UserAppliedJob> userAppliedJobList = jobService.getListUserAppliedJob(Integer.parseInt(jobId));
        boolean isListAppliedJobEmpty = false;
        if(userAppliedJobList.size() == 0){
            isListAppliedJobEmpty = true;
        }
        model.addAttribute("listUserAppliedJob", userAppliedJobList);
        model.addAttribute("isListAppliedJobEmpty", isListAppliedJobEmpty);

        model.addAttribute("listJobIdCurrentCompany", jobService.getListJobIdCurrentCompany());
        Optional<String> username = SecurityUtils.getCurrentUserLogin();
        if(!"anonymousUser".equals(username.get())) {
            model.addAttribute("currentUserRole", jobService.getCurrentUser().getRole().getName());
        } else {
            model.addAttribute("currentUserRole", "anonymous");
        }

        SecurityUtils.check(model);
        return "job/job-details";
    }

    @GetMapping("/add")
    public String toPostJob(Model model) {
        PostJobDTO newJob = new PostJobDTO();

        model.addAttribute("lstPosition", lstPositionDTO);
        model.addAttribute("lstJobType", lstJobType);
        model.addAttribute("lstSkill", lstSkillDTO);
        model.addAttribute("lstCurrency", lstCurrency);
        model.addAttribute("lstGender", lstGender);
        model.addAttribute("lstJobLevel", lstJobLevelDTO);
        model.addAttribute("newJob", newJob);
        return "job/post-job";
    }

    @PostMapping("/save")
    public String addNewJob(@Validated @ModelAttribute("newJob") PostJobDTO newJob, BindingResult bindingResult,
                            Model model) throws ParseException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("lstPosition", lstPositionDTO);
            model.addAttribute("lstJobType", lstJobType);
            model.addAttribute("lstSkill", lstSkillDTO);
            model.addAttribute("lstCurrency", lstCurrency);
            model.addAttribute("lstGender", lstGender);
            model.addAttribute("lstJobLevel", lstJobLevelDTO);
            return "job/post-job";
        }
        jobService.saveJob(newJob);
        return UrlConstant.getRedirectUrl("list?pageNumber=1&keyword=");
    }

    @GetMapping("/edit/{jobId}")
    public String toEditJob(@PathVariable("jobId") int jobId, Model model) throws Exception {
        UpdateJobDTO updateJobDTO = jobService.getUpdateJobDetailById(jobId);
        model.addAttribute("editJob", updateJobDTO);

        model.addAttribute("lstPosition", lstPositionDTO);
        model.addAttribute("lstJobType", lstJobType);
        model.addAttribute("lstSkill", lstSkillDTO);
        model.addAttribute("lstCurrency", lstCurrency);
        model.addAttribute("lstGender", lstGender);
        model.addAttribute("lstJobLevel", lstJobLevelDTO);
        return "job/update-job";
    }

    @PostMapping("/edit/{jobId}")
    public String EditJob(
            @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Job ID is number only!") @PathVariable("jobId") String jobId,
            @Validated @ModelAttribute("editJob") UpdateJobDTO editJob, BindingResult bindingResult, Model model)
            throws ParseException, JobNotFoundException, AccessDenied {
        if (bindingResult.hasErrors()) {
            List<PositionDTO> lstPositionDTO = helperService.getAllPosition();
            List<JobType> lstJobType = helperService.getAllJobType();
            List<SkillDTO> lstSkillDTO = helperService.getAllSkill();
            List<Currency> lstCurrency = helperService.getAllCurrency();
            List<Gender> lstGender = helperService.getAllGender();
            List<JobLevelDTO> lstJobLevelDTO = helperService.getAllJobLevel();

            model.addAttribute("lstPosition", lstPositionDTO);
            model.addAttribute("lstJobType", lstJobType);
            model.addAttribute("lstCurrency", lstCurrency);
            model.addAttribute("lstSkill", lstSkillDTO);
            model.addAttribute("lstGender", lstGender);
            model.addAttribute("lstJobLevel", lstJobLevelDTO);

            return "job/update-job";
        }
        jobService.editJob(editJob, jobId);
        return UrlConstant.getRedirectUrl("/job/list?pageNumber=1&keyword=");
    }

    @GetMapping("/delete/{jobId}")
    public String handleDeleteJob(
            @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Job ID is number only!") @PathVariable("jobId") String jobId) throws DeleteJobNotFoundException, AccessDenied {
        jobService.deleteJob(jobId);
        return UrlConstant.getRedirectUrl("/job/list?pageNumber=1&keyword=");
    }

    @GetMapping("/save/{jobId}")
    public String saveJob( @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Job ID is number only!") @PathVariable("jobId") String jobId,Model model) throws JobNotFoundException {
        boolean isJobSaved = jobService.saveFavoriteJob(jobId);
        if(isJobSaved){
            model.addAttribute("isJobSaved",true);
        }
        return UrlConstant.getRedirectUrl("/job/list?pageNumber=1&keyword=");
    }

    @GetMapping("/discard-save/{jobId}")
    public String discardSave( @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Job ID is number only!") @PathVariable("jobId") String jobId,Model model) throws JobNotFoundException {
        boolean isJobSaved = jobService.discardFavoriteJob(jobId);
        if(isJobSaved){
            model.addAttribute("isJobSaved",true);
        }
        return UrlConstant.getRedirectUrl("/candidate/dashboard/bookmark");
    }
}
