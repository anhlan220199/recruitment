package com.project.recruitment.dto;

import com.project.recruitment.constants.CommonConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UpdateCompanyDTO {
    private int companyId;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    private String name;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    private String street;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    private String district;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    private String city;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    private String aboutUs;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    private String type;

    private String website;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    private String hotline;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    @Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[_A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message = CommonConstant.ErrorMessage.FORMAT_ERROR)
    private String email;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    @Min(value = 1, message = CommonConstant.ErrorMessage.MIN_QUANTITY)
    private int size;

    private int deleteFlag;

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getHotline() {
        return hotline;
    }

    public void setHotline(String hotline) {
        this.hotline = hotline;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}
