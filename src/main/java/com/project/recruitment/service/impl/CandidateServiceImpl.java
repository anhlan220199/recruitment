package com.project.recruitment.service.impl;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.constants.PathConstant;
import com.project.recruitment.dto.*;
import com.project.recruitment.entity.ConfirmationToken;
import com.project.recruitment.entity.FavoriteJob;
import com.project.recruitment.entity.User;
import com.project.recruitment.entity.UserAppliedJob;
import com.project.recruitment.exception.CandidateNotFoundException;
import com.project.recruitment.exception.ListJobNotFoundException;
import com.project.recruitment.exception.ListUserNotFoundException;
import com.project.recruitment.helper.FileHelper;
import com.project.recruitment.helper.Helper;
import com.project.recruitment.repository.*;
import com.project.recruitment.service.CandidateService;
import com.project.recruitment.util.HibernateUtils;
import com.project.recruitment.util.SecurityUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CandidateServiceImpl implements CandidateService {

    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final GenderRepository genderRepository;
    private final JobTypeRepository jobTypeRepository;
    private final FavoriteJobRepository favoriteJobRepository;
    private final UserAppliedJobRepository userAppliedJobRepository;
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final Helper helper;

    public CandidateServiceImpl(UserRepository userRepository, CompanyRepository companyRepository, GenderRepository genderRepository, JobTypeRepository jobTypeRepository, FavoriteJobRepository favoriteJobRepository, UserAppliedJobRepository userAppliedJobRepository, ConfirmationTokenRepository confirmationTokenRepository, Helper helper) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.genderRepository = genderRepository;
        this.jobTypeRepository = jobTypeRepository;
        this.favoriteJobRepository = favoriteJobRepository;
        this.userAppliedJobRepository = userAppliedJobRepository;
        this.confirmationTokenRepository = confirmationTokenRepository;
        this.helper = helper;
    }

    public PasswordEncoder candidatePasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public Page<ListCandidateDTO> listAll(String keyword, String pageNumbers) throws IllegalArgumentException, ListUserNotFoundException, ListJobNotFoundException {

        Integer pageNumber = Integer.parseInt(pageNumbers);

        Page<User> pageCandidateEntity = null;
        Page<ListCandidateDTO> pageCandidateDTO = null;

        // check pageNumber
        Page<User> allCandidate = userRepository.findAllByIsCandidateIsAndDeleteFlagIs(1, 1, null);
        int sizeList = allCandidate.getSize();
        pageNumber = Helper.checkPageNumber(sizeList, pageNumber);

        // get all Candidate
        if (keyword == null || "".equals(keyword)) {

            pageCandidateEntity = userRepository.findAllByIsCandidateIsAndDeleteFlagIs(1, 1,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE, Sort.by(Sort.Direction.DESC, "userId")));

            pageCandidateDTO = helper.mapToPageCandidateDTO(pageCandidateEntity);

            // get job with keyword
        } else {
            String keywordFinal = HibernateUtils.escapeSQLLikeStatement(keyword);

            pageCandidateEntity = userRepository.relativeSearching(keywordFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCandidateDTO = helper.mapToPageCandidateDTO(pageCandidateEntity);
        }

        return pageCandidateDTO;
    }

    @Override
    public CandidateDetailDTO getCandidateDetailById(Integer userId) throws CandidateNotFoundException {
        Optional<User> user = userRepository.findByUserIdAndIsCandidateIsAndDeleteFlagIs(userId, 1,1);
        if (user.isEmpty()) {
            throw new CandidateNotFoundException();
        }
        CandidateDetailDTO candidateDetailDTO = helper.mapuserToCandidateDetailDTO(user.get());
        return candidateDetailDTO;
    }

    @Override
    public Optional<User> findUserByUsername(String username) throws CandidateNotFoundException {
        Optional<User> user = userRepository.findByUsernameAndDeleteFlagIs(username, 1);
        if (user.isEmpty()) {
            throw new CandidateNotFoundException();
        }
        return user;
    }

    @Override
    public User getCurrentUser() throws CandidateNotFoundException {
        Optional<String> currentUsername = SecurityUtils.getCurrentUserLogin();
        if (!currentUsername.isPresent()) {
            throw new CandidateNotFoundException();
        }
        Optional<User> currentUser = findUserByUsername(currentUsername.get());
        return currentUser.get();
    }

    @Override
    public UpdateCandidateDTO getCandidateToUpdateForm() throws CandidateNotFoundException {
        User userEntity = getCurrentUser();
        UpdateCandidateDTO updateCandidateDTO = helper.mapUserEntityToUpdateCandidateDTO(userEntity);
        return updateCandidateDTO;
    }

    @Override
    public ChangePasswordDTO getCandidateToChangePasswordForm() throws CandidateNotFoundException {
        User userEntity = getCurrentUser();
        ChangePasswordDTO changePasswordDTO = helper.mapUserEntityToChangePasswordDTO(userEntity);
        return changePasswordDTO;
    }

    @Override
    public boolean editProfile(UpdateCandidateDTO updateCandidateDTO) throws CandidateNotFoundException {

        User currentUser = getCurrentUser();

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(updateCandidateDTO, currentUser);

        currentUser.setGender(genderRepository.findByName(updateCandidateDTO.getGender()));
        currentUser.setDeleteFlag(1);

        userRepository.save(currentUser);
        return true;
    }

    private BindingResult checkPasswordDTO(String oldPassword, ChangePasswordDTO changePasswordDTO, BindingResult bindingResult){

        String oldPasswordDTO = changePasswordDTO.getOldPassword();
        String newPassword = changePasswordDTO.getNewPassword();
        String confirmPassword = changePasswordDTO.getConfirmPassword();

        if(newPassword.equals(oldPasswordDTO)|| newPassword.equals(oldPassword)){
            bindingResult.rejectValue("newPassword","newPassword.error","Your new password have to be different from the old one!");
        }
        if(!newPassword.equals(confirmPassword)){
            bindingResult.rejectValue("confirmPassword","confirmPassword.error","Confirm password does not match!");
        }
        if(!candidatePasswordEncoder().matches(oldPasswordDTO,oldPassword)){
            bindingResult.rejectValue("oldPassword","oldPassword.error","Your current password is incorrect!");
        }
        return bindingResult;
    }

    @Override
    public BindingResult changePassword(ChangePasswordDTO changePasswordDTO, BindingResult bindingResult) throws CandidateNotFoundException {

        User userEntity = getCurrentUser();

        String oldPassword = userEntity.getPassword();

        BindingResult isValid = checkPasswordDTO(oldPassword, changePasswordDTO, bindingResult);

        if (!isValid.hasErrors()) {
            String newPassword = changePasswordDTO.getNewPassword();
            userEntity.setPassword(candidatePasswordEncoder().encode(newPassword));
            userEntity.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().get());
            userEntity.setLastModifiedDate(new Date().toInstant());
            userEntity.setDeleteFlag(1);
            userRepository.save(userEntity);
        }
        return isValid;
    }

    @Override
    public void deleteCandidate(String id) throws CandidateNotFoundException {

        Integer candidateId = Integer.parseInt(id);
        Optional<User> userEntity = userRepository.findByUserIdAndIsCandidateIsAndDeleteFlagIs(candidateId,1, 1);

        if (userEntity.isEmpty()) {
            throw new CandidateNotFoundException();
        }
        userEntity.get().setDeleteFlag(0);
        userRepository.save(userEntity.get());
    }

    @Override
    public Integer countCandidate() {
        List<User> listUserEntity = userRepository.findAllByIsCandidateIsAndDeleteFlagIs(1, 1);
        Integer count = listUserEntity.size();
        return count;
    }

    @Override
    public List<UserAppliedJob> getListUserAppliedJobByCurrentCandidate() throws CandidateNotFoundException {
        Optional<User> candidate = userRepository.findByUsernameAndIsCandidateAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(), 1,1);
        if (candidate.isEmpty()){
            throw new CandidateNotFoundException();
        }
        List<UserAppliedJob> listUserAppliedJob = candidate.get().getListUserAppliedJob();
        return listUserAppliedJob;
    }

    @Override
    public UpdateResumeDTO getCandidateToAddResumeForm() throws CandidateNotFoundException {
        User userEntity = getCurrentUser();
        UpdateResumeDTO updateResumeDTO = helper.mapUserEntityToUpdateResumeDTO(userEntity);
        return updateResumeDTO;
    }

    @Override
    public void addResume(UpdateResumeDTO updateResumeDTO, String fileName) throws CandidateNotFoundException {

        User currentUser = getCurrentUser();

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(updateResumeDTO, currentUser);

        currentUser.setJobType(jobTypeRepository.findByName(updateResumeDTO.getJobType()).get());
        currentUser.setDeleteFlag(1);
        currentUser.setCvFileName(fileName);

        userRepository.save(currentUser);
    }

    @Override
    public boolean saveCVFile(MultipartFile multipartFile, String fileName) throws IOException {
        return FileHelper.saveFile(multipartFile, PathConstant.PATH_TO_CANDIDATE_CV, fileName);
    }

    @Override
    public boolean deleteProfile() throws CandidateNotFoundException {
        User currentUser = getCurrentUser();
        currentUser.setDeleteFlag(0);
        currentUser.setLastModifiedBy(getCurrentUser().getUsername());
        currentUser.setLastModifiedDate(new Date().toInstant());
        userRepository.save(currentUser);
        return true;
    }

    @Override
    public List<ListJobDTO> getListJobBookmark() throws CandidateNotFoundException {
        User user = getCurrentUser();
        List<FavoriteJob> listBookmarkJob = favoriteJobRepository.findAllByUserAndDeleteFlagIs(user, 1);

        List<ListJobDTO> listJobDTOs = new ArrayList<>();
        for (Integer i=0; i<listBookmarkJob.size(); i++) {
            ListJobDTO listJobDTO = helper.mapJobEntityToListJobDTO(listBookmarkJob.get(i).getJob());
            listJobDTOs.add(listJobDTO);
        }
        return listJobDTOs;
    }

    @Override
    public ResumeDetailDTO getResumeDetail() throws CandidateNotFoundException {
        User userEntity = getCurrentUser();
        ResumeDetailDTO resumeDetailDTO = helper.mapUserEntityToResumeDetailDTO(userEntity);
        return resumeDetailDTO;
    }

    @Override
    public void updateResetPasswordToken(String token, String email) throws CandidateNotFoundException {
        Optional<User> user = userRepository.findByEmailAndDeleteFlagIs(email, 1);
        if (user.isPresent()) {
            ConfirmationToken confirmationToken = new ConfirmationToken();
            confirmationToken.setUser(user.get());
            confirmationToken.setConfirmationToken(token);
            confirmationToken.setDeleteFlag(1);
            confirmationTokenRepository.save(confirmationToken);
        } else {
            throw new CandidateNotFoundException();
        }
    }

    @Override
    public User getByResetPasswordToken(String token) {
       ConfirmationToken confirmationToken = confirmationTokenRepository.findByConfirmationTokenAndDeleteFlagIs(token, 1);
        User user = confirmationToken.getUser();
        return user;
    }

    @Override
    public boolean updatePassword(User user, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        user.setPassword(encodedPassword);
        userRepository.save(user);

        return true;
    }

    @Override
    public Page<ListCandidateDTO> filterCandidateByExpertiseAndSkillAndExperienceYear(String expertise, String skill, String experienceYear, String pageNumbers) {
        Integer pageNumber = Integer.parseInt(pageNumbers);

        Page<ListCandidateDTO> pageCandidateDTO = null;
        Page<User> pageCandidateEntity = null;

        // check pageNumber
        Page<User> allCandidate = userRepository.findAllByIsCandidateIsAndDeleteFlagIs(1, 1, null);
        int sizeList = allCandidate.getSize();
        pageNumber = Helper.checkPageNumber(sizeList, pageNumber);

        // filter by expertise

        if((expertise != null || !"".equals(expertise)) && (skill==null || "".equals(skill)) && (experienceYear==null || "".equals(experienceYear)) ){
            String expertiseFinal = HibernateUtils.escapeSQLLikeStatement(expertise);

            pageCandidateEntity = userRepository.relativeSearchingByExpertise(expertiseFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCandidateDTO = helper.mapToPageCandidateDTO(pageCandidateEntity);

            //filter by skill
        }else if((expertise == null || "".equals(expertise)) && (skill!=null || !"".equals(skill)) && (experienceYear==null || "".equals(experienceYear)) ){
            String skillFinal = HibernateUtils.escapeSQLLikeStatement(skill);

            pageCandidateEntity = userRepository.relativeSearchingByProfessionalSkill(skillFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCandidateDTO = helper.mapToPageCandidateDTO(pageCandidateEntity);

            //filter by experience
        }else if((expertise == null || "".equals(expertise)) && (skill==null || "".equals(skill)) && (experienceYear!=null || !"".equals(experienceYear)) ){
            String experienceYearFinal = HibernateUtils.escapeSQLLikeStatement(experienceYear);

            pageCandidateEntity = userRepository.relativeSearchingByExperienceYear(experienceYearFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCandidateDTO = helper.mapToPageCandidateDTO(pageCandidateEntity);

            //filter by expertise and experience
        }else if((expertise != null || !"".equals(expertise)) && (experienceYear!=null || !"".equals(experienceYear)) && (skill==null || "".equals(skill)) ){
            String experienceYearFinal = HibernateUtils.escapeSQLLikeStatement(experienceYear);
            String expertiseFinal = HibernateUtils.escapeSQLLikeStatement(expertise);
            pageCandidateEntity = userRepository.relativeSearchingByExpertiseAndExperienceYear(expertiseFinal.toLowerCase(),experienceYearFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCandidateDTO = helper.mapToPageCandidateDTO(pageCandidateEntity);

            //filter by expertise, skill, experience
        } else if((expertise != null || !"".equals(expertise)) && (experienceYear!=null || !"".equals(experienceYear)) && (skill!=null || !"".equals(skill)) ){
            String expertiseFinal = HibernateUtils.escapeSQLLikeStatement(expertise);
            String skillFinal = HibernateUtils.escapeSQLLikeStatement(skill);
            String experienceYearFinal = HibernateUtils.escapeSQLLikeStatement(experienceYear);
            pageCandidateEntity = userRepository.relativeSearchingByExpertiseAndProfessionalSkillAndExperienceYear(expertiseFinal.toLowerCase(),skillFinal.toLowerCase(),experienceYearFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCandidateDTO = helper.mapToPageCandidateDTO(pageCandidateEntity);

            //get all
        }else{

            pageCandidateEntity = userRepository.findAllByIsCandidateIsAndDeleteFlagIs(1, 1,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE, Sort.by(Sort.Direction.DESC, "userId")));

            pageCandidateDTO = helper.mapToPageCandidateDTO(pageCandidateEntity);

        }

        return pageCandidateDTO;
    }

    @Override
    public List<String> getListExperienceYear() {
        return userRepository.findDistinctExperienceYear();
    }
}
