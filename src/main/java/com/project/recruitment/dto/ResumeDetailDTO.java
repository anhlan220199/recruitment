package com.project.recruitment.dto;

import com.project.recruitment.entity.Company;
import com.project.recruitment.entity.Gender;
import com.project.recruitment.entity.JobType;
import com.project.recruitment.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ResumeDetailDTO {

    private int userId;
    private String fullName;
    private String phone;
    private String email;
    private String aboutMe;
    private String expertise;
    private String address;
    private String location;
    private String experience;
    private String qualification;
    private String workExperience;
    private String educationBackground;
    private String specialQualification;
    private String professionalSkill;
    private Company company;
    private Gender gender;
    private JobType jobType;

    public int getUserId() {
        return userId;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public String getExpertise() {
        return expertise;
    }

    public String getAddress() {
        return address;
    }

    public String getLocation() {
        return location;
    }

    public String getExperience() {
        return experience;
    }

    public String getQualification() {
        return qualification;
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public String getEducationBackground() {
        return educationBackground;
    }

    public String getSpecialQualification() {
        return specialQualification;
    }

    public String getProfessionalSkill() {
        return professionalSkill;
    }

    public Company getCompany() {
        return company;
    }

    public Gender getGender() {
        return gender;
    }

    public JobType getJobType() {
        return jobType;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public void setEducationBackground(String educationBackground) {
        this.educationBackground = educationBackground;
    }

    public void setSpecialQualification(String specialQualification) {
        this.specialQualification = specialQualification;
    }

    public void setProfessionalSkill(String professionalSkill) {
        this.professionalSkill = professionalSkill;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }
}
