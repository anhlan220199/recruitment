package com.project.recruitment.service;

import com.project.recruitment.dto.CompanyCreatingDTO;
import com.project.recruitment.dto.CompanyDetailDTO;
import com.project.recruitment.dto.ListCompanyDTO;
import com.project.recruitment.dto.UpdateCompanyDTO;
import com.project.recruitment.entity.Company;
import com.project.recruitment.entity.CompanyType;
import com.project.recruitment.exception.CompanyAlreadyExistException;
import com.project.recruitment.exception.CompanyNotFoundException;
import com.project.recruitment.exception.DeleteCompanyNotFoundException;
import com.project.recruitment.exception.PageNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CompanyService {

    CompanyDetailDTO getCompanyInformationDetail(int companyId) throws PageNotFoundException;

    Page<ListCompanyDTO> listAll(String keyword, String pageNumber);

    List<ListCompanyDTO> getAllCompany();

    List<CompanyType> getAllCompanyType();

    Integer countCompany();

    void saveNewCompany(CompanyCreatingDTO companyCreatingDTO) throws CompanyAlreadyExistException;

    void deleteCompany(String companyId) throws DeleteCompanyNotFoundException;

    UpdateCompanyDTO getCompanyToUpdate(String companyId) throws CompanyNotFoundException;

    boolean updateCompanyInfo(String companyId, UpdateCompanyDTO updateCompanyDTO);

    List<String> getAllCompanyLocation();

    Page<ListCompanyDTO> filterCompanyByTypeAndCity(String city, String companyType, String pageNumber);

    Company getCompanyByCompanyId(String companyId) throws CompanyNotFoundException;

    List<CompanyDetailDTO> getTopListCompanies();


}
