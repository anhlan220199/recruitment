package com.project.recruitment.dto;

import com.project.recruitment.constants.CommonConstant.ErrorMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PostJobDTO {

    private Integer jobId;

    private Integer companyId;

    @NotNull(message = ErrorMessage.NOTNULL)
    private Integer positionId;

    @NotNull(message = ErrorMessage.NOTNULL)
    @Pattern(regexp = "^[^<>.?;:'!~%\\_@#/*\"\"`={}\\[\\]|$]+$", message = ErrorMessage.FORMAT_ERROR)
    private String city;

    @NotNull(message = ErrorMessage.NOTNULL)
    private String jobType;

    private String jobTitle;

    @NotNull(message = ErrorMessage.NOTNULL)
    private String experience;

    @NotNull(message = ErrorMessage.NOTNULL)
    private Integer skillId;

    @Size(min = 1, max = 100, message = ErrorMessage.SIZE_ERROR)
    @Pattern(regexp = "^[^<>.?;:'!~%\\_@#/*\"\"`={}\\[\\]|-]+$", message = ErrorMessage.FORMAT_ERROR)
    private String salary;

    @NotNull(message = ErrorMessage.NOTNULL)
    private String currency;

    private String requirement;

    private String description;

    private String otherBenefit;

    private String gender;

    @NotNull(message = ErrorMessage.NOTNULL)
    private Integer quantity;

    @NotNull(message = ErrorMessage.NOTNULL)
    private Integer joblevelId;

    @NotNull(message = ErrorMessage.NOTNULL)
    private String deadline;

    // Getter
    public Integer getJobId() {
        return jobId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public String getCity() {
        return city;
    }

    public String getJobType() {
        return jobType;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getExperience() {
        return experience;
    }

    public Integer getSkillId() {
        return skillId;
    }

    public String getSalary() {
        return salary;
    }

    public String getCurrency() {
        return currency;
    }

    public String getRequirement() {
        return requirement;
    }

    public String getDescription() {
        return description;
    }

    public String getOtherBenefit() {
        return otherBenefit;
    }

    public String getGender() {
        return gender;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Integer getJoblevelId() {
        return joblevelId;
    }

    public String getDeadline() {
        return deadline;
    }

    // Setter
    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public void setSkillId(Integer skillId) {
        this.skillId = skillId;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOtherBenefit(String otherBenefit) {
        this.otherBenefit = otherBenefit;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setJoblevelId(Integer joblevelId) {
        this.joblevelId = joblevelId;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }
}
