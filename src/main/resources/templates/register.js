function get() {
    var settings = {
        "dataType": "text",
        "async": true,
        "url": "http://localhost:8080/SpringMVCHello/",
        "method": "GET",
    }

    $.ajax(settings).done(function(data, status) {
        console.log(data);
    });
}

function User(name, age, phone) {
    this.name = name;
    this.age = age;
    this.phone = phone;
}

function post() {
    let user = new User("va", 21, "09999999");
    var settings = {
        "dataType": "json",
        "data": JSON.stringify(user),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        "async": true,
        "url": "http://localhost:8080/SpringMVCHello/post",
        "method": "POST",
    }

    $.ajax(settings).done(function(data, status) {
        console.log(data);
    });
}