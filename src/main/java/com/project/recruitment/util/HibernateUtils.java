package com.project.recruitment.util;

import org.thymeleaf.util.StringUtils;

public final class HibernateUtils {

    private HibernateUtils() {
    }

    public static String escapeSQLLikeStatement(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return value.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_").replaceAll("#", "\\\\#");
    }

}
