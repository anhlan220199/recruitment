package com.project.recruitment.repository;

import com.project.recruitment.entity.Company;
import com.project.recruitment.entity.Job;
import com.project.recruitment.entity.Skill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface JobRepository extends JpaRepository<Job, Integer> {

    @Query(value = "SELECT * from `job` WHERE LOWER(job_title) LIKE %?1% AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeSearching(String keyword, Pageable pageable);

    //j join `skill` sk on j.skill_id=sk.skill_id join `position` p on j.position_id=p.position_id join `job_level` jl on j.job_level_id=jl.job_level_id
    @Query(value = "SELECT * from `job` j where j.skill_id=?1 and j.position_id=?2 and j.job_level_id=?3 and LOWER(j.city) LIKE %?4% and j.job_type_id=?5 and j.company_id=?6 and (j.salary>=?7 and j.salary<=?8) and delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFiltering(Integer skillId, Integer positionId, Integer jobLevelId, String city, Integer jobTypeId, Integer companyId, Integer salaryMin, Integer salaryMax, Pageable pageable);

    @Query(value = "SELECT * from `job` j where j.skill_id=?1 AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringBySkill(Integer skillId, Pageable pageable);

    @Query(value = "SELECT * from `job` j where j.position_id=?1 AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringByPosition(Integer positionId, Pageable pageable);

    @Query(value = "SELECT * from `job` j where LOWER(j.city) LIKE %?1%  AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringByCity(String city, Pageable pageable);

    @Query(value = "SELECT * from `job` j where j.job_level_id=?1 AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringByJoblevel(Integer jobLevelId, Pageable pageable);

    @Query(value = "SELECT * from `job` j where j.job_type_id=?1  AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringByJobType(Integer jobTypeId, Pageable pageable);

    @Query(value = "SELECT * from `job` j where   j.company_id=?1 and delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringByCompany(Integer companyId, Pageable pageable);

    @Query(value = "SELECT * from `job` j where   j.skill_id=?1 AND j.position_id=?2 AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringBySkillAndPosition(Integer skillId, Integer positionId, Pageable pageable);

    @Query(value = "SELECT * from `job` j where   j.skill_id=?1 AND j.job_level_id=?2 AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringBySkillAndLevel(Integer skillId, Integer jobLevelId, Pageable pageable);

    @Query(value = "SELECT * from `job` j where   j.skill_id=?1 AND j.job_level_id=?2 AND j.position_id=?2 AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringBySkillAndLevelAndPosition(Integer skillId, Integer jobLevelId, Integer positionId, Pageable pageable);

    @Query(value = "SELECT * from `job` j where   j.skill_id=?1 AND j.company_id=?2 AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringBySkillAndCompany(Integer skillId, Integer companyId, Pageable pageable);

    @Query(value = "SELECT * from `job` j where   j.skill_id=?1 AND j.position_id=?2 AND j.company_id=?3 AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeJobFilteringBySkillAndPositionAndCompany(Integer skillId, Integer positionId, Integer companyId, Pageable pageable);

    Optional<Job> findByJobIdAndDeleteFlagIs(Integer jobId, Integer i);

    List<Job> findJobsBySkillOrderByJobIdDesc(Skill skill);

    Page<Job> findAllByDeleteFlagIs(Integer i, Pageable pageable);

    List<Job> findAllByDeleteFlagIs(Integer i);

    @Query(value = "SELECT DISTINCT city from `job` where delete_flag=1", nativeQuery = true)
    List<String> findDistinctCity();

    List<Job> findAllBySalaryGreaterThanEqualAndSalaryLessThanEqual(Integer minsalary, Integer maxSalary);

    Page<Job> findAllByCompanyAndDeleteFlagIs(Company company, Integer deleteFlag, Pageable pageable);

    @Query(value = "SELECT * from `job` WHERE LOWER(job_title) LIKE %?1% AND company_id=?2 AND delete_flag=1", nativeQuery = true)
    Page<Job> relativeSearchingByCompanyAndDeleteFlagIs(String keyword, Integer companyId, Integer deleteFlag, Pageable pageable);

    @Query(value = "SELECT * from `job` WHERE delete_flag=1 ORDER BY `job_id` DESC LIMIT 10", nativeQuery = true)
    List<Job> getListRecentJob();

    @Query(value = "SELECT * from `job` WHERE `delete_flag` = 1 ORDER BY `salary` DESC LIMIT 10", nativeQuery = true)
    List<Job> getListFeatureJob();

    @Query(value = "SELECT * FROM job j where j.company_id=?1 and month(j.created_date)=MONTH(CURRENT_DATE()) and year(j.created_date)=year(CURRENT_DATE()) and j.delete_flag=1", nativeQuery = true)
    List<Job> getListJobPostedThisMonth(Integer companyId);
}
