package com.project.recruitment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "favorite_job", schema = "recruitment_db", catalog = "")
public class FavoriteJob extends AbstractAuditingEntity {
    private int favoriteJobId;
    private int deleteFlag;
    private Job job;
    private User user;

    @Id
    @Column(name = "favorite_job_id")
    public int getFavoriteJobId() {
        return favoriteJobId;
    }

    public void setFavoriteJobId(int favoriteJobId) {
        this.favoriteJobId = favoriteJobId;
    }

    @Basic
    @Column(name = "delete_flag")
    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @ManyToOne
    @JoinColumn(name = "job_id", referencedColumnName = "job_id", nullable = false)
    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
