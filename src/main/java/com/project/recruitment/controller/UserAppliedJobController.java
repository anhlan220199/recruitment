package com.project.recruitment.controller;

import com.project.recruitment.constants.PathConstant;
import com.project.recruitment.constants.UrlConstant;
import com.project.recruitment.dto.*;
import com.project.recruitment.exception.CandidateNotFoundException;
import com.project.recruitment.exception.JobNotFoundException;
import com.project.recruitment.exception.UserAppliedJobNotFoundException;
import com.project.recruitment.service.EmailService;
import com.project.recruitment.service.UserAppliedJobService;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Pattern;
import java.io.*;
import java.text.ParseException;

@Controller
@Validated
@RequestMapping("/userAppliedJob")
public class UserAppliedJobController {
    private final UserAppliedJobService userAppliedJobService;
    private final EmailService emailService;

    public UserAppliedJobController(UserAppliedJobService userAppliedJobService, EmailService emailService) {
        this.userAppliedJobService = userAppliedJobService;
        this.emailService = emailService;
    }

    @GetMapping("/delete/{userAppliedJobId}")
    public String handleDeleteCandidate(
            @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Candidate ID is number only!") @PathVariable("userAppliedJobId") String userAppliedJobId) throws UserAppliedJobNotFoundException {
        userAppliedJobService.deleteUserAppliedJob(userAppliedJobId);
        return UrlConstant.getRedirectUrl("/employer/dashboard/candidate/manage?pageNumber=1&keyword=");
    }

    @GetMapping(value = "/downloadCV/{userAppliedJobId}")
    public void download1(@Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "userAppliedJob ID is number only!") @PathVariable("userAppliedJobId") String userAppliedJobId, HttpServletResponse response) throws FileNotFoundException, IOException {
        String cvFileName = userAppliedJobService.getAppliedCVFileName(userAppliedJobId);
        if (cvFileName!=null && !"".equals(cvFileName)){
            File file = new File(PathConstant.PATH_TO_PROJECT + PathConstant.PATH_TO_APPLY_ONLINE_CV + cvFileName);
            byte[] data = FileUtils.readFileToByteArray(file);
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + file.getName());
            response.setContentLength(data.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(data));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
        }else {
            throw new FileNotFoundException();
        }
    }

    @GetMapping("/interview/add/{userAppliedJobId}")
    public String getAddInterviewScheduleForm(
            Model model,
            @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "userAppliedJob ID is number only!") @PathVariable("userAppliedJobId") String userAppliedJobId)
            throws UserAppliedJobNotFoundException {
        InterviewScheduleDTO interviewScheduleDTO = userAppliedJobService.getUserAppliedJob(Integer.parseInt(userAppliedJobId));
        model.addAttribute("userAppliedJobDTO", interviewScheduleDTO);
        return "/company/company-add-interview-schedule";
    }

    @PostMapping("/interview/add/{userAppliedJobId}")
    public String addInterviewSchedule(
            Model model,
            @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "userAppliedJob ID is number only!") @PathVariable("userAppliedJobId") String userAppliedJobId,
            @Validated @ModelAttribute("userAppliedJobDTO") InterviewScheduleDTO interviewScheduleDTO, BindingResult bindingResult)
            throws UserAppliedJobNotFoundException, JobNotFoundException, MessagingException, CandidateNotFoundException, ParseException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("userAppliedJobDTO", userAppliedJobService.getUserAppliedJob(Integer.parseInt(userAppliedJobId)));
            return "/company/company-add-interview-schedule";
        }
        emailService.sendInterviewScheduleMail(interviewScheduleDTO);
        userAppliedJobService.addInterviewSchedule(userAppliedJobId, interviewScheduleDTO);
        return UrlConstant.getRedirectUrl("/job/detail/" + interviewScheduleDTO.getJobId());
    }
}
