
DROP DATABASE IF EXISTS `recruitment_db`;

CREATE DATABASE IF NOT EXISTS `recruitment_db`;

USE `recruitment_db`;
set @@GLOBAL.TIME_ZONE = '+07:00';
-- TABLE 1: position
DROP TABLE IF EXISTS `recruitment_db`.`position`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`position` (
  `position_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `created_date` DATETIME NOT NULL,
  `created_by` VARCHAR(200) NOT NULL,
  `last_modified_date` DATETIME NOT NULL,
  `last_modified_by` VARCHAR(200) NOT NULL,
  `delete_flag` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`position_id`));

INSERT INTO
		`position` 	(`name`, 				`created_date`, 		`created_by`, 			`last_modified_date`, 		`last_modified_by`	)
VALUES
					('Developer', 			'2020-01-01 10:10:10', 	'admin', 				'2020-01-01 10:10:10', 		'admin'				),
					('Tester', 				'2020-01-01 10:10:10', 	'admin', 				'2020-01-01 10:10:10',		'admin'				),
                    ('Project Manager', 	'2020-01-01 10:10:10', 	'admin', 				'2020-01-01 10:10:10', 		'admin'				),
                    ('Department Leader', 	'2020-01-01 10:10:10', 	'admin', 				'2020-01-01 10:10:10', 		'admin'				),
                    ('BrSE', 				'2020-01-01 10:10:10', 	'admin', 				'2020-01-01 10:10:10', 		'admin'				),
                    ('Business Analyst', 	'2020-01-01 10:10:10', 	'admin', 				'2020-01-01 10:10:10', 		'admin'				),
                    ('SA', 					'2020-01-01 10:10:10', 	'admin', 				'2020-01-01 10:10:10', 		'admin'				);

-- TABLE 2: job_type
DROP TABLE IF EXISTS `recruitment_db`.`job_type`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`job_type` (
	`job_type_id` int AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL
);

INSERT INTO `job_type` (`name`)
VALUES ('Full Time'), ('Part Time'), ('Any');

-- TABLE 3: currency
DROP TABLE IF EXISTS `recruitment_db`.`currency`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`currency` (
	`currency_id` int AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL
);

INSERT INTO `currency` (`name`)
VALUES ('VND'), ('USD');

-- TABLE 4: gender
DROP TABLE IF EXISTS `recruitment_db`.`gender`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`gender` (
	`gender_id` int AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL
);

INSERT INTO `gender` (`name`)
VALUES ('Male'), ('Female'), ('Any Gender');

-- TABLE 5: company_type
DROP TABLE IF EXISTS `recruitment_db`.`company_type`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`company_type` (
  `company_type_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `delete_flag` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`company_type_id`));
  
INSERT INTO `company_type` (`name`) VALUES ('Product'), ('Outsource');

 -- TABLE 6: skill
DROP TABLE IF EXISTS `recruitment_db`.`skill`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`skill` (
  `skill_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`skill_id`));

INSERT INTO `skill` (`name`)
VALUES ('Java'), ('PHP'), ('Python'), ('Java Script'), ('AI'), ('Angular');

 -- TABLE 7: job_level
DROP TABLE IF EXISTS `recruitment_db`.`job_level`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`job_level` (
  `job_level_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`job_level_id`));

INSERT INTO
	`job_level` (`name`)
VALUES ('Fresher'), ('Junior'), ('Senior');

 -- TABLE 8: role
DROP TABLE IF EXISTS `recruitment_db`.`role`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`role` (
  `role_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`role_id`));

  INSERT INTO `role` 	(`name`)
  VALUES ('ADMIN_SYSTEM'), ('ADMIN_COMPANY'), ('CANDIDATE'), ('ANONYMOUS_USER');

-- TABLE 9: company
DROP TABLE IF EXISTS `recruitment_db`.`company`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`company` (
	`company_id` INT NOT NULL AUTO_INCREMENT,
    
	`company_type_id` INT NOT NULL,
    
	`name` VARCHAR(200) NOT NULL,
	`street` VARCHAR(200),
	`district` VARCHAR(200),
	`city` VARCHAR(200),
	`hotline` VARCHAR(200),
    `email` VARCHAR(200),
    `website` VARCHAR(200),
	`about_us` LONGTEXT,
	`size` INT,
    `total_rating` float,
	`created_date` DATETIME NOT NULL,
	`created_by` VARCHAR(200) NOT NULL,
	`last_modified_date` DATETIME NOT NULL,
	`last_modified_by` VARCHAR(200) NOT NULL,
	`delete_flag` INT NOT NULL DEFAULT 1,
	PRIMARY KEY (`company_id`),
    FOREIGN KEY (`company_type_id`) REFERENCES company_type (`company_type_id`)
);

INSERT INTO
	`company` 	(`name`,						`street`, 					`district`,			`city`, 		`company_type_id`, 	`hotline`,		`email`, 				`about_us`,																																																																		`website`,						`size`,	`total_rating`,	`created_date`, 		`created_by`, 			`last_modified_date`, 		`last_modified_by`)
VALUES
				('Công ty TNHH Hướng Dương', 	'27 Hoàng Đạo Thúy',		' Thanh Xuân', 		'Hà Nội',			1, 				'0388402392',	'company@gmail.com',	'Nơi làm việc tốt nhất Việt Nam 2017, 2018 ngành Viễn thông (theo khảo sát Anphabe) là Doanh nghiệp viễn thông có tốc độ tăng trưởng nhanh nhất thế giới, luôn đi đầu trong đổi mới sáng tạo và luôn lắng nghe, thấu hiểu để đem tới những dịch vụ tốt nhất cho khách hàng.',	'https://us.innisfree.com/',	100,	4,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
                ('Công ty TNHH IT OCB', 		'720A Điện Biên Phủ',		'Bình Thạnh',		'TP.HCM',			2, 				'0388402392',	'company@gmail.com',	'Nơi làm việc tốt nhất Việt Nam 2017, 2018 ngành Viễn thông (theo khảo sát Anphabe) là Doanh nghiệp viễn thông có tốc độ tăng trưởng nhanh nhất thế giới, luôn đi đầu trong đổi mới sáng tạo và luôn lắng nghe, thấu hiểu để đem tới những dịch vụ tốt nhất cho khách hàng.',	'https://us.innisfree.com/',	100,	4.4,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
                ('Công ty ĐTVT', 				'6 Xuân Thủy',				'Cầu Giấy',			'Hà Nội',			1,				'0388402392',	'company@gmail.com',	'Employment Hero is one Australia’s fastest-growing SaaS companies. Our platform helps small and medium-sized businesses manage all of their HR, payroll, recruitment and employee benefit needs via one beautiful, cloud-based platform.',										'https://us.innisfree.com/',	100, 	5,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
                ('Công ty Cổ phần HTV', 		'Kim Liên',					'Đống Đa',			'Hà Nội',			2, 				'0388402392',	'company@gmail.com',	'Employment Hero is one Australia’s fastest-growing SaaS companies. Our platform helps small and medium-sized businesses manage all of their HR, payroll, recruitment and employee benefit needs via one beautiful, cloud-based platform.',										'https://us.innisfree.com/',	100,	3,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
                ('Công ty TNHH Happy', 	 		'720A Điện Biên Phủ',		'Bình Thạnh',		'TP.HCM',			1, 				'0388402392',	'company@gmail.com',	'Employment Hero is one Australia’s fastest-growing SaaS companies. Our platform helps small and medium-sized businesses manage all of their HR, payroll, recruitment and employee benefit needs via one beautiful, cloud-based platform.',										'https://us.innisfree.com/',	100,	2,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
                ('Viettel Group', 	'27 Hoàng Đạo Thúy',		' Thanh Xuân', 		'Hà Nội',			1, 				'0388402392',	'company@gmail.com',	'Nơi làm việc tốt nhất Việt Nam 2017, 2018 ngành Viễn thông (theo khảo sát Anphabe) là Doanh nghiệp viễn thông có tốc độ tăng trưởng nhanh nhất thế giới, luôn đi đầu trong đổi mới sáng tạo và luôn lắng nghe, thấu hiểu để đem tới những dịch vụ tốt nhất cho khách hàng.',	'https://us.innisfree.com/',	100,	5,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
                ('VTI Group', 		'720A Điện Biên Phủ',		'Bình Thạnh',		'TP.HCM',			2, 				'0388402392',	'company@gmail.com',	'Nơi làm việc tốt nhất Việt Nam 2017, 2018 ngành Viễn thông (theo khảo sát Anphabe) là Doanh nghiệp viễn thông có tốc độ tăng trưởng nhanh nhất thế giới, luôn đi đầu trong đổi mới sáng tạo và luôn lắng nghe, thấu hiểu để đem tới những dịch vụ tốt nhất cho khách hàng.',	'https://us.innisfree.com/',	100,	5,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
                ('TpBank', 				'6 Xuân Thủy',				'Cầu Giấy',			'Hà Nội',			1,				'0388402392',	'company@gmail.com',	'Employment Hero is one Australia’s fastest-growing SaaS companies. Our platform helps small and medium-sized businesses manage all of their HR, payroll, recruitment and employee benefit needs via one beautiful, cloud-based platform.',										'https://us.innisfree.com/',	100, 	5,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
                ('FPT Software', 		'Kim Liên',					'Đống Đa',			'Hà Nội',			2, 				'0388402392',	'company@gmail.com',	'Employment Hero is one Australia’s fastest-growing SaaS companies. Our platform helps small and medium-sized businesses manage all of their HR, payroll, recruitment and employee benefit needs via one beautiful, cloud-based platform.',										'https://us.innisfree.com/',	100,	4.5,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
                ('Open House', 	 		'720A Điện Biên Phủ',		'Bình Thạnh',		'TP.HCM',			1, 				'0388402392',	'company@gmail.com',	'Employment Hero is one Australia’s fastest-growing SaaS companies. Our platform helps small and medium-sized businesses manage all of their HR, payroll, recruitment and employee benefit needs via one beautiful, cloud-based platform.',										'https://us.innisfree.com/',	100,	5,	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu');

-- TABLE 10: user
DROP TABLE IF EXISTS `recruitment_db`.`user`;
CREATE TABLE IF NOT EXISTS`recruitment_db`.`user` (
	`user_id` INT NOT NULL AUTO_INCREMENT,

	`company_id` INT,
	`job_type_id` INT,
    `role_id` INT,
	`gender_id` INT,

	`username` VARCHAR(200) NOT NULL,
	`password` VARCHAR(200) NOT NULL,
    `is_candidate` INT NOT NULL DEFAULT 0,
	`full_name` VARCHAR(200),
	`phone` VARCHAR(200),
	`cv_file_name` VARCHAR(200),
	`cv_file_path` VARCHAR(200),
    `email` VARCHAR(200),
    `about_me` VARCHAR(500),
    `expertise` VARCHAR(200),
    `address` VARCHAR(200),
	`location` VARCHAR(200),
	`experience` VARCHAR(200),
    `qualification` VARCHAR(200),
    `work_experience` VARCHAR(500),
    `education_background` VARCHAR(500),
    `special_qualification` VARCHAR(500),
    `professional_skill` VARCHAR(500),
	`created_date` DATETIME NOT NULL,
	`created_by` VARCHAR(200) NOT NULL,
	`last_modified_date` DATETIME NOT NULL,
	`last_modified_by` VARCHAR(200) NOT NULL,
	`delete_flag` INT NOT NULL DEFAULT 1,

    `reset_password_token` VARCHAR(30),

	PRIMARY KEY (`user_id`),
	FOREIGN KEY (`gender_id`) REFERENCES `gender` (`gender_id`),
    FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
    FOREIGN KEY (`job_type_id`) REFERENCES `job_type` (`job_type_id`),
    FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
);

  INSERT INTO
	`user` 	(`username`, 	`company_id`, 	`password`, 													`is_candidate`,		`full_name`, 			`phone`, 		`cv_file_name`, 	`cv_file_path`, 		`email`, 						`about_me`,																															`expertise`, 	`address`, 						`job_type_id`,	 `role_id`, 	`location`, 	`experience`, 	`gender_id`, 	`qualification`, 	`work_experience`, 																																	`education_background`, 																															`special_qualification`,									`professional_skill`,	`created_date`, 		`created_by`, 			`last_modified_date`, 		`last_modified_by`)
  VALUES
			('admin', 		null,  			'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS',		0,		 		'Admin', 				'0123123321', 		null, 				null, 				'thao@gmail.com', 				null, 																																null, 			null, 								null, 			1, 			null, 			null, 			null, 			null,				null,																																				null,																																				null,														null,					'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
            
            ('lannt', 		2, 				'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	0,				'Nguyen Thi Lan', 		'0123823321', 		null, 				null, 				'lanmeo220199@gmail.com',		null, 																																null, 			null,								null, 			2,			null,			null, 			null, 			null, 				null,																																				null,																																				null,														null,					'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
			('huongdt', 	1, 				'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	0,				'Do Thu Huong', 		'0123823321', 		null, 				null, 				'dothuhuong@gmail.com',			null, 																																null, 			null,								null, 			2,			null,			null, 			null, 			null, 				null,																																				null,																																				null,														null,					'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
            ('mynlh', 		3, 				'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	0,				'Nguyen Le Hoang My', 	'0123823321', 		null, 				null, 				'mynguyen@gmail.com',			null, 																																null, 			null,								null, 			2,			null,			null, 			null, 			null, 				null,																																				null,																																				null,														null,					'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
            ('diephn', 		4, 				'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	0,				'Ha Ngoc Diep', 		'0123823321', 		null, 				null, 				'hangocdiep@gmail.com',			null, 																																null, 			null,								null, 			2,			null,			null, 			null, 			null, 				null,																																				null,																																				null,														null,					'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
            ('vinhpt', 		5, 				'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	0,				'Pham The Vinh', 		'0123823321', 		null, 				null, 				'phamthevinh@gmail.com',		null, 																																null, 			null,								null, 			2,			null,			null, 			null, 			null, 				null,																																				null,																																				null,														null,					'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
           
			('mynh', 		null,  			'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	1,				'Ngo Huyen My', 		'0123123323', 		null, 				null, 				'ngohuyenmy@gmail.com', 		'Combined with a handful of model sentence structures, to generate lorem Ipsum which  It has survived not only five centuries', 	'Tester',		'10 Duy Tan, Cau Giay, Ha Noi',		1, 				3, 			'Ha Noi',		'1 year', 		1, 				'Graduated', 		'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage', 	'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage',	'5 years+ experience designing and building products.',		'Java, PHP, MySQL',		'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
            ('trangct', 	null,  			'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	1,				'Cao Thi Trang', 		'0123123324', 		null, 				null, 				'caothitrang@gmail.com', 		'Combined with a handful of model sentence structures, to generate lorem Ipsum which  It has survived not only five centuries', 	'Tester',		'7 Minh Khai, HBT, Ha Noi',			2, 				3, 			'Ha Noi',		'2 years', 		1, 				'Graduated', 		'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage', 	'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage',	'5 years+ experience designing and building products.',		'Java, PHP, MySQL',		'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
            ('nhungnth', 	null,  			'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	1,				'Nguyen Hong Nhung', 	'0123123325', 		null, 				null, 				'nguyenhongnhung@gmail.com', 	'Combined with a handful of model sentence structures, to generate lorem Ipsum which  It has survived not only five centuries', 	'BrSE',		'62 Linh Nam, Hoang Mai, Ha Noi',	1, 				3, 			'Ha Noi',		'1 year', 		1, 				'Graduated', 		'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage', 	'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage',	'5 years+ experience designing and building products.',		'Java, PHP, MySQL',		'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
            ('huongctm', 	null,  			'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	1,				'Cao Mai Huong', 		'0123123326', 		null, 				null, 				'caomaihuong@gmail.com', 		'Combined with a handful of model sentence structures, to generate lorem Ipsum which  It has survived not only five centuries', 	'Developer',		'122 Tran Dai Nghia, HBT, Ha Noi',	2, 				3, 			'Ha Noi',		'3 years', 		2, 				'Graduated', 		'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage', 	'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage',	'5 years+ experience designing and building products.',		'Python, PHP, MySQL',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
			('tienhv', 		null,  			'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	1,				'Ha Van Tien', 			'0123123327', 		null, 				null, 				'havantien@gmail.com', 			'Combined with a handful of model sentence structures, to generate lorem Ipsum which  It has survived not only five centuries', 	'Developer',		'10 Le Thanh Nghi, HBT, Ha Noi',	1, 				3, 			'Ha Noi',		'1 year', 		1, 				'Graduated', 		'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage', 	'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage',	'5 years+ experience designing and building products.',		'Java, PHP, MySQL',		'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
			('anhnv', 		null,  			'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	1,				'Nguyen Viet Anh', 		'0123123328', 		null, 				null, 				'menbinh74@gmail.com', 			'Combined with a handful of model sentence structures, to generate lorem Ipsum which  It has survived not only five centuries', 	'Project Manager',		'232 Giai Phong, Cau Giay, Ha Noi',	2, 				3, 			'Ha Noi',		'4 years', 		2, 				'Graduated', 		'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage', 	'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage',	'5 years+ experience designing and building products.',		'JS, PHP, MySQL',		'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
			('thaoctt', 	null,  			'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	1,				'Cao Thi Thu Thao', 	'0123123329', 		null, 				null, 				'thaoctt0809@gmail.com', 		'Combined with a handful of model sentence structures, to generate lorem Ipsum which  It has survived not only five centuries', 	'Developer',		'22 Duy Tan, Cau Giay, Ha Noi',		1, 				3, 			'Ha Noi',		'1 year', 		1, 				'Graduated', 		'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage', 	'Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage',	'5 years+ experience designing and building products.',		'Java, PHP, MySQL',		'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu'),
			
            ('duongvlt', 	null,  			'$2a$10$HUGIwxanKDIeIBn8O2aE1O8fUQoNiEi0ql3M.KC6wiND2By4gZKpS', 	0,				'Vu Le Thuy Duong', 	'0123823321', 		null, 				null, 				'vulethuyduong@gmail.com',		null, 																																null, 			null, 								null,			4,			null, 			null, 			null, 			null,				null,																																				null,																																				null,														null,					'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu');

    -- TABLE 11: job
DROP TABLE IF EXISTS `recruitment_db`.`job`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`job` (
	`job_id` INT NOT NULL AUTO_INCREMENT,

	`user_id` INT,
	`company_id`INT NOT NULL,
	`position_id`INT NOT NULL,
	`gender_id` INT NOT NULL,
    `skill_id` INT NOT NULL,
    `job_type_id` INT NOT NULL,
    `currency_id` INT NOT NULL,
    `job_level_id` INT NOT NULL,

	`city` VARCHAR(200),
	`job_title` VARCHAR(200),
	`experience`VARCHAR(200),
    `salary` INT,
	`requirement` LONGTEXT,
	`description` LONGTEXT,
	`other_benefit` LONGTEXT,
	`quantity` INT,
	`deadline` DATE,
	`created_date` DATETIME,
	`created_by` VARCHAR(200),
	`last_modified_date` DATETIME,
	`last_modified_by` VARCHAR(200),
	`delete_flag` INT NOT NULL DEFAULT 1,
	FOREIGN KEY (`user_id`) REFERENCES `user`(`user_id`),
	FOREIGN KEY (`company_id`) REFERENCES `company`(`company_id`),
	FOREIGN KEY (`position_id`) REFERENCES `position`(`position_id`),
	FOREIGN KEY (`skill_id`) REFERENCES `skill`(`skill_id`),
	FOREIGN KEY (`job_level_id`) REFERENCES `job_level`(`job_level_id`),
    FOREIGN KEY (`gender_id`) REFERENCES `gender` (`gender_id`),
    FOREIGN KEY (`job_type_id`) REFERENCES `job_type` (`job_type_id`),
	FOREIGN KEY (`currency_id`) REFERENCES `currency` (`currency_id`),
	PRIMARY KEY (`job_id`));
   INSERT INTO
	`job` 	(`company_id`, `position_id`, 	`city`,		`job_type_id`, `job_title`, 							`experience`, 					`skill_id`, 	`salary`, `currency_id`, 	`requirement`, 																						`description`, 																																								`other_benefit`, 					`gender_id`,	`quantity`,		`job_level_id`, `deadline`, 	`created_date`,			`created_by`,			`last_modified_date`,		`last_modified_by`, `delete_flag`	)
  VALUES
			(1,				1 ,				'Hanoi', 		1, 			'Fresher Java Developer', 				'khong doi hoi kinh nghiem', 		1, 			150, 		 1, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		1,				3,				1,			'2021-03-01', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (2,				2 ,				'Hanoi', 		1, 			'Fresher PHP Tester', 					'1 nam kinh nghiem', 				2, 			350, 		 1, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		1,				5,				1,			'2021-03-02', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (2,				3 ,				'Da Nang', 		1, 			'Fresher Python Project Manager', 		'1 nam kinh nghiem', 				3, 			350, 		 1, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		1,				5,				1,			'2021-03-02', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (3,				4 ,				'Hanoi', 		1, 			'Fresher Java Script Department Leader','3 nam kinh nghiem', 				4, 			500, 		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				1,			'2021-03-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (4,				5 ,				'TP HCM', 		1, 			'Fresher AI BrSE',						'3 nam kinh nghiem',				5, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				1,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),

            (1,				6 ,				'Hanoi', 		1, 			'Junior Angular Business Analyst', 		'3 nam kinh nghiem', 				6, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				2,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (2,				7 ,				'TP HCM', 		1, 			'Junior Java SA', 						'3 nam kinh nghiem', 				1, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				2,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (3,				1 ,				'Hanoi', 		1, 			'Junior PHP Script Developer',			'3 nam kinh nghiem', 				2, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		1,				10,				2,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (4,				2 ,				'Hanoi', 		1, 			'Junior Python Script Tester', 			'3 nam kinh nghiem', 				3, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		1,				10,				2,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (4,				3 ,				'TP HCM', 		1, 			'Junior Java Script Project Manager',	'1 nam kinh nghiem', 				4, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		1,				10,				2,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),

			(1,				4 ,				'Hanoi', 		1, 			'Senior AI Department Leader', 			'3 nam kinh nghiem', 				5, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (3,				5 ,				'Hai Phong', 	2, 			'Senior Angular BrSE', 					'3 nam kinh nghiem', 				6, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (1,				6 ,				'Hanoi', 		2, 			'Senior Java Business Analyst', 		'3 nam kinh nghiem', 				1, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
			(1,				7 ,				'Hanoi', 		2, 			'Senior PHP SA', 						'3 nam kinh nghiem', 				2, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
            (1,				1 ,				'Hanoi', 		2, 			'Senior Python Developer', 				'3 nam kinh nghiem', 				3, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 			'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),

            (2,				4 ,				'Hanoi', 		1, 			'Senior AI Department Leader', 			'3 nam kinh nghiem', 				5, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt', 		'2021-04-01 10:10:10', 		'lannt',		1			),
            (2,				5 ,				'Hai Phong', 	2, 			'Senior Angular BrSE', 					'3 nam kinh nghiem', 				6, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt', 		'2021-04-01 10:10:10', 		'lannt',		1			),
            (2,				6 ,				'Hanoi', 		2, 			'Senior Java Business Analyst', 		'3 nam kinh nghiem', 				1, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt', 		'2021-04-01 10:10:10', 		'lannt',		1			),
			(2,				7 ,				'Hanoi', 		2, 			'Senior PHP SA', 						'3 nam kinh nghiem', 				2, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt', 		'2021-04-01 10:10:10', 		'lannt',		1			),
            (2,				1 ,				'Hanoi', 		2, 			'Senior Python Developer', 				'3 nam kinh nghiem', 				3, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 			'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt', 		'2021-04-01 10:10:10', 		'lannt',		1			),

            (2,				4 ,				'Hanoi', 		1, 			'Senior AI Department Leader', 			'3 nam kinh nghiem', 				5, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt', 		'2021-04-01 10:10:10', 	'lannt',		1			),
            (2,				5 ,				'Hai Phong', 	2, 			'Senior Angular BrSE', 					'3 nam kinh nghiem', 				6, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt', 		'2021-04-01 10:10:10', 	'lannt',		1			),
            (2,				6 ,				'Hanoi', 		2, 			'Senior Java Business Analyst', 		'3 nam kinh nghiem', 				1, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt', 		'2021-04-01 10:10:10', 	'lannt',		1			),
			(2,				7 ,				'Hanoi', 		2, 			'Senior PHP SA', 						'3 nam kinh nghiem', 				2, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt', 		'2021-04-01 10:10:10', 	'lannt',		1			),
            (2,				1 ,				'Hanoi', 		2, 			'Senior Python Developer', 				'3 nam kinh nghiem', 				3, 			1000,		 2, 			'Proven hands-on Software Development experience Proven working experience in Java development',	'Knowledge of other coding-languages (PHP, Python, Java, etc).Ability to write clean, easy-to-understand code.Outstanding analytical and problem-solving capabilities.', 	'du lịch hàng năm, hỗ trợ nhà ở', 		2,				10,				3,			'2021-05-03', 	'2021-04-01 10:10:10', 	'lannt',		'2021-04-01 10:10:10', 	'lannt',		1			);

-- TABLE 12: review
DROP TABLE IF EXISTS `recruitment_db`.`review`;
   CREATE TABLE if not exists `recruitment_db`.`review` (
    `review_id` INT NOT NULL AUTO_INCREMENT,

  `company_id` INT NOT NULL,
   `user_id` INT NOT NULL,

    `title` VARCHAR(200) NOT NULL ,
	`satisfaction` VARCHAR(1000),
    `improvement` VARCHAR(1000),
	`rating` INT,
	`created_date` DATETIME NOT NULL,
	`created_by` VARCHAR(200) NOT NULL,
	`last_modified_date` DATETIME NOT NULL,
	`last_modified_by` VARCHAR(200) NOT NULL,
	`delete_flag` INT NOT NULL DEFAULT 1,
	FOREIGN KEY (user_id) REFERENCES `user`(user_id),
	FOREIGN KEY (company_id) REFERENCES `company`(company_id),
	PRIMARY KEY (`review_id`));

   INSERT INTO
	`review` 	(`company_id`, `user_id`, 	`title`,											`satisfaction`,																																																																																																						 `improvement`,																																																																																																						`rating`,	`created_date`, 		`created_by`, 			`last_modified_date`, 		`last_modified_by`, `delete_flag`	)
  VALUES
				(	1, 			3, 			'good environment to develop your coding skill', 	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	5,			'2020-01-01 10:10:10', 	'mynh', 				'2020-01-01 10:10:10', 			'mynh',				1			),
                (	2, 			3,			'good company',										'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	4,			'2020-01-01 10:10:10', 	'mynh', 				'2020-01-01 10:10:10', 			'mynh',				1			),
                (	2, 			4, 			'good environment to develop your coding skill',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	5,			'2020-01-01 10:10:10', 	'trangct', 				'2020-01-01 10:10:10', 			'trangct',			1			),
                (	2, 			5,			'good company',										'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	4,			'2020-01-01 10:10:10', 	'nhungnth', 			'2020-01-01 10:10:10', 			'nhungnth',			1			),
                (	2, 			6, 			'good environment to develop your coding skill',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	5,			'2020-01-01 10:10:10', 	'huongctm', 			'2020-01-01 10:10:10', 			'huongctm',			1			),
                (	2, 			7,			'good company',										'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	4,			'2020-01-01 10:10:10', 	'tienhv', 				'2020-01-01 10:10:10', 			'tienhv',			1			),
                (	2, 			8, 			'good environment to develop your coding skill',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	5,			'2020-01-01 10:10:10', 	'muoimeo', 				'2020-01-01 10:10:10', 			'muoimeo',			1			),
                (	2, 			9,			'good company',										'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	'Many long-range studies have demonstrated that companies with satisfied employees routinely increase their profits on a year-over-year basis. Various employee satisfaction surveys are released each year, and often, these appear in articles with titles such as "The 100 Best Companies to Work For." Companies that appear on these lists are often the most profitable companies that are offering their services today. ',	4,			'2020-01-01 10:10:10', 	'thaoctt', 				'2020-01-01 10:10:10', 			'thaoctt',			1			);
  -- TABLE 13: favorite_job
DROP TABLE IF EXISTS `recruitment_db`.`favorite_job`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`favorite_job` (
	`favorite_job_id` INT NOT NULL AUTO_INCREMENT,

	`user_id` INT NOT NULL,
	`job_id` INT NOT NULL,

	`created_date` DATETIME NOT NULL,
	`created_by` VARCHAR(200) NOT NULL,
	`last_modified_date` DATETIME NOT NULL,
	`last_modified_by` VARCHAR(200) NOT NULL,
	`delete_flag` INT NOT NULL default 1,
	FOREIGN KEY (`user_id`) REFERENCES `user`(`user_id`),
	FOREIGN KEY (`job_id`) REFERENCES `job`(`job_id`),
	PRIMARY KEY (`favorite_job_id`));

  INSERT INTO
	`favorite_job` 	(`user_id`, 	`job_id`,	`created_date`, 		`created_by`, 	`last_modified_date`, 		`last_modified_by`, `delete_flag`	)
  VALUES
					(3, 			1,			'2020-01-01 10:10:10', 	'admin', 		'2020-01-01 10:10:10', 		'admin',				1			),
					(3, 			2,			'2020-01-01 10:10:10', 	'admin', 		'2020-01-01 10:10:10', 		'admin',				1			),
					(3, 			3,			'2020-01-01 10:10:10', 	'admin', 		'2020-01-01 10:10:10', 		'admin',				1			),
					(3, 			4,			'2020-01-01 10:10:10', 	'admin', 		'2020-01-01 10:10:10', 		'admin',				1			),
					(3, 			5,			'2020-01-01 10:10:10', 	'admin', 		'2020-01-01 10:10:10', 		'admin',				1			),
					(3, 			6,			'2020-01-01 10:10:10', 	'admin', 		'2020-01-01 10:10:10', 		'admin',				1			),
		
					(4, 			2,			'2020-01-01 10:10:10', 	'admin', 		'2020-01-01 10:10:10', 		'admin',				1			),
					(4, 			1,			'2020-01-01 10:10:10', 	'admin', 		'2020-01-01 10:10:10', 		'admin',				1			);

-- TABLE 14: user_applied_job
DROP TABLE IF EXISTS `recruitment_db`.`user_applied_job`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`user_applied_job` (
	`user_applied_job_id` INT NOT NULL AUTO_INCREMENT,

	`user_id` INT NOT NULL,
    `job_id` INT NOT NULL,

	`email` VARCHAR(200),
	`message` VARCHAR(200),
	`file_cv_path` VARCHAR(200),
	`applied_date` DATETIME,

    `interview_date` DATETIME,
    `interview_location` VARCHAR(200),

	`created_date` DATETIME NOT NULL,
	`created_by` VARCHAR(200) NOT NULL,
	`last_modified_date` DATETIME NOT NULL,
	`last_modified_by` VARCHAR(200) NOT NULL,
	`delete_flag` INT NOT NULL default 1,
	FOREIGN KEY (`user_id`) REFERENCES `user`(`user_id`),
	FOREIGN KEY (`job_id`) REFERENCES `job`(`job_id`),
	PRIMARY KEY (`user_applied_job_id`));

 INSERT INTO
	`user_applied_job` 	(`user_id`, 	`job_id`, 	`email`,					`applied_date`,			`created_date`, 		`created_by`, 			`last_modified_date`, 		`last_modified_by`, `delete_flag`	)
  VALUES
						(3, 				2,		'lanwiki220199@gmail.com',	'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
						(3, 				3,		'lanwiki220199@gmail.com',	'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
						(4, 				2,		'trangct@gmail.com',		'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
                        (5, 				1,		'nhungnth@gmail.com',		'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
                        (6, 				2,		'huongctm@gmail.com',		'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
                        (7, 				2,		'tienhv@gmail.com',			'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
                        (8, 				1,		'muoimeo@gmail.com',		'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
                        (8, 				2,		'muoimeo@gmail.com',		'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
						(5, 				2,		'nhungnth@gmail.com',		'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			),
                        (9, 				2,		'thaoctt@gmail.com',		'2021-01-01 10:10:10',	'2020-01-01 10:10:10', 	'thao.caothithu', 		'2020-01-01 10:10:10', 		'thao.caothithu',		1			);
 -- TABLE 14:
DROP TABLE IF EXISTS `recruitment_db`.`confirmation_token`;
CREATE TABLE IF NOT EXISTS `recruitment_db`.`confirmation_token` (
	`token_id` INT NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`confirmation_token` VARCHAR(200),
	`created_date` DATETIME NOT NULL,
	`created_by` VARCHAR(200) NOT NULL,
	`last_modified_date` DATETIME NOT NULL,
	`last_modified_by` VARCHAR(200) NOT NULL,
	`delete_flag` INT NOT NULL default 1,
	FOREIGN KEY (`user_id`) REFERENCES `user`(`user_id`),
	PRIMARY KEY (`token_id`));




