package com.project.recruitment.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class PositionDTO {

    private int positionId;
    private String name;

    public int getPositionId() {
        return positionId;
    }

    public String getName() {
        return name;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public void setName(String name) {
        this.name = name;
    }
}
