package com.project.recruitment.service;

import com.project.recruitment.dto.DashboardCandidateDTO;
import com.project.recruitment.dto.DashboardJobDTO;
import com.project.recruitment.dto.EmployerProfileEditDTO;
import com.project.recruitment.exception.EmployerNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;

public interface EmployerService {
    EmployerProfileEditDTO getEmployerProfileToEdit() throws EmployerNotFoundException;
    boolean editEmployerProfile(String employerId, EmployerProfileEditDTO employerProfileEditDTO)throws EmployerNotFoundException;
    BindingResult changePassword(EmployerProfileEditDTO employerProfileEditDTO,BindingResult bindingResult)throws EmployerNotFoundException;
    Page<DashboardJobDTO> listAllJob(String keyword, String pageNumber);
    Page<DashboardCandidateDTO> listAllAppliedCandidate(String keyword, String pageNumber);
    boolean deleteMyAccount() throws EmployerNotFoundException;
}
