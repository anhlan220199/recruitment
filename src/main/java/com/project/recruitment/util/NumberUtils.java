package com.project.recruitment.util;

public class NumberUtils {

    @SuppressWarnings("unused")
    public static boolean isNumber(String strNumber) {
        if (strNumber == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(strNumber);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
