package com.project.recruitment.helper;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.dto.*;
import com.project.recruitment.entity.*;
import com.project.recruitment.util.DateTimeUtils;
import org.modelmapper.ModelMapper;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@Service
public class Helper {

    private static final ModelMapper modelMapper = new ModelMapper();

    public static Integer checkPageNumber(int sizeList, Integer pageNumber) {
        if (sizeList > 0) {
            if (sizeList % CommonConstant.PAGE_SIZE == 0) {
                if (pageNumber > (sizeList / CommonConstant.PAGE_SIZE)) {
                    pageNumber = sizeList / CommonConstant.PAGE_SIZE;
                } else if (pageNumber <= 0) {
                    pageNumber = 1;
                }
            } else {
                if (pageNumber > ((sizeList / CommonConstant.PAGE_SIZE) + 1)) {
                    pageNumber = (sizeList / CommonConstant.PAGE_SIZE) + 1;
                } else if (pageNumber <= 0) {
                    pageNumber = 1;
                }
            }
        }
        return pageNumber;
    }

    // The getSiteURL() method returns the application’s URL which can be used in production
    public static String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }

    // Map Job -> ListJobDTO
    public ListJobDTO mapJobEntityToListJobDTO(Job jobEntity) {
        String deadlineApplyJob = DateTimeUtils.instantToString(jobEntity.getDeadline(), CommonConstant.FORMAT_DATE);
        ListJobDTO listJobDTO = modelMapper.map(jobEntity, ListJobDTO.class);
        listJobDTO.setDeadline(deadlineApplyJob);
        return listJobDTO;
    }

    // Map User -> ListCandidateDTO
    public ListCandidateDTO mapToListCandidateDTO(User userEntity) {
        ListCandidateDTO listCandidateDTO = modelMapper.map(userEntity, ListCandidateDTO.class);
        return listCandidateDTO;
    }

    // Map a page Job entity to a page<ListJobDTO>
    public Page<ListJobDTO> mapToPageJobDTO(Page<Job> pageJobEntity) {
        final Page<ListJobDTO> contactDtoPage = pageJobEntity.map(this::mapJobEntityToListJobDTO);
        return contactDtoPage;
    }

    // Map a page User entity to a page<ListCandidateDTO>
    public Page<ListCandidateDTO> mapToPageCandidateDTO(Page<User> pageUserEntity) {
        final Page<ListCandidateDTO> contactDtoPage = pageUserEntity.map(this::mapToListCandidateDTO);
        return contactDtoPage;
    }

    public static JobDetailDTO mapToJobDetailDTO(Job job) {
        JobDetailDTO jobDetailDTO = modelMapper.map(job, JobDetailDTO.class);
        return jobDetailDTO;
    }

    // Map Position -> PositionDTO
    public PositionDTO mapToPositionDTO (Position positionEntity) {
        PositionDTO positionDTO = modelMapper.map(positionEntity, PositionDTO.class);
        return positionDTO;
    }

    // Map Skill -> SkillDTO
    public SkillDTO mapToSkillDTO (Skill skillEntity) {
        SkillDTO skillDTO = modelMapper.map(skillEntity, SkillDTO.class);
        return skillDTO;
    }

    // Map JobLevel -> JobLevelDTO
    public JobLevelDTO mapToJobLevelDTO (JobLevel jobLevelEntity) {
        JobLevelDTO jobLevelDTO = modelMapper.map(jobLevelEntity, JobLevelDTO.class);
        return jobLevelDTO;
    }

    public static CompanyDetailDTO mapToCompanyDetailDTO(Company company) {
        int openJobNumber = company.getListJob().size();
        CompanyDetailDTO companyDetailDTO = modelMapper.map(company, CompanyDetailDTO.class);
        companyDetailDTO.setOpenJobNumber(openJobNumber);
        companyDetailDTO.setType(company.getCompanyType().getName());
        return companyDetailDTO;
    }

    public static SimilarJobDTO mapToSimilarJobDTO(Job job) {
        SimilarJobDTO similarJobDTO = modelMapper.map(job, SimilarJobDTO.class);
        similarJobDTO.setJobType(job.getJobType().getName());
        return similarJobDTO;
    }

    public static Job mapToEntity(PostJobDTO jobDTO) {
        Job jobEntity = modelMapper.map(jobDTO, Job.class);
        return jobEntity;
    }

    // map UpdateJobDTO --> Entity
    public static UpdateJobDTO mapJobToUpdateJobDTO(Job job) {
        UpdateJobDTO updateJobDTO = modelMapper.map(job, UpdateJobDTO.class);
        return updateJobDTO;
    }

    // map UpdateJobDTO --> Entity
    public static CandidateDetailDTO mapuserToCandidateDetailDTO(User user) {
        CandidateDetailDTO candidateDetailDTO = modelMapper.map(user, CandidateDetailDTO.class);
        if(user.getGender()!=null){
            candidateDetailDTO.setGender(user.getGender().getName());
        }
        if(user.getJobType()!=null){
            candidateDetailDTO.setJobType(user.getJobType().getName());
        }
        return candidateDetailDTO;
    }

    // map entity --> CandidateDetailsDTO
    public static User mapCandidateDetailDTOtoUser(CandidateDetailDTO candidateDetailDTO) {
        User user = modelMapper.map(candidateDetailDTO, User.class);
        return user;
    }

    // map UpdateJobDTO --> Entity
    public static Job mapUpdateJobDTOToEntity(UpdateJobDTO updateJobDTO) {
        Job jobEntity = modelMapper.map(updateJobDTO, Job.class);
        return jobEntity;
    }

    // Map a page Company entity to a page<ListCompanyDTO>
    public Page<ListCompanyDTO> mapToPageCompanyDTO(Page<Company> pageCompanyEntity) {
        final Page<ListCompanyDTO> contactDtoPage = pageCompanyEntity.map(this::mapToListCompanyDTO);
        return contactDtoPage;
    }

    // Map Company -> ListCompanyDTO
    public ListCompanyDTO mapToListCompanyDTO(Company companyEntity) {
        int openJobNumber = companyEntity.getListJob().size();
        ListCompanyDTO listCompanyDTO = modelMapper.map(companyEntity, ListCompanyDTO.class);
        listCompanyDTO.setOpenJobNumber(openJobNumber);
        listCompanyDTO.setType(companyEntity.getCompanyType().getName());
        return listCompanyDTO;
    }
    // map: userEntity -> updateCandidateDTO
    public UpdateCandidateDTO mapUserEntityToUpdateCandidateDTO(User user) {
        UpdateCandidateDTO updateCandidateDTO = modelMapper.map(user, UpdateCandidateDTO.class);
        return updateCandidateDTO;
    }

    // map: userEntity -> ChangePasswordDTO
    public ChangePasswordDTO mapUserEntityToChangePasswordDTO(User user) {
        ChangePasswordDTO changePasswordDTO = modelMapper.map(user, ChangePasswordDTO.class);
        return changePasswordDTO;
    }

    // map: userEntity -> updateCandidateDTO
    public UpdateResumeDTO mapUserEntityToUpdateResumeDTO(User user) {
        UpdateResumeDTO updateResumeDTO = modelMapper.map(user, UpdateResumeDTO.class);
        return updateResumeDTO;
    }

    // map: userEntity -> resumeDetailDTO
    public ResumeDetailDTO mapUserEntityToResumeDetailDTO(User user) {
        ResumeDetailDTO resumeDetailDTO = modelMapper.map(user, ResumeDetailDTO.class);
        return resumeDetailDTO;
    }

    //Map CompanyCreatingDTO --> Company
    public static Company mapToCompanyEntity(CompanyCreatingDTO companyCreatingDTO){
        Company company = modelMapper.map(companyCreatingDTO, Company.class);
        company.setDeleteFlag(1);
        return company;
    }
    // map entity --> UpdateCompanyDTO
    public static UpdateCompanyDTO mapToUpdateCompanyDTO (Company company) {
        UpdateCompanyDTO updateCompanyDTO = modelMapper.map(company, UpdateCompanyDTO.class);
        return updateCompanyDTO;
    }
    //Map UpdateCompanyDTO --> Company
    public static Company mapToCompanyEntity(UpdateCompanyDTO updateCompanyDTO){
        Company company = modelMapper.map(updateCompanyDTO, Company.class);
        return company;
    }
    // map User --> EmployerProfileEditDTO
    public static EmployerProfileEditDTO mapToEmployerProfileEditDTO (User user) {
        EmployerProfileEditDTO employerProfileEditDTO = modelMapper.map(user, EmployerProfileEditDTO.class);
        return employerProfileEditDTO;
    }
    //Map EmployerProfileEditDTO --> User
    public static User mapToUserEntity(EmployerProfileEditDTO employerProfileEditDTO){
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        User user = modelMapper.map(employerProfileEditDTO, User.class);
        return user;
    }
    // Map a page Job entity to a Page<DashboardJobDTO>
    public Page<DashboardJobDTO> mapToPageDashboardJobDTO(Page<Job> pageJobEntity) {
        final Page<DashboardJobDTO> contactDtoPage = pageJobEntity.map(this::mapToDashboardJobDTO);
        return contactDtoPage;
    }

    // Map Job -> DashboardJobDTO
    public DashboardJobDTO mapToDashboardJobDTO(Job jobEntity) {
        String deadlineApplyJob = DateTimeUtils.instantToString(jobEntity.getDeadline(), CommonConstant.FORMAT_DATE);
        Integer userApplyNumber = jobEntity.getListUserAppliedJob().size();
        DashboardJobDTO dashboardJobDTO = modelMapper.map(jobEntity, DashboardJobDTO.class);
        dashboardJobDTO.setDeadline(deadlineApplyJob);
        dashboardJobDTO.setUserApplyNumber(userApplyNumber);
        dashboardJobDTO.setJobType(jobEntity.getJobType().getName());
        if(jobEntity.getDeadline().isAfter(Instant.now())){
            dashboardJobDTO.setStatus("Active");
        }else{
            dashboardJobDTO.setStatus("Expired");
        }
        return dashboardJobDTO;
    }

    // Map a page Job entity to a Page<DashboardJobDTO>
    public Page<DashboardCandidateDTO> mapToPageDashboardCandidateDTO(Page<UserAppliedJob> pageCandidateEntity) {
        final Page<DashboardCandidateDTO> contactDtoPage = pageCandidateEntity.map(this::mapToDashboardCandidateDTO);
        return contactDtoPage;
    }

    // Map UserAppliedJob -> DashboardCandidateDTO
    public DashboardCandidateDTO mapToDashboardCandidateDTO(UserAppliedJob userAppliedJobEntity) {
        String appliedDate = DateTimeUtils.instantToString(userAppliedJobEntity.getAppliedDate(), CommonConstant.FORMAT_DATE);
        DashboardCandidateDTO dashboardCandidateDTO = modelMapper.map(userAppliedJobEntity, DashboardCandidateDTO.class);
        dashboardCandidateDTO.setAppliedDate(appliedDate);
        return dashboardCandidateDTO;
    }

    // map  CandidateDetailsDTO--> entity
    public static User mapRegisterDTOtoUser(RegisterDTO registerDTO) {
        User user = modelMapper.map(registerDTO, User.class);
        return user;
    }

    // map  UserAppliedJob --> InterviewScheduleDTO
    public static InterviewScheduleDTO mapUserAppliedJobToInterviewScheduleDTO(UserAppliedJob userAppliedJob) {
        InterviewScheduleDTO interviewScheduleDTO = modelMapper.map(userAppliedJob, InterviewScheduleDTO.class);
        return interviewScheduleDTO;
    }

    //Map CompanyReviewDTO --> Review
    public static Review mapToReviewEntity(CompanyReviewDTO CompanyReviewDTO){
        Review review = modelMapper.map(CompanyReviewDTO, Review.class);
        review.setDeleteFlag(1);
        return review;
    }

    //Map Review --> CompanyReviewDTO
    public static CompanyReviewDTO mapToCompanyReviewDTO(Review review){
        CompanyReviewDTO companyReviewDTO = modelMapper.map(review, CompanyReviewDTO.class);
        return companyReviewDTO;
    }

    // Map a page Review entity to a page<ListCompanyReviewDTO>
    public Page<ListCompanyReviewDTO> mapToPageListCompanyReviewDTO(Page<Review> pageCompanyReviewEntity) {
        final Page<ListCompanyReviewDTO> contactDtoPage = pageCompanyReviewEntity.map(this::mapToListCompanyReviewDTO);
        return contactDtoPage;
    }

    // Map Review -> ListCompanyReviewDTO
    public ListCompanyReviewDTO mapToListCompanyReviewDTO(Review reviewEntity) {
        ListCompanyReviewDTO listCompanyReviewDTO = modelMapper.map(reviewEntity, ListCompanyReviewDTO.class);
        listCompanyReviewDTO.setReviewDate(DateTimeUtils.instantToString(reviewEntity.getLastModifiedDate(),CommonConstant.FORMAT_DATETIME));
        return listCompanyReviewDTO;
    }

}
