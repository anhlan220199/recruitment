package com.project.recruitment.constants;

public class PathConstant {
    private PathConstant() {

    }

    public static String getProjectPath() {
        return System.getProperty("user.dir");
    }

    public static final String PATH_TO_PROJECT = getProjectPath() + "\\src\\main\\resources";

    public static final String PATH_TO_CANDIDATE_CV = "\\candidate-cv\\";
    public static final String PATH_TO_APPLY_ONLINE_CV = "\\apply-online-cv\\";
}
