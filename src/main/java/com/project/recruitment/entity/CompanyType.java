package com.project.recruitment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "company_type", schema = "recruitment_db", catalog = "")
public class CompanyType {
    private int companyTypeId;
    private String name;
    private int deleteFlag;
    private Set<Company> listCompany;

    @Id
    @Column(name = "company_type_id")
    public int getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(int companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "delete_flag")
    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @OneToMany(mappedBy = "companyType")
    public Set<Company> getListCompany() {
        return listCompany;
    }

    public void setListCompany(Set<Company> listCompany) {
        this.listCompany = listCompany;
    }
}
