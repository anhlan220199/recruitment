package com.project.recruitment.repository;

import com.project.recruitment.entity.FavoriteJob;
import com.project.recruitment.entity.Job;
import com.project.recruitment.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FavoriteJobRepository extends JpaRepository<FavoriteJob,Integer> {
    Optional<FavoriteJob> findByUserAndJob(User user, Job job);

    List<FavoriteJob> findAllByUserAndDeleteFlagIs(User user, Integer deleteFlag);
}

