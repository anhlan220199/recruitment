package com.project.recruitment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CandidateDetailDTO {

    private int userId;
    private String username;
    private Integer companyId;
    private String password;
    private String fullName;
    private String phone;
    private String cvFileName;
    private String cvFilePath;
    private String email;
    private String aboutMe;
    private String expertise;
    private String address;
    private String jobType;
    private String location;
    private String experience;
    private String gender;
    private String qualification;
    private String workExperience;
    private String educationBackground;
    private String specialQualification;
    private String professionalSkill;

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public String getCvFileName() {
        return cvFileName;
    }

    public String getCvFilePath() {
        return cvFilePath;
    }

    public String getEmail() {
        return email;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public String getExpertise() {
        return expertise;
    }

    public String getAddress() {
        return address;
    }

    public String getJobType() {
        return jobType;
    }

    public String getLocation() {
        return location;
    }

    public String getExperience() {
        return experience;
    }

    public String getGender() {
        return gender;
    }

    public String getQualification() {
        return qualification;
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public String getEducationBackground() {
        return educationBackground;
    }

    public String getSpecialQualification() {
        return specialQualification;
    }

    public String getProfessionalSkill() {
        return professionalSkill;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCvFileName(String cvFileName) {
        this.cvFileName = cvFileName;
    }

    public void setCvFilePath(String cvFilePath) {
        this.cvFilePath = cvFilePath;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public void setEducationBackground(String educationBackground) {
        this.educationBackground = educationBackground;
    }

    public void setSpecialQualification(String specialQualification) {
        this.specialQualification = specialQualification;
    }

    public void setProfessionalSkill(String professionalSkill) {
        this.professionalSkill = professionalSkill;
    }
}
