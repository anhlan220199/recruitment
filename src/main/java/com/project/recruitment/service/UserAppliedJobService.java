package com.project.recruitment.service;

import com.project.recruitment.dto.InterviewScheduleDTO;
import com.project.recruitment.entity.UserAppliedJob;
import com.project.recruitment.exception.UserAppliedJobNotFoundException;

import java.io.FileNotFoundException;
import java.text.ParseException;

public interface UserAppliedJobService {

    void deleteUserAppliedJob(String userAppliedJobId) throws UserAppliedJobNotFoundException;
    Integer countUserAppliedJob() throws UserAppliedJobNotFoundException;

    String getAppliedCVFileName(String id) throws FileNotFoundException;

    InterviewScheduleDTO getUserAppliedJob(Integer id) throws UserAppliedJobNotFoundException;

    void addInterviewSchedule(String userAppliedJobId, InterviewScheduleDTO interviewScheduleDTO) throws UserAppliedJobNotFoundException, ParseException;
}
