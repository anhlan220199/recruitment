package com.project.recruitment.controller;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.constants.UrlConstant;
import com.project.recruitment.dto.*;
import com.project.recruitment.entity.Job;
import com.project.recruitment.exception.*;
import com.project.recruitment.service.CompanyReviewService;
import com.project.recruitment.service.CompanyService;
import com.project.recruitment.service.EmailService;
import com.project.recruitment.service.JobService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Pattern;
import java.util.List;

@Controller
@Validated
@RequestMapping("/company")
public class CompanyController {

    private final CompanyService companyService;
    private final CompanyReviewService companyReviewService;
    private final EmailService emailService;
    private final JobService jobService;

    public CompanyController(CompanyService companyService, CompanyReviewService companyReviewService, EmailService emailService, JobService jobService) {
        this.companyService = companyService;
        this.companyReviewService = companyReviewService;
        this.emailService = emailService;
        this.jobService = jobService;
    }

    @GetMapping("/detail/{id}")
    public String getCompanyInformationDetail(Model model, @PathVariable int id) throws PageNotFoundException {
        CompanyDetailDTO companyDetailDTO = companyService.getCompanyInformationDetail(id);
        model.addAttribute("companyDetail", companyDetailDTO);
        model.addAttribute("applyJobDTO",new CandidateJobDTO());
        return "company/company-details";
    }

    @GetMapping("/list")
    public String viewListCompany(Model model,
                                  @Pattern(regexp = "^-?[0-9]\\d*(\\d+)?$", message = "Page number is number only!")
                                  @RequestParam(defaultValue = "1", value = "pageNumber", required = true) String pageNumber,
                                  @RequestParam(value = "keyword", required = true) String keyword) {
        Page<ListCompanyDTO> page = companyService.listAll(keyword, pageNumber);
        model.addAttribute("page", page);
        model.addAttribute("isFilter",false);
        model.addAttribute("keyword", keyword);
        model.addAttribute("isEmptyListCompany", page.isEmpty());
        model.addAttribute("pageSize", CommonConstant.PAGE_SIZE);
        model.addAttribute("listCity", companyService.getAllCompanyLocation());
        model.addAttribute("listCompanyType", companyService.getAllCompanyType());
        return "company/company-listing";
    }

    @GetMapping("/filter")
    public String viewListCompany(Model model,
                                  @Pattern(regexp = "^-?[0-9]\\d*(\\d+)?$", message = "Page number is number only!")
                                  @RequestParam(defaultValue = "1", value = "pageNumber", required = true) String pageNumber,
                                  @RequestParam(value = "city", required = false) String city,
                                  @RequestParam(value = "type", required = false) String companyType ) {
        Page<ListCompanyDTO> page = companyService.filterCompanyByTypeAndCity(city, companyType, pageNumber);
        model.addAttribute("page", page);
        model.addAttribute("isFilter",true);
        model.addAttribute("isEmptyListCompany", page.isEmpty());
        model.addAttribute("pageSize", CommonConstant.PAGE_SIZE);
        model.addAttribute("listCity", companyService.getAllCompanyLocation());
        model.addAttribute("listCompanyType", companyService.getAllCompanyType());
        model.addAttribute("city",city);
        model.addAttribute("type",companyType);
        return "company/company-listing";
    }

    @GetMapping("/dashboard/job/manage")
    public String viewManageJob() {
        return "company/company-dashboard-manage-job";
    }

    @GetMapping("/dashboard")
    public String goToDashboardPage(Model model) {
        model.addAttribute("totalJobPosted",jobService.getTotalJobPostedThisMonth());
        return "company/company-dashboard";
    }

    @GetMapping("/create")
    public String getCompanyCreatingForm(Model model) {
        model.addAttribute("newCompany", new CompanyCreatingDTO());
        model.addAttribute("companyTypes", companyService.getAllCompanyType());
        return "company/company-creating";
    }

    @PostMapping("/create")
    public String saveNewCompany(@ModelAttribute("newCompany") CompanyCreatingDTO companyCreatingDTO, BindingResult bindingResult, Model model) throws CompanyAlreadyExistException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("companyTypes", companyService.getAllCompanyType());
            model.addAttribute("newCompany", new CompanyCreatingDTO());
            return "company/company-creating";
        }
        companyService.saveNewCompany(companyCreatingDTO);
        return "redirect:/register/company";
    }

    @GetMapping("/delete/{companyId}")
    public String handleDeleteCompany(
            @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Company ID is number only!") @PathVariable("companyId") String companyId) throws DeleteCompanyNotFoundException {
        companyService.deleteCompany(companyId);
        return "redirect:/company/list?pageNumber=1&keyword=";
    }

    @GetMapping("/edit/{companyId}")
    public String getEditCompanyForm(@Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Company ID is number only!") @PathVariable("companyId") String companyId, Model model) throws CompanyNotFoundException {
        model.addAttribute("company", companyService.getCompanyToUpdate(companyId));
        model.addAttribute("companyTypes", companyService.getAllCompanyType());
        return "company/company-edit";
    }

    @PostMapping("/edit/{companyId}")
    public String handleEditCompany(@Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Company ID is number only!") @PathVariable("companyId") String companyId, @ModelAttribute("company") UpdateCompanyDTO updateCompanyDTO, BindingResult bindingResult, Model model) throws CompanyNotFoundException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("company", companyService.getCompanyToUpdate(companyId));
            model.addAttribute("companyTypes", companyService.getAllCompanyType());
            return "company/company-edit";
        }
        boolean isSuccess = companyService.updateCompanyInfo(companyId, updateCompanyDTO);
        if (isSuccess) {
            model.addAttribute("isSuccess", true);
            model.addAttribute("updateSuccess", CommonConstant.UserMessage.UPDATE_SUCCESS);
            return "company/company-edit";
        }
        return "company/company-edit";

    }

    @GetMapping("/review-list")
    public String viewAllReview(Model model,
                                @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Company ID is number only!")
                                @RequestParam(value = "companyId", required = true) String companyId,
                                @Pattern(regexp = "^-?[0-9]\\d*(\\d+)?$", message = "Page number is number only!")
                                @RequestParam(defaultValue = "1", value = "pageNumber", required = true) String pageNumber
                                ) throws CompanyNotFoundException {
        Page<ListCompanyReviewDTO> page = companyReviewService.listAllReviewByCompanyId(companyId,pageNumber);
        model.addAttribute("page", page);
        model.addAttribute("companyId",companyId);
        model.addAttribute("company",companyService.getCompanyByCompanyId(companyId));
        model.addAttribute("isEmptyListCompanyReview", page.isEmpty());
        model.addAttribute("pageSize", CommonConstant.PAGE_SIZE);
        return "company/review-list";
    }

    @PostMapping("/contact/{candidateId}")
    public String sendEmailToCandidate(
            HttpServletRequest request,
            @PathVariable("candidateId") Integer candidateId) throws CandidateNotFoundException, MessagingException {
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        String message = request.getParameter("message");
        emailService.sendEmailFromCompanyToCandidate(email, name, message, candidateId);
        return UrlConstant.getRedirectUrl("/candidate/detail/" + candidateId);
    }
}
