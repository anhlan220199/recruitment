package com.project.recruitment.service.impl;

import com.project.recruitment.constants.PathConstant;
import com.project.recruitment.dto.CandidateJobDTO;
import com.project.recruitment.entity.Job;
import com.project.recruitment.entity.User;
import com.project.recruitment.entity.UserAppliedJob;
import com.project.recruitment.exception.ApplyJobNotFoundException;
import com.project.recruitment.exception.CandidateNotFoundException;
import com.project.recruitment.exception.LogoutException;
import com.project.recruitment.helper.FileHelper;
import com.project.recruitment.repository.JobRepository;
import com.project.recruitment.repository.UserAppliedJobRepository;
import com.project.recruitment.repository.UserRepository;
import com.project.recruitment.service.CandidateJobService;
import com.project.recruitment.util.SecurityUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

@Service
public class CandidateJobServiceImpl implements CandidateJobService {

    private final UserAppliedJobRepository userAppliedJobRepository;
    private final UserRepository userRepository;
    private final JobRepository jobRepository;

    public CandidateJobServiceImpl(UserAppliedJobRepository userAppliedJobRepository, JobRepository jobRepository,
                                   UserRepository userRepository) {
        this.userAppliedJobRepository = userAppliedJobRepository;
        this.jobRepository = jobRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void saveApplyJob(CandidateJobDTO candidateJobDTO, Integer jobId, String cvPath) throws ApplyJobNotFoundException, LogoutException, CandidateNotFoundException {
        Optional<User> userOptional = getCurrentUserLogin();
        Optional<Job> jobOptional = getJobApply(jobId);
        if(jobOptional.isPresent()){
            User user= userOptional.get();
            Job job= jobOptional.get();
            Optional<UserAppliedJob> userAppliedJobOptional = userAppliedJobRepository.findByUserAndJobAndDeleteFlagIs(user,job,1);
            if(userAppliedJobOptional.isPresent()){
                UserAppliedJob userAppliedJob = userAppliedJobOptional.get();
                userAppliedJob.setEmail(candidateJobDTO.getEmail());
                userAppliedJob.setMessage(candidateJobDTO.getMessage());
                userAppliedJob.setFileCVPath(cvPath);
                userAppliedJob.setAppliedDate(Instant.now());
                userAppliedJob.setLastModifiedBy(user.getUsername());
                userAppliedJob.setLastModifiedDate(Instant.now());
                userAppliedJob.setDeleteFlag(1);
                userAppliedJobRepository.save(userAppliedJob);
            }else{
                UserAppliedJob userAppliedJob = new UserAppliedJob();
                userAppliedJob.setUser(userOptional.get());
                userAppliedJob.setJob(jobOptional.get());
                userAppliedJob.setEmail(candidateJobDTO.getEmail());
                userAppliedJob.setMessage(candidateJobDTO.getMessage());
                userAppliedJob.setFileCVPath(cvPath);
                userAppliedJob.setCreatedBy(user.getUsername());
                userAppliedJob.setCreatedDate(Instant.now());
                userAppliedJob.setLastModifiedBy(user.getUsername());
                userAppliedJob.setLastModifiedDate(Instant.now());
                userAppliedJob.setAppliedDate(Instant.now());
                userAppliedJob.setDeleteFlag(1);
                userAppliedJobRepository.save(userAppliedJob);
            }
        }else{
            throw new ApplyJobNotFoundException();
        }
    }

    @Override
    public boolean saveApplyOnlineCVFile(MultipartFile multipartFile, String fileName) throws IOException {
        return FileHelper.saveFile(multipartFile, PathConstant.PATH_TO_APPLY_ONLINE_CV, fileName);
    }

    public Optional<User> getCurrentUserLogin() throws LogoutException, CandidateNotFoundException {
        Optional<String> userName = SecurityUtils.getCurrentUserLogin();
        if (userName.get().equals("anonymousUser")) {
            throw new LogoutException();
        }
        Optional<User> existCandidate = userRepository.findByUsernameAndDeleteFlagIs(userName.get(),1);
        if (existCandidate.isEmpty()) {
            throw new CandidateNotFoundException();
        }
        return existCandidate;
    }

    public Optional<Job> getJobApply(Integer jobId) throws ApplyJobNotFoundException {
        Optional<Job> jobEntity = jobRepository.findByJobIdAndDeleteFlagIs(jobId, 1);
        if (jobEntity.isEmpty()) {
            throw new ApplyJobNotFoundException();
        }
        return jobEntity;
    }
}
