package com.project.recruitment.controller;

import com.project.recruitment.entity.User;
import com.project.recruitment.exception.CandidateNotFoundException;
import com.project.recruitment.helper.Helper;
import com.project.recruitment.service.CandidateService;
import com.project.recruitment.service.EmailService;
import net.bytebuddy.utility.RandomString;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/login")
public class LoginController {

    private final CandidateService candidateService;
    private final EmailService emailService;

    public LoginController(CandidateService candidateService, EmailService emailService) {
        this.candidateService = candidateService;
        this.emailService = emailService;
    }

    @GetMapping("")
    public String toLoginForm() {
        return "login/login";
    }

    @GetMapping("/forgot_password")
    public String showForgotPasswordForm() {
        return "/login/forgot_password_form";
    }

    @PostMapping("/forgot_password")
    public String processForgotPassword(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String token = RandomString.make(30);

        try {
            candidateService.updateResetPasswordToken(token, email);
            String resetPasswordLink = Helper.getSiteURL(request) + "/login/reset_password?token=" + token;
            emailService.sendEmailForgotPassword(email, resetPasswordLink);
            model.addAttribute("message", "We have sent a reset password link to your email. Please check.");
        } catch (CandidateNotFoundException ex) {
            model.addAttribute("error", "Email address don't exists! Please enter valid email address again!");
        } catch (MessagingException e) {
            model.addAttribute("error", "Error while sending email");
        }
        return "/login/forgot_password_form";
    }

    @GetMapping("/reset_password")
    public String showResetPasswordForm(@Param(value = "token") String token, Model model) throws CandidateNotFoundException {
        User user = candidateService.getByResetPasswordToken(token);
        model.addAttribute("token", token);
        if (user == null) {
            model.addAttribute("message", "Invalid Token");
        }
        return "/login/reset_password_form";
    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request, Model model) throws CandidateNotFoundException {
        String token = request.getParameter("token");
        String password = request.getParameter("password");

        User user = candidateService.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");

        if (user == null) {
            model.addAttribute("message", "Invalid Token");
        } else {
            boolean isSuccessResetPassword = candidateService.updatePassword(user, password);
            model.addAttribute("message", "You have successfully changed your password.");
            model.addAttribute("isSuccessResetPassword", isSuccessResetPassword);
        }
        return "/login/reset_password_form";
    }
}
