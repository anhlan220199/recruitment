package com.project.recruitment.repository;

import com.project.recruitment.entity.Job;
import com.project.recruitment.entity.User;
import com.project.recruitment.entity.UserAppliedJob;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserAppliedJobRepository extends JpaRepository<UserAppliedJob, Integer> {

    @Query(value = "SELECT * from `user_applied_job` uaj where uaj.job_id in (select job_id from `job` where company_id=?1) AND delete_flag=?2", nativeQuery = true)
    Page<UserAppliedJob> findAllUserAppliedJobByCompanyIdAndDeleteFlagIs(Integer companyId, Integer deleteFlag, Pageable pageable);

    @Query(value = "select * from user_applied_job uaj where uaj.job_id in (select job_id from job where company_id=?1) and uaj.user_id in (select user_id from user where LOWER(full_name) like %?2%) AND delete_flag=?3", nativeQuery = true)
    Page<UserAppliedJob> relativeSearchingByCompanyIdAndDeleteFlagIs(Integer companyId,String fullName, Integer deleteFlag, Pageable pageable);

    Optional<UserAppliedJob> findByUserAppliedJobIdAndDeleteFlagIs(Integer userAppliedJobId, Integer deleteFlag);

    List<UserAppliedJob> findAllByDeleteFlagIs(Integer deleteFlag);
    Optional<UserAppliedJob> findByUserAndJobAndDeleteFlagIs(User user, Job job, int deleteFlag);
}
