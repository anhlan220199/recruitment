package com.project.recruitment.repository;

import com.project.recruitment.entity.ConfirmationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, Integer> {

    Optional<ConfirmationToken> findByConfirmationToken(String confirmationToken);

    ConfirmationToken findByConfirmationTokenAndDeleteFlagIs(String confirmationToken, Integer deleteFlag);
}
