package com.project.recruitment.entity;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "user_applied_job", schema = "recruitment_db")
public class UserAppliedJob extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;
    private Integer userAppliedJobId;
    private Instant appliedDate;
    private int deleteFlag;
    private Job job;
    private User user;
    private String email;
    private String message;
    private String fileCVPath;
    private Instant interviewDate;
    private String interviewLocation;

    @Id
    @Column(name = "user_applied_job_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getUserAppliedJobId() {
        return userAppliedJobId;
    }

    public void setUserAppliedJobId(Integer userAppliedJobId) {
        this.userAppliedJobId = userAppliedJobId;
    }

    @Basic
    @Column(name = "applied_date")
    public Instant getAppliedDate() {
        return appliedDate;
    }

    public void setAppliedDate(Instant appliedDate) {
        this.appliedDate = appliedDate;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "file_cv_path")
    public String getFileCVPath() {
        return fileCVPath;
    }

    public void setFileCVPath(String fileCVPath) {
        this.fileCVPath = fileCVPath;
    }

    @Basic
    @Column(name = "delete_flag")
    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @ManyToOne
    @JoinColumn(name = "job_id", referencedColumnName = "job_id", nullable = false)
    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Instant getInterviewDate() {
        return interviewDate;
    }

    public String getInterviewLocation() {
        return interviewLocation;
    }

    public void setInterviewDate(Instant interviewEmailTitle) {
        this.interviewDate = interviewEmailTitle;
    }

    public void setInterviewLocation(String interviewEmailContent) {
        this.interviewLocation = interviewEmailContent;
    }
}
