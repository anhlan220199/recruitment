package com.project.recruitment.service;

import com.project.recruitment.dto.*;
import com.project.recruitment.entity.*;
import com.project.recruitment.exception.*;
import org.springframework.data.domain.Page;

import java.text.ParseException;
import java.util.List;

public interface JobService {

    Page<ListJobDTO> listAll(String keyword, String pageNumber);

    Page<ListJobDTO> filterJob(String skill, String level, String position, String city, String jobType, String companyName, Integer salaryMin, Integer salaryMax, String pageNumber);

    ListJobDTO findJobById(Integer jobID);

    JobDetailDTO getJobDetailById(int jobId) throws PageNotFoundException;

    void saveJob(PostJobDTO newJob) throws ParseException;

    List<SimilarJobDTO> getSimilarJobs(Skill skill);

    void deleteJob(String id) throws  AccessDenied,DeleteJobNotFoundException;

    void editJob(UpdateJobDTO editJob, String jobIdString) throws ParseException,AccessDenied,JobNotFoundException;

    UpdateJobDTO getUpdateJobDetailById(Integer jobId) throws Exception;

    boolean saveFavoriteJob(String jobId) throws JobNotFoundException;

    boolean discardFavoriteJob(String jobId) throws JobNotFoundException;

    boolean isJobSavedByCurrentUser(Integer jobId);

    List<Skill> getAllSkill();

    List<Position> getAllJobPosition();

    List<JobLevel> getAllJobLevel();

    List<JobType> getAllJobType();

    List<String> getAllJobLocation();

    List<Company> getAllCompany();

    Integer countJobs();

    List<ListJobDTO> getListRecentJobFirst();

    List<ListJobDTO> getListRecentJobSecond();

    List<ListJobDTO> getListFeatureJobFirst();

    List<ListJobDTO> getListFeatureJobSecond();

    List<UserAppliedJob> getListUserAppliedJob(Integer jobId) throws JobNotFoundException;

    User getCurrentUser() throws CandidateNotFoundException;

    List<Integer> getListJobIdCurrentCompany() throws CandidateNotFoundException;

    int getTotalJobPostedThisMonth();
    
}
