package com.project.recruitment.service;

import com.project.recruitment.dto.CandidateJobDTO;
import com.project.recruitment.dto.InterviewScheduleDTO;
import com.project.recruitment.entity.ConfirmationToken;
import com.project.recruitment.exception.CandidateNotFoundException;
import com.project.recruitment.exception.CompanyNotFoundException;
import com.project.recruitment.exception.JobNotFoundException;
import com.project.recruitment.exception.UserAppliedJobNotFoundException;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;

public interface EmailService {
    void sendRegisterConfirmationMail(String emailTo, ConfirmationToken confirmationToken) throws MessagingException, UnsupportedEncodingException;

    boolean sendApplyJobSuccessMail(CandidateJobDTO candidateJobDTO) throws MessagingException, JobNotFoundException;

    boolean sendInterviewScheduleMail(InterviewScheduleDTO interviewScheduleDTO) throws MessagingException, JobNotFoundException, CandidateNotFoundException, ParseException;

    void sendEmailForgotPassword(String email, String resetPasswordLink) throws MessagingException;

    void sendEmailFromCandidateToCompany(String emailCandidate, String name, String message, Integer companyId) throws MessagingException, CompanyNotFoundException;

    void sendEmailFromCompanyToCandidate(String emailCompany, String name, String message, Integer candidateId) throws CandidateNotFoundException, MessagingException;
    void sendEmailToUserAppliedJobOfCompany(String emailCompany, String name, String messageCompany, Integer userAppliedJobId) throws UserAppliedJobNotFoundException, MessagingException;
}
