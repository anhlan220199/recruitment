package com.project.recruitment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ListCandidateDTO {

    private Integer userId;

    private Integer companyId;

    private String fullName;

    private String phone;

    private String email;

    private String aboutMe;

    private String expertise;

    private String address;

    private int deleteFlag;

    public Integer getUserId() {
        return userId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public String getExpertise() {
        return expertise;
    }

    public String getAddress() {
        return address;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}
