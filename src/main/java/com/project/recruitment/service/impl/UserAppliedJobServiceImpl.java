package com.project.recruitment.service.impl;

import com.project.recruitment.dto.InterviewScheduleDTO;
import com.project.recruitment.entity.UserAppliedJob;
import com.project.recruitment.exception.UserAppliedJobNotFoundException;
import com.project.recruitment.helper.Helper;
import com.project.recruitment.repository.JobRepository;
import com.project.recruitment.repository.UserAppliedJobRepository;
import com.project.recruitment.repository.UserRepository;
import com.project.recruitment.service.UserAppliedJobService;
import com.project.recruitment.util.DateTimeUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class UserAppliedJobServiceImpl implements UserAppliedJobService {

    private final UserAppliedJobRepository userAppliedJobRepository;
    private final Helper helper;
    private final JobRepository jobRepository;
    private final UserRepository userRepository;

    public UserAppliedJobServiceImpl(UserAppliedJobRepository userAppliedJobRepository, Helper helper, JobRepository jobRepository, UserRepository userRepository) {
        this.userAppliedJobRepository = userAppliedJobRepository;
        this.helper = helper;
        this.jobRepository = jobRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void deleteUserAppliedJob(String id) throws UserAppliedJobNotFoundException {
        Integer userAppliedJobId = Integer.parseInt(id);
        Optional<UserAppliedJob> userAppliedJobEntity = userAppliedJobRepository.findByUserAppliedJobIdAndDeleteFlagIs(userAppliedJobId, 1);

        if (userAppliedJobEntity.isEmpty()) {
            throw new UserAppliedJobNotFoundException();
        }
        userAppliedJobEntity.get().setDeleteFlag(0);
        userAppliedJobRepository.save(userAppliedJobEntity.get());
    }

    @Override
    public String getAppliedCVFileName(String id) throws FileNotFoundException {
        Integer userAppliedJobId = Integer.parseInt(id);
        Optional<UserAppliedJob> userAppliedJob = userAppliedJobRepository.findByUserAppliedJobIdAndDeleteFlagIs(userAppliedJobId,1);
        if(userAppliedJob.isPresent()){
           String cvFileName = userAppliedJob.get().getFileCVPath();
            if(cvFileName!=null && !"".equals(cvFileName)){
                return cvFileName;
            }
            throw new FileNotFoundException();
        }
       throw new FileNotFoundException();

    }

    @Override
    public Integer countUserAppliedJob() throws UserAppliedJobNotFoundException {
        List<UserAppliedJob> listUserAppliedJob = userAppliedJobRepository.findAllByDeleteFlagIs(1);
        Integer count = 0;
        if (!listUserAppliedJob.isEmpty()){
            count = listUserAppliedJob.size();
        }
        return count;
    }

    public InterviewScheduleDTO getUserAppliedJob(Integer id) throws UserAppliedJobNotFoundException {
        Optional<UserAppliedJob> userAppliedJob = userAppliedJobRepository.findByUserAppliedJobIdAndDeleteFlagIs(id, 1);
        if (userAppliedJob.isEmpty()){
            throw new UserAppliedJobNotFoundException();
        }
        InterviewScheduleDTO interviewScheduleDTO = helper.mapUserAppliedJobToInterviewScheduleDTO(userAppliedJob.get());
        interviewScheduleDTO.setJobId(userAppliedJob.get().getJob().getJobId());
        interviewScheduleDTO.setUserId(userAppliedJob.get().getUser().getUserId());
        return interviewScheduleDTO;
    }

    @Override
    public void addInterviewSchedule(String userAppliedJobId, InterviewScheduleDTO interviewScheduleDTO) throws UserAppliedJobNotFoundException, ParseException {
        Integer userAppliedJobIdFinal = Integer.parseInt(userAppliedJobId);
        Optional<UserAppliedJob> userAppliedJobOld = userAppliedJobRepository.findByUserAppliedJobIdAndDeleteFlagIs(userAppliedJobIdFinal,1);
        if (userAppliedJobOld.isEmpty()){
            throw new UserAppliedJobNotFoundException();
        }
        UserAppliedJob userAppliedJob = userAppliedJobOld.get();
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(interviewScheduleDTO, userAppliedJob);
//        userAppliedJob.setJob(jobRepository.findByJobIdAndDeleteFlagIs(interviewScheduleDTO.getJobId(),1).get());
//        userAppliedJob.setUser(userRepository.findByUserIdAndDeleteFlagIs(interviewScheduleDTO.getUserId(),1).get());
        Instant instantInterview = DateTimeUtils.convertStringToDateInterview(interviewScheduleDTO, null);
        userAppliedJob.setInterviewDate(instantInterview);
        userAppliedJobRepository.save(userAppliedJob);
    }
}
