package com.project.recruitment.repository;

import com.project.recruitment.entity.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {

    @Query(value = "SELECT * from `company` WHERE LOWER(name) LIKE %?1% AND delete_flag=1", nativeQuery = true)
    Page<Company> relativeSearching(String keyword, Pageable pageable);

    @Query(value = "SELECT * from `company` WHERE LOWER(city) LIKE %?1% AND delete_flag=1", nativeQuery = true)
    Page<Company> relativeCityFiltering(String city, Pageable pageable);

    // sua lai
    @Query(value = "SELECT * from `company` WHERE company_type_id=?1 AND delete_flag=1", nativeQuery = true)
    Page<Company> relativeCompanyTypeFiltering(Integer companyTypeId, Pageable pageable);

    // sua lai
    @Query(value = "SELECT * from `company` WHERE LOWER(city) LIKE %?1% AND company_type_id=?2 AND delete_flag=1", nativeQuery = true)
    Page<Company> relativeFiltering(String city,Integer companyTypeId, Pageable pageable);

    Page<Company> findAllByDeleteFlagIs(Integer i, Pageable pageable);

    Optional<Company> findByCompanyId(Integer companyId);

    List<Company> findAllByDeleteFlagIs(Integer deleteFlag);

    Optional<Company> findByEmailAndDeleteFlagIs(String email, Integer deleteFlag);

    Optional<Company> findByCompanyIdAndDeleteFlagIs(Integer companyId, Integer deleteFlag);

    @Query(value = "SELECT DISTINCT city from `company` where delete_flag=1",  nativeQuery = true)
    List<String> findDistinctCity();

    Optional<Company>findByNameAndDeleteFlag(String companyName, Integer deleteFlag);

}
