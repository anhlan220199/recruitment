package com.project.recruitment.service.impl;

import com.project.recruitment.dto.RegisterDTO;
import com.project.recruitment.entity.ConfirmationToken;
import com.project.recruitment.entity.User;
import com.project.recruitment.exception.RegisterFailException;
import com.project.recruitment.helper.Helper;
import com.project.recruitment.repository.ConfirmationTokenRepository;
import com.project.recruitment.repository.RoleRepository;
import com.project.recruitment.repository.UserRepository;
import com.project.recruitment.service.EmailService;
import com.project.recruitment.service.RegisterService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RegisterServiceImpl implements RegisterService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final EmailService emailService;

    public RegisterServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, ConfirmationTokenRepository confirmationTokenRepository, EmailService emailService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.confirmationTokenRepository = confirmationTokenRepository;
        this.emailService = emailService;
    }

    /* email and username must be unique*/
    @Override
    public void saveNewUser(RegisterDTO registerDTO, String roleName) throws RegisterFailException, UnsupportedEncodingException, MessagingException {
        List<User> listUser = userRepository.findByUsernameOrEmail(registerDTO.getUsername(), registerDTO.getEmail());
        if (listUser.size() == 0) {
            User user = Helper.mapRegisterDTOtoUser(registerDTO);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setRole(roleRepository.findByName(roleName).get());
            user.setDeleteFlag(0);
            if (roleName.equals("CANDIDATE")) {
                user.setIsCandidate(1);
            }
            userRepository.save(user);
            ConfirmationToken confirmationToken = new ConfirmationToken();
            confirmationToken.setConfirmationToken(UUID.randomUUID().toString());
            Optional<User> userOptional = userRepository.findByEmailAndDeleteFlagIs(registerDTO.getEmail(), 0);
            if (userOptional.isPresent()) {
                confirmationToken.setUser(userOptional.get());
                confirmationTokenRepository.save(confirmationToken);
                emailService.sendRegisterConfirmationMail(registerDTO.getEmail(), confirmationToken);
            }
        } else {
            throw new RegisterFailException();
        }
    }

    @Override
    public boolean confirmationAccount(String token) {
        Optional<ConfirmationToken> confirmationTokenOptional = confirmationTokenRepository.findByConfirmationToken(token);
        if (confirmationTokenOptional.isPresent()) {
            String email = confirmationTokenOptional.get().getUser().getEmail();
            Optional<User> userOptional = userRepository.findByEmailAndDeleteFlagIs(email, 0);
            if (userOptional.isPresent()) {
                userOptional.get().setDeleteFlag(1);
                userRepository.save(userOptional.get());
                return true;
            }
        }
        return false;

    }
}
