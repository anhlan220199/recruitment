package com.project.recruitment.constants;

public class UrlConstant {

    private UrlConstant() {
    }

    public static final String REFRESH_TOKEN = "/get_refresh_token";

    public static String getRedirectUrl(String url) {
        return "redirect:" + url;
    }

}
