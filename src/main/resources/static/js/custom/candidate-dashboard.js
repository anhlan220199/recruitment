$(window).on("load", function(){

    //Get the context of the Chart canvas element we want to select
    var applicationCtx = $("#candidate-application-view-chart");

    // Chart Options
    var applicationChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom',
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Month',
                    padding: 5,
                    color: "#4ab021"
                },
                stacked: true
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Application Submit',
                    color: "#4ab021"

                },
                max: 200,
                min: 0,
                ticks: {
                    stepSize: 10
                },
                stacked: true
            }]
        },
        title: {
            display: true,
            text: 'APPLICATION SUBMIT THROUGH MONTHS'
        }
    };

    // Chart Data
    var applicationChartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "Total application submit",
            data: [10, 20, 30, 25, 38, 19, 14],
            lineTension: 0,
            fill: false,
            borderColor: "#4ab021",
            pointBorderColor: "#4ab021",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
        }]
    };

    var applicationConfig = {
        type: 'line',

        // Chart Options
        options : applicationChartOptions,

        data : applicationChartData
    };

    // Create the chart
    var applicationLineChart = new Chart(applicationCtx, applicationConfig);
});