package com.project.recruitment.service.impl;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.dto.*;
import com.project.recruitment.entity.Company;
import com.project.recruitment.entity.CompanyType;
import com.project.recruitment.entity.Job;
import com.project.recruitment.entity.Review;
import com.project.recruitment.exception.CompanyAlreadyExistException;
import com.project.recruitment.exception.CompanyNotFoundException;
import com.project.recruitment.exception.DeleteCompanyNotFoundException;
import com.project.recruitment.exception.PageNotFoundException;
import com.project.recruitment.helper.Helper;
import com.project.recruitment.repository.CompanyRepository;
import com.project.recruitment.repository.CompanyTypeRepository;
import com.project.recruitment.service.CompanyService;
import com.project.recruitment.service.HelperService;
import com.project.recruitment.util.DateTimeUtils;
import com.project.recruitment.util.HibernateUtils;
import com.project.recruitment.util.SecurityUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.*;

@Service
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    private final CompanyTypeRepository companyTypeRepository;
    private final HelperService helperService;

    private final Helper helper;
    private final EntityManager entityManager;

    public CompanyServiceImpl(CompanyRepository companyRepository, CompanyTypeRepository companyTypeRepository, HelperService helperService, Helper helper, EntityManager entityManager) {
        this.companyRepository = companyRepository;
        this.companyTypeRepository = companyTypeRepository;
        this.helperService = helperService;
        this.helper = helper;
        this.entityManager = entityManager;
    }

    @Override
    public CompanyDetailDTO getCompanyInformationDetail(int companyId) throws PageNotFoundException {
        Optional<Company> companyOpt = companyRepository.findByCompanyId(companyId);
        if (companyOpt.isEmpty()) {
            throw new PageNotFoundException();
        }
        Company company = companyOpt.get();
        Set<JobDetailDTO> jobList = new HashSet<>();
        for (Job job : company.getListJob()) {
            JobDetailDTO jobDetailDTO = Helper.mapToJobDetailDTO(job);
            jobDetailDTO.setJobTitle(job.getJobLevel().getName() + " " + job.getSkill().getName() + " " + job.getPosition().getName());
            jobDetailDTO.setJobType(job.getJobType().getName());
            jobDetailDTO.setDeadline(DateTimeUtils.instantToString(job.getDeadline(), CommonConstant.FORMAT_DATE));
            jobList.add(jobDetailDTO);
        }

        CompanyDetailDTO companyDetail = Helper.mapToCompanyDetailDTO(company);
        companyDetail.setType(company.getCompanyType().getName());
        companyDetail.setListJob(jobList);
        if(company.getTotalRating() != null){
            companyDetail.setTotalRating(company.getTotalRating());
        }
        return companyDetail;
    }

    @Override
    public Page<ListCompanyDTO> listAll(String keyword, String pageNumbers) {
        Integer pageNumber = Integer.parseInt(pageNumbers);
        Page<Company> pageCompanyEntity = null;
        Page<ListCompanyDTO> pageCompanyDTO = null;

        // check pageNumber
        Page<Company> allCompany = companyRepository.findAllByDeleteFlagIs(1, null);
        int sizeList = allCompany.getSize();
        pageNumber = Helper.checkPageNumber(sizeList, pageNumber);

        // get all Company
        if (keyword == null || "".equals(keyword)) {

            pageCompanyEntity = companyRepository.findAllByDeleteFlagIs(1,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE, Sort.by(Sort.Direction.DESC, "companyId")));

            pageCompanyDTO = helper.mapToPageCompanyDTO(pageCompanyEntity);

            // get company with keyword
        } else {
            String keywordFinal = HibernateUtils.escapeSQLLikeStatement(keyword);

            pageCompanyEntity = companyRepository.relativeSearching(keywordFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCompanyDTO = helper.mapToPageCompanyDTO(pageCompanyEntity);
        }

        return pageCompanyDTO;
    }

    @Override
    public List<ListCompanyDTO> getAllCompany() {
        List<ListCompanyDTO> listCompanyDTOs = new ArrayList<>();
        List<Company> listCompany = companyRepository.findAllByDeleteFlagIs(1);
        for (Company company : listCompany) {
            listCompanyDTOs.add(helper.mapToListCompanyDTO(company));
        }
        return listCompanyDTOs;
    }

    @Override
    public void saveNewCompany(CompanyCreatingDTO companyCreatingDTO) throws CompanyAlreadyExistException {
        boolean isExist = companyRepository.findByEmailAndDeleteFlagIs(companyCreatingDTO.getEmail(), 1).isPresent();
        boolean isDeleted = companyRepository.findByEmailAndDeleteFlagIs(companyCreatingDTO.getEmail(), 0).isPresent();
        if (!isExist) {
            Company company = Helper.mapToCompanyEntity(companyCreatingDTO);
            company.setCompanyType(companyTypeRepository.findByName(companyCreatingDTO.getType()));
            companyRepository.save(company);
        } else if (isDeleted) {
            Company companyRestore = companyRepository.findByEmailAndDeleteFlagIs(companyCreatingDTO.getEmail(), 0).get();
            companyRestore.setDeleteFlag(1);
            companyRepository.save(companyRestore);
        } else {
            throw new CompanyAlreadyExistException();
        }
    }

    @Override
    public void deleteCompany(String id) throws DeleteCompanyNotFoundException {
        int companyId = Integer.parseInt(id);
        Optional<Company> companyOpt = companyRepository.findByCompanyIdAndDeleteFlagIs(companyId, 1);
        if (companyOpt.isPresent()) {
            Company company = companyOpt.get();
            company.setDeleteFlag(0);
            companyRepository.save(company);
        }else{
            throw new DeleteCompanyNotFoundException();
        }
    }

    @Override
    public UpdateCompanyDTO getCompanyToUpdate(String id) throws CompanyNotFoundException {
        int companyId = Integer.parseInt(id);
        Optional<Company> companyOpt = companyRepository.findByCompanyIdAndDeleteFlagIs(companyId, 1);
        if (companyOpt.isPresent()) {
            UpdateCompanyDTO updateCompanyDTO = Helper.mapToUpdateCompanyDTO(companyOpt.get());
            updateCompanyDTO.setCompanyId(companyId);
            updateCompanyDTO.setType(companyOpt.get().getCompanyType().getName());
            return updateCompanyDTO;
        }
        throw new CompanyNotFoundException();
    }

    @Override
    public boolean updateCompanyInfo(String id, UpdateCompanyDTO updateCompanyDTO) {
        int companyId =Integer.parseInt(id);
        Optional<Company> companyOpt=companyRepository.findByCompanyIdAndDeleteFlagIs(companyId,1);
        boolean isExist= companyOpt.isPresent();
        if(isExist){
            updateCompanyDTO.setCompanyId(companyId);
            Company company=Helper.mapToCompanyEntity(updateCompanyDTO);
            company.setCreatedDate(companyOpt.get().getCreatedDate());
            company.setCreatedBy(companyOpt.get().getCreatedBy());
            company.setLastModifiedDate(new Date().toInstant());
            company.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().get());
            company.setDeleteFlag(1);
            company.setCompanyType(companyTypeRepository.findByName(updateCompanyDTO.getType()));
            companyRepository.save(company);
            return true;
        }
        return false;
    }

    @Override
    public List<CompanyType> getAllCompanyType() {
        return companyTypeRepository.findAllByDeleteFlagIs(1).get();
    }

    @Override
    public Integer countCompany() {
        List<Company> listCompany = companyRepository.findAllByDeleteFlagIs(1);
        Integer size = listCompany.size();
        return size;
    }

    @Override
    public List<String> getAllCompanyLocation() {
       return companyRepository.findDistinctCity();
    }

    @Override
    public Page<ListCompanyDTO> filterCompanyByTypeAndCity(String city, String companyType, String pageNum) {
        Page<Company> pageCompanyEntity;
        Page<ListCompanyDTO> pageCompanyDTO;
        Integer pageNumber = Integer.parseInt(pageNum);
        // check pageNumber
        Page<Company> allCompany = companyRepository.findAllByDeleteFlagIs(1, null);
        int sizeList = allCompany.getSize();
        pageNumber = Helper.checkPageNumber(sizeList, pageNumber);

        Optional<CompanyType> companyTypeOptional = companyTypeRepository.findByNameAndDeleteFlagIs(companyType,1);

        // get all Company
        if ((city == null || "".equals(city)) && companyTypeOptional.isEmpty()) {

            pageCompanyEntity = companyRepository.findAllByDeleteFlagIs(1,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE, Sort.by(Sort.Direction.DESC, "companyId")));

            pageCompanyDTO = helper.mapToPageCompanyDTO(pageCompanyEntity);

            // get company with city
        } else if ((city != null && (!"".equals(city))) && companyTypeOptional.isEmpty()) {
            String cityFinal = HibernateUtils.escapeSQLLikeStatement(city);

            pageCompanyEntity = companyRepository.relativeCityFiltering(cityFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCompanyDTO = helper.mapToPageCompanyDTO(pageCompanyEntity);

            // get company with companyType
        } else if(companyTypeOptional.isPresent() && (city == null || "".equals(city))) {
            Integer companyTypeId = companyTypeOptional.get().getCompanyTypeId();

            pageCompanyEntity = companyRepository.relativeCompanyTypeFiltering(companyTypeId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCompanyDTO = helper.mapToPageCompanyDTO(pageCompanyEntity);
        }else{
            String cityFinal = HibernateUtils.escapeSQLLikeStatement(city);
            Integer companyTypeId = companyTypeOptional.get().getCompanyTypeId();

            pageCompanyEntity = companyRepository.relativeFiltering(cityFinal.toLowerCase(),companyTypeId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageCompanyDTO = helper.mapToPageCompanyDTO(pageCompanyEntity);

        }

        return pageCompanyDTO;
    }

    @Override
    public Company getCompanyByCompanyId(String id) throws CompanyNotFoundException {
        Integer companyId= Integer.parseInt(id);
        Optional<Company> companyOptional = companyRepository.findByCompanyIdAndDeleteFlagIs(companyId,1);
        if(companyOptional.isPresent()){
            return companyOptional.get();
        }
        throw new CompanyNotFoundException();
    }

    @Override
    public List<CompanyDetailDTO> getTopListCompanies() {
        List<Company> companyEntityList = entityManager.createNativeQuery("SELECT * from company c WHERE c.delete_flag=1 order by total_rating DESC", Company.class).setMaxResults(10).getResultList();
        List<CompanyDetailDTO> companyList = new ArrayList<>();
        for(Company company:companyEntityList){
            CompanyDetailDTO companyDetailDTO =Helper.mapToCompanyDetailDTO(company);
            companyList.add(companyDetailDTO);
        }
       return companyList;
    }

}
