package com.project.recruitment.service;

import com.project.recruitment.dto.CandidateJobDTO;
import com.project.recruitment.exception.ApplyJobNotFoundException;
import com.project.recruitment.exception.CandidateNotFoundException;
import com.project.recruitment.exception.LogoutException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface CandidateJobService {
    void saveApplyJob(CandidateJobDTO candidateJobDTO, Integer jobId, String cvPath) throws ApplyJobNotFoundException, LogoutException, CandidateNotFoundException;

    boolean saveApplyOnlineCVFile(MultipartFile multipartFile, String fileName) throws IOException;
}
