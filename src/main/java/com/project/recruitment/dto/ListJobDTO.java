package com.project.recruitment.dto;

import com.project.recruitment.entity.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class ListJobDTO {

    private Integer jobId;

    private String city;

    private JobType jobType;

    private String jobTitle;

    private String deadline;

    private JobLevel jobLevel;

    private Skill skill;

    private Position position;

    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public JobLevel getJobLevel() {
        return jobLevel;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Skill getSkill() {
        return skill;
    }

    public Position getPosition() {
        return position;
    }

    public void setJobLevel(JobLevel jobLevel) {
        this.jobLevel = jobLevel;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public JobType getJobType() {
        return jobType;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

}
