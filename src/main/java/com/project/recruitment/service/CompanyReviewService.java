package com.project.recruitment.service;

import com.project.recruitment.dto.CompanyReviewDTO;
import com.project.recruitment.dto.ListCompanyReviewDTO;
import com.project.recruitment.entity.Company;
import com.project.recruitment.exception.CompanyNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CompanyReviewService {
    boolean saveCompanyReview(String id, CompanyReviewDTO companyReviewDTO) throws CompanyNotFoundException;
    Page<ListCompanyReviewDTO> listAllReviewByCompanyId(String companyId, String pageNumber) throws CompanyNotFoundException;
    Company getCompanyToReview(String companyId) throws CompanyNotFoundException;
}
