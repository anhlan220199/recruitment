package com.project.recruitment.controller;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.constants.PathConstant;
import com.project.recruitment.constants.UrlConstant;
import com.project.recruitment.dto.DashboardCandidateDTO;
import com.project.recruitment.dto.DashboardJobDTO;
import com.project.recruitment.dto.EmployerProfileEditDTO;
import com.project.recruitment.dto.ListCandidateDTO;
import com.project.recruitment.entity.Job;
import com.project.recruitment.exception.*;
import com.project.recruitment.service.EmailService;
import com.project.recruitment.service.EmployerService;
import com.project.recruitment.service.JobService;
import com.project.recruitment.service.UserAppliedJobService;
import org.apache.commons.io.FileUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Pattern;
import java.io.*;
import java.util.List;

@Controller
@Validated
@RequestMapping("/employer/dashboard")
public class EmployerDashBoardController {
    private final EmployerService employerService;
    private final JobService jobService;
    private final UserAppliedJobService userAppliedJobService;
    private final EmailService emailService;

    public EmployerDashBoardController(EmployerService employerService, JobService jobService, UserAppliedJobService userAppliedJobService, EmailService emailService) {
        this.employerService = employerService;
        this.jobService = jobService;
        this.userAppliedJobService = userAppliedJobService;
        this.emailService = emailService;
    }

    @GetMapping("/")
    public String viewDashBoard(Model model) throws EmployerNotFoundException {
        model.addAttribute("employer", employerService.getEmployerProfileToEdit());
        model.addAttribute("totalJobPosted",jobService.getTotalJobPostedThisMonth());
        return "company/company-dashboard";
    }

    @GetMapping("/profile/edit")
    public String viewYourProfile(Model model) throws EmployerNotFoundException {
        model.addAttribute("employer", employerService.getEmployerProfileToEdit());
        return "company/company-dashboard-edit-profile";
    }

    @PostMapping("/profile/edit/{employerId}")
    public String updateYourProfile(@Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Employer ID is number only!") @PathVariable("employerId") String employerId, @ModelAttribute("employer") EmployerProfileEditDTO employerProfileEditDTO, BindingResult bindingResult, Model model) throws EmployerNotFoundException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("employer", employerProfileEditDTO);
            return "company/company-dashboard-edit-profile";
        }
        boolean isUpdateSuccess = employerService.editEmployerProfile(employerId, employerProfileEditDTO);
        if (isUpdateSuccess) {
            model.addAttribute("isSuccess", true);
            model.addAttribute("updateSuccess", CommonConstant.UserMessage.UPDATE_SUCCESS);
            model.addAttribute("employer", employerProfileEditDTO);
        }
        model.addAttribute("employer", employerService.getEmployerProfileToEdit());
        return "company/company-dashboard-edit-profile";
    }

    @PostMapping("/password/change")
    public String changeYourPassword(@ModelAttribute("employer") EmployerProfileEditDTO employerProfileEditDTO, BindingResult bindingResult, Model model) throws EmployerNotFoundException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("employer", employerProfileEditDTO);
            return "company/company-dashboard-edit-profile";
        }
        BindingResult result = employerService.changePassword(employerProfileEditDTO, bindingResult);
        if (!result.hasFieldErrors()) {
            model.addAttribute("isPasswordChangedSuccess", true);
            model.addAttribute("changePasswordSuccess", CommonConstant.UserMessage.CHANGE_SUCCESS);
            model.addAttribute("employer", employerProfileEditDTO);
            return "company/company-dashboard-edit-profile";
        }
        model.addAttribute("employer", employerProfileEditDTO);
        return "company/company-dashboard-edit-profile";
    }

    @GetMapping("/my-account/delete")
    public String DeleteMyAccount(Model model, RedirectAttributes redirectAttributes) throws EmployerNotFoundException {
        boolean isDeleted = employerService.deleteMyAccount();
        redirectAttributes.addFlashAttribute("isAccountDeleted", isDeleted);
        redirectAttributes.addFlashAttribute("message","Your account have been deleted!");
        return "redirect:/register/company";
    }

    @GetMapping("/candidate/manage")
    public String getListCandidate(Model model,
                                   @Pattern(regexp = "^-?[0-9]\\d*(\\d+)?$", message = "Page number is number only!")
                                   @RequestParam(defaultValue = "1", value = "pageNumber", required = true) String pageNumber,
                                   @RequestParam(value = "keyword", required = true) String keyword) {
        Page<DashboardCandidateDTO> page = employerService.listAllAppliedCandidate(keyword, pageNumber);

        model.addAttribute("page", page);
        model.addAttribute("isEmptyListCandidate", page.isEmpty());
        model.addAttribute("pageSize", CommonConstant.PAGE_SIZE);
        model.addAttribute("keyword", keyword);
        return "company/company-dashboard-manage-candidate";
    }

    @GetMapping("/job/manage")
    public String getManageJobForm(Model model,
                                   @Pattern(regexp = "^-?[0-9]\\d*(\\d+)?$", message = "Page number is number only!")
                                   @RequestParam(defaultValue = "1", value = "pageNumber", required = true) String pageNumber,
                                   @RequestParam(value = "keyword", required = true) String keyword) {
        Page<DashboardJobDTO> page = employerService.listAllJob(keyword, pageNumber);

        model.addAttribute("page", page);
        model.addAttribute("isEmptyListJob", page.isEmpty());
        model.addAttribute("pageSize", CommonConstant.PAGE_SIZE);
        model.addAttribute("keyword", keyword);
        return "company/company-dashboard-manage-job";
    }

    @GetMapping("/delete/{jobId}")
    public String handleDeleteJob(
            @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Job ID is number only!") @PathVariable("jobId") String jobId) throws DeleteJobNotFoundException, AccessDenied {
        jobService.deleteJob(jobId);
        return UrlConstant.getRedirectUrl("/employer/dashboard/job/manage?pageNumber=1&keyword=");
    }

    @PostMapping("/contact/{userAppliedJobId}")
    public String sendEmailToUserAppliedJob(
            HttpServletRequest request,
            @PathVariable("userAppliedJobId") Integer userAppliedJobId, Model model) throws MessagingException, UserAppliedJobNotFoundException {
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        String message = request.getParameter("message");
        emailService.sendEmailToUserAppliedJobOfCompany(email, name, message, userAppliedJobId);
        model.addAttribute("isEmailSend",true);
        model.addAttribute("sendEmailSuccessMessage","Email is sent!");
        return "company/company-dashboard-manage-candidate";
    }

}
