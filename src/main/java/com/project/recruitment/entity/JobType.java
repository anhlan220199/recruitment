package com.project.recruitment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "job_type", schema = "recruitment_db", catalog = "")
public class JobType {
    private int jobTypeId;
    private String name;
    private Set<Job> listJob;
    private Set<User> listUser;

    @Id
    @Column(name = "job_type_id")
    public int getJobTypeId() {
        return jobTypeId;
    }

    public void setJobTypeId(int jobTypeId) {
        this.jobTypeId = jobTypeId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "jobType")
    public Set<Job> getListJob() {
        return listJob;
    }

    public void setListJob(Set<Job> listJob) {
        this.listJob = listJob;
    }

    @OneToMany(mappedBy = "jobType")
    public Set<User> getListUser() {
        return listUser;
    }

    public void setListUser(Set<User> listUser) {
        this.listUser = listUser;
    }
}
