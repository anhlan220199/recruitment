package com.project.recruitment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/common")
public class CommonController {

    @GetMapping("/contact")
    public String getContactPage(){
        return "/others/contact";
    }

    @GetMapping("/aboutus")
    public String getAboutUsPage(){
        return "/others/about-us";
    }
    @GetMapping("/faq")
    public String getFAQPage(){
        return "/others/faq";
    }

    @GetMapping("/terms")
    public String getTermsAndConditionPage(){
        return "/others/terms-and-condition";
    }

    @GetMapping("/work")
    public String getHowItWorkPage(){
        return "/others/how-it-work";
    }
}
