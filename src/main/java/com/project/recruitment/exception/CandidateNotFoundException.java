//
package com.project.recruitment.exception;

/**
 * This class is . 
 * 
 * @Description: .
 * @author: NVAnh
 * @create_date: Nov 11, 2020
 * @version: 1.0
 * @modifer: NVAnh
 * @modifer_date: Nov 11, 2020
 */
public class CandidateNotFoundException extends NotFoundException{

	private static final long serialVersionUID = 2418404265782791165L;

}
