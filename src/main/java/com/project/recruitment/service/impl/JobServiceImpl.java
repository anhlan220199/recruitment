package com.project.recruitment.service.impl;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.dto.*;
import com.project.recruitment.entity.*;
import com.project.recruitment.exception.*;
import com.project.recruitment.helper.Helper;
import com.project.recruitment.repository.*;
import com.project.recruitment.service.JobService;
import com.project.recruitment.util.DateTimeUtils;
import com.project.recruitment.util.HibernateUtils;
import com.project.recruitment.util.SecurityUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class JobServiceImpl implements JobService {

    private final JobRepository jobRepository;
    private final JobLevelRepository jobLevelRepository;
    private final JobTypeRepository jobTypeRepository;
    private final SkillRepository skillRepository;
    private final PositionRepository positionRepository;
    private final CompanyRepository companyRepository;
    private final CurrencyRepository currencyRepository;
    private final GenderRepository genderRepository;
    private final UserRepository userRepository;
    private final FavoriteJobRepository favoriteJobRepository;
    private final RoleRepository roleRepository;
    private final EntityManager entityManager;

    private final Helper helper = new Helper();
    ModelMapper modelMapper = new ModelMapper();

    public JobServiceImpl(JobRepository jobRepository, JobLevelRepository jobLevelRepository, JobTypeRepository jobTypeRepository, SkillRepository skillRepository, PositionRepository positionRepository, CompanyRepository companyRepository, CurrencyRepository currencyRepository, GenderRepository genderRepository, UserRepository userRepository, FavoriteJobRepository favoriteJobRepository, RoleRepository roleRepository, EntityManager entityManager) {
        this.jobRepository = jobRepository;
        this.jobLevelRepository = jobLevelRepository;
        this.jobTypeRepository = jobTypeRepository;
        this.skillRepository = skillRepository;
        this.positionRepository = positionRepository;
        this.companyRepository = companyRepository;
        this.currencyRepository = currencyRepository;
        this.genderRepository = genderRepository;
        this.userRepository = userRepository;
        this.favoriteJobRepository = favoriteJobRepository;
        this.roleRepository = roleRepository;
        this.entityManager = entityManager;
    }

    @Override
    public Page<ListJobDTO> listAll(String keyword, String pageNumbers)
            throws IllegalArgumentException {

        Integer pageNumber = Integer.parseInt(pageNumbers);

        Page<Job> pageJobEntity = null;
        Page<ListJobDTO> pageJobDTO = null;

        // check pageNumber
        Page<Job> allJob = jobRepository.findAllByDeleteFlagIs(1, null);
        int sizeList = allJob.getSize();
        pageNumber = Helper.checkPageNumber(sizeList, pageNumber);

        // get all Job
        if (keyword == null || "".equals(keyword)) {

            pageJobEntity = jobRepository.findAllByDeleteFlagIs(1,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE, Sort.by(Sort.Direction.DESC, "jobId")));

            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

            // get job with keyword
        } else {
            String keywordFinal = HibernateUtils.escapeSQLLikeStatement(keyword);

            pageJobEntity = jobRepository.relativeSearching(keywordFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);
        }

        return pageJobDTO;

    }

    @Override
    public ListJobDTO findJobById(Integer jobID) {
        Optional<Job> job = jobRepository.findById(jobID);
        if (!job.isPresent()) {
            return null;
        }
        ModelMapper modelMapper = new ModelMapper();
        ListJobDTO lstJobDTO = modelMapper.map(job.get(), ListJobDTO.class);
        return lstJobDTO;
    }

    @Override
    public JobDetailDTO getJobDetailById(int jobId) throws PageNotFoundException {
        Optional<Job> jobOpt = jobRepository.findByJobIdAndDeleteFlagIs(jobId, 1);
        if (!jobOpt.isPresent()) {
            throw new PageNotFoundException();
        }
        Job jobEntity = jobOpt.get();
        String postJobDate = DateTimeUtils.instantToString(jobEntity.getCreatedDate(), CommonConstant.FORMAT_DATE);
        String deadline = DateTimeUtils.instantToString(jobEntity.getDeadline(), CommonConstant.FORMAT_DATE);
        JobDetailDTO jobDetail = Helper.mapToJobDetailDTO(jobEntity);
        jobDetail.setJobType(jobEntity.getJobType().getName());
        jobDetail.setCurrency(jobEntity.getCurrency().getName());
        jobDetail.setGender(jobEntity.getGender().getName());
        jobDetail.setCreatedDate(postJobDate);
        jobDetail.setDeadline(deadline);
        return jobDetail;
    }

    @Override
    public void saveJob(PostJobDTO newJob) throws ParseException {

        Job job = Helper.mapToEntity(newJob);

        // set deadline --> job
        LocalDateTime deadline = DateTimeUtils.convertStringToDate(newJob, CommonConstant.FORMAT_DATE);
        job.setDeadline(deadline.toInstant(ZoneOffset.UTC));

        // set Company
        job.setCompany(userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(), 1).get().getCompany());

        // set Currency
        job.setCurrency(currencyRepository.findByName(newJob.getCurrency()));

        // set Gender
        job.setGender(genderRepository.findByName(newJob.getGender()));

        // set JobLevel
        job.setJobLevel(jobLevelRepository.findByJobLevelId(newJob.getJoblevelId()));

        // set JobType
        job.setJobType(jobTypeRepository.findByName(newJob.getJobType()).get());

        // set Position
        job.setPosition(positionRepository.findByPositionId(newJob.getPositionId()));

        // set Skill
        job.setSkill(skillRepository.findBySkillId(newJob.getSkillId()).get());

        // set Delete Flag
        job.setDeleteFlag(1);

        // set Job Title
        Optional<JobLevel> jobLevel = jobLevelRepository.findById(newJob.getJoblevelId());
        Optional<Skill> skill = skillRepository.findById(newJob.getSkillId());
        Optional<Position> position = positionRepository.findById(newJob.getPositionId());

        if (jobLevel.isPresent() && skill.isPresent() && position.isPresent()) {
            String jobTitle = jobLevel.get().getName() + " " + skill.get().getName() + " " + position.get().getName();
            job.setJobTitle(jobTitle);
        }

        // save Job
        jobRepository.save(job);
    }

    @Override
    public List<SimilarJobDTO> getSimilarJobs(Skill skill) {
        List<SimilarJobDTO> similarJobDTOList = new ArrayList<>();
        List<Job> jobList = jobRepository.findJobsBySkillOrderByJobIdDesc(skill);
        if (!jobList.isEmpty()) {
            for (Job job : jobList) {
                String deadline = DateTimeUtils.instantToString(job.getDeadline(), CommonConstant.FORMAT_DATE);
                SimilarJobDTO similarJobDTO = Helper.mapToSimilarJobDTO(job);
                similarJobDTO.setDeadline(deadline);
//				similarJobDTO.setCompanyName(job.getCompany().getName());
                similarJobDTO.setCompany(job.getCompany());
                similarJobDTOList.add(similarJobDTO);
            }
            return similarJobDTOList;
        }
        return null;
    }

    @Override
    public void deleteJob(String id) throws AccessDenied,DeleteJobNotFoundException {
        Integer jobId = Integer.parseInt(id);
        Optional<Job> jobEntity = jobRepository.findByJobIdAndDeleteFlagIs(jobId, 1);
        Optional<User> userOptional = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(),1);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            String roleName = user.getRole().getName();
            if(jobEntity.isPresent()){
                Company company = jobEntity.get().getCompany();
                boolean isUserBelongToCompany=company.getListUser().contains(user);
                if(roleName.equals("ADMIN_SYSTEM") || (isUserBelongToCompany && roleName.equals("ADMIN_COMPANY"))){
                    jobEntity.get().setDeleteFlag(0);
                    jobRepository.save(jobEntity.get());
                } else {
                    throw new AccessDenied();
                }
            } else {
                throw new DeleteJobNotFoundException();
            }
        }
    }

    @Override
    public void editJob(UpdateJobDTO editJob, String jobIdString) throws ParseException,AccessDenied,JobNotFoundException {
        Integer jobId = Integer.parseInt(jobIdString);
        Optional<Job> jobEntity = jobRepository.findByJobIdAndDeleteFlagIs(jobId, 1);
        Optional<User> userOptional = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(),1);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            String roleName = user.getRole().getName();
            if(jobEntity.isPresent()){
                Company company = jobEntity.get().getCompany();
                boolean isUserBelongToCompany=company.getListUser().contains(user);
                if(roleName.equals("ADMIN_SYSTEM") || (isUserBelongToCompany && roleName.equals("ADMIN_COMPANY"))){
                    ModelMapper modelMapper = new ModelMapper();
                    modelMapper.map(editJob, jobEntity);
                    // set Deadline
                    Date deadline = DateTimeUtils.convertStringToDateUpdate(editJob, CommonConstant.FORMAT_DATE);
                    jobEntity.get().setDeadline(deadline.toInstant());

                    // set salary
                    jobEntity.get().setSalary(Integer.parseInt(editJob.getSalary()));

                    jobEntity.get().setCity(editJob.getCity());
                    jobEntity.get().setExperience(editJob.getExperience());
                    jobEntity.get().setRequirement(editJob.getRequirement());
                    jobEntity.get().setDescription(editJob.getDescription());
                    jobEntity.get().setOtherBenefit(editJob.getOtherBenefit());
                    // set Company
                    jobEntity.get().setCompany(userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(), 1).get().getCompany());

                    // set Currency
                    jobEntity.get().setCurrency(currencyRepository.findByName(editJob.getCurrency()));

                    // set Gender
                    jobEntity.get().setGender(genderRepository.findByName(editJob.getGender()));

                    // set JobLevel
                    jobEntity.get().setJobLevel(jobLevelRepository.findByJobLevelId(editJob.getJoblevelId()));

                    // set JobType
                    jobEntity.get().setJobType(jobTypeRepository.findByName(editJob.getJobType()).get());

                    // set Position
                    jobEntity.get().setPosition(positionRepository.findByPositionId(editJob.getPositionId()));

                    // set Skill
                    jobEntity.get().setSkill(skillRepository.findBySkillId(editJob.getSkillId()).get());

                    // set Delete Flag
                    jobEntity.get().setDeleteFlag(1);

                    Optional<JobLevel> jobLevel = jobLevelRepository.findById(editJob.getJoblevelId());
                    Optional<Skill> skill = skillRepository.findById(editJob.getSkillId());
                    Optional<Position> position = positionRepository.findById(editJob.getPositionId());
                    if (jobLevel.isPresent() && skill.isPresent() && position.isPresent()) {
                        String jobTitle = jobLevel.get().getName() + " " + skill.get().getName() + " " + position.get().getName();
                        jobEntity.get().setJobTitle(jobTitle);
                    }
                    jobRepository.save(jobEntity.get());
                } else {
                    throw new AccessDenied();
                }
            } else {
                throw new JobNotFoundException();
            }
        }

    }

    @Override
    public UpdateJobDTO getUpdateJobDetailById(Integer jobId) throws Exception {
        Optional<Job> job = jobRepository.findByJobIdAndDeleteFlagIs(jobId, 1);
        if (job.isEmpty()) {
            throw new Exception();
        }

        User currentUser = getCurrentUser();

        boolean isTrueJob = (currentUser.getCompany().getCompanyId() == job.get().getCompany().getCompanyId());
        boolean isAdmin = (currentUser.getRole().getName().equals("ADMIN_SYSTEM"));

        if (!isTrueJob && !isAdmin){
            throw new AccessDenied();
        }

        UpdateJobDTO updateJobDTO = Helper.mapJobToUpdateJobDTO(job.get());
        updateJobDTO.setDeadline(DateTimeUtils.instantToString(job.get().getDeadline(), CommonConstant.FORMAT_DATE));
        updateJobDTO.setJobType(job.get().getJobType().getName());
        updateJobDTO.setCurrency(job.get().getCurrency().getName());
        updateJobDTO.setGender(job.get().getGender().getName());
        return updateJobDTO;
    }

    @Override
    public List<Skill> getAllSkill() {
        return skillRepository.findAll();
    }

    @Override
    public List<Position> getAllJobPosition() {
        return positionRepository.findAllByDeleteFlagIs(1);
    }

    @Override
    public List<JobLevel> getAllJobLevel() {
        return jobLevelRepository.findAll();
    }

    @Override
    public List<JobType> getAllJobType() {
        return jobTypeRepository.findAll();
    }

    @Override
    public List<String> getAllJobLocation() {
        return jobRepository.findDistinctCity();
    }

    @Override
    public List<Company> getAllCompany() {
        return companyRepository.findAllByDeleteFlagIs(1);
    }

    @Override
    public Integer countJobs() {
        List<Job> listAllJobs = jobRepository.findAllByDeleteFlagIs(1);
        Integer count = listAllJobs.size();
        return count;
    }

    @Override
    public List<ListJobDTO> getListRecentJobFirst() {
        List<Job> listRecentJob = jobRepository.getListRecentJob();
        List<ListJobDTO> listJobDTOs = new ArrayList<>();
        for (Integer i=0; i<5; i++) {
            ListJobDTO listJobDTO = helper.mapJobEntityToListJobDTO(listRecentJob.get(i));
            listJobDTOs.add(listJobDTO);
        }
        return listJobDTOs;
    }

    @Override
    public List<ListJobDTO> getListRecentJobSecond() {
        List<Job> listRecentJob = jobRepository.getListRecentJob();
        List<ListJobDTO> listJobDTOs = new ArrayList<>();
        for (Integer i=5; i<10; i++) {
            ListJobDTO listJobDTO = helper.mapJobEntityToListJobDTO(listRecentJob.get(i));
            listJobDTOs.add(listJobDTO);
        }
        return listJobDTOs;
    }

    @Override
    public List<ListJobDTO> getListFeatureJobFirst() {
        List<Job> listFeatureJob = jobRepository.getListFeatureJob();
        List<ListJobDTO> listJobDTOs = new ArrayList<>();
        for (Integer i=0; i<5; i++) {
            ListJobDTO listJobDTO = helper.mapJobEntityToListJobDTO(listFeatureJob.get(i));
            listJobDTOs.add(listJobDTO);
        }
        return listJobDTOs;
    }

    @Override
    public List<ListJobDTO> getListFeatureJobSecond() {
        List<Job> listFeatureJob = jobRepository.getListFeatureJob();
        List<ListJobDTO> listJobDTOs = new ArrayList<>();
        for (Integer i=5; i<10; i++) {
            ListJobDTO listJobDTO = helper.mapJobEntityToListJobDTO(listFeatureJob.get(i));
            listJobDTOs.add(listJobDTO);
        }
        return listJobDTOs;
    }
    
    public List<UserAppliedJob> getListUserAppliedJob(Integer jobId) throws JobNotFoundException {
        Optional<Job> jobOpt = jobRepository.findByJobIdAndDeleteFlagIs(jobId, 1);
        if (jobOpt.isEmpty()){
            throw new JobNotFoundException();
        }
        List<UserAppliedJob> lstUserAppliedJob = jobOpt.get().getListUserAppliedJob();
        return lstUserAppliedJob;
    }

    @Override
    public User getCurrentUser() throws CandidateNotFoundException {
        Optional<User> currentUser = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(), 1);
        if (currentUser.isEmpty()) {
            throw new CandidateNotFoundException();
        }
        return currentUser.get();
    }

    @Override
    public List<Integer> getListJobIdCurrentCompany() throws CandidateNotFoundException {
        List<Integer> listJobId = new ArrayList<>();
        Optional<String> username = SecurityUtils.getCurrentUserLogin();
        if("anonymousUser".equals(username.get())) {
            return listJobId;
        }
        User currentUser = getCurrentUser();
        if(currentUser.getRole().getName().equals(CommonConstant.ADMIN_COMPANY)){
            List<Job> lstJob = currentUser.getCompany().getListJob();
            for (Job job : lstJob ) {
                listJobId.add(job.getJobId());
            }
        }
        return listJobId;
    }

    @Override
    public Page<ListJobDTO> filterJob(String skill, String level, String position, String city, String jobType,String companyName, Integer salaryMin, Integer salaryMax, String pageNum)
            throws IllegalArgumentException {

        Page<Job> pageJobEntity;
        Page<ListJobDTO> pageJobDTO = null;
        Integer pageNumber = Integer.parseInt(pageNum);
        // check pageNumber
        Page<Job> allJob = jobRepository.findAllByDeleteFlagIs(1, null);
        int sizeList = allJob.getSize();
        pageNumber = Helper.checkPageNumber(sizeList, pageNumber);


        String skillFinal = HibernateUtils.escapeSQLLikeStatement(skill);
        Optional<Skill> skillOptional=skillRepository.findByName(skillFinal);

        String levelFinal = HibernateUtils.escapeSQLLikeStatement(level);
        Optional<JobLevel> jobLevelOptional = jobLevelRepository.findByName(levelFinal);

        String positionFinal = HibernateUtils.escapeSQLLikeStatement(position);
        Optional<Position> positionOptional = positionRepository.findByNameAndDeleteFlag(positionFinal, 1);

        String companyNameFinal = HibernateUtils.escapeSQLLikeStatement(companyName);
        Optional<Company> companyOptional = companyRepository.findByNameAndDeleteFlag(companyNameFinal, 1);

        String cityFinal = HibernateUtils.escapeSQLLikeStatement(city);

        String jobTypeFinal = HibernateUtils.escapeSQLLikeStatement(jobType);
        Optional<JobType> jobTypeOptional = jobTypeRepository.findByName(jobTypeFinal);

        // get job with skill
         if (skillOptional.isPresent() && jobLevelOptional.isEmpty() && positionOptional.isEmpty() && companyOptional.isEmpty() && jobTypeOptional.isEmpty() && (city == null || "".equals(city))) {
            Integer skillId = skillOptional.get().getSkillId();
            pageJobEntity = jobRepository.relativeJobFilteringBySkill(skillId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

            //get job with level
        } else if (skillOptional.isEmpty() && jobLevelOptional.isPresent() && positionOptional.isEmpty() && companyOptional.isEmpty() && jobTypeOptional.isEmpty() && (city == null || "".equals(city))) {
            Integer jobLevelId = jobLevelOptional.get().getJobLevelId();
            pageJobEntity = jobRepository.relativeJobFilteringByJoblevel(jobLevelId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

            //get job with position
        } else if (skillOptional.isEmpty() && jobLevelOptional.isEmpty() && positionOptional.isPresent() && companyOptional.isEmpty() && jobTypeOptional.isEmpty() && (city == null || "".equals(city))) {
            Integer positionId = positionOptional.get().getPositionId();
            pageJobEntity = jobRepository.relativeJobFilteringByPosition(positionId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

            //get job by city
        } else if (skillOptional.isEmpty() && jobLevelOptional.isEmpty() &&  positionOptional.isEmpty() && companyOptional.isEmpty() && jobTypeOptional.isEmpty() && (city !=null || !"".equals(city))) {
            pageJobEntity = jobRepository.relativeJobFilteringByCity(cityFinal.toLowerCase(),
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

            //get job by jobType
        }else if (skillOptional.isEmpty() && jobLevelOptional.isEmpty() &&  positionOptional.isEmpty() && companyOptional.isEmpty() && jobTypeOptional.isPresent() && (city == null || "".equals(city))) {
            Integer jobTypeId= jobTypeOptional.get().getJobTypeId();
            pageJobEntity = jobRepository.relativeJobFilteringByJobType(jobTypeId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

            //get job by companyId
        }else if (skillOptional.isEmpty() && jobLevelOptional.isEmpty() &&  positionOptional.isEmpty() && companyOptional.isPresent() && jobTypeOptional.isEmpty() && (city == null || "".equals(city))) {
            Integer companyId = companyOptional.get().getCompanyId();
            pageJobEntity = jobRepository.relativeJobFilteringByCompany(companyId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

            //filter job by skill and position ( Vd Java Developer)
        } else if (skillOptional.isPresent() && jobLevelOptional.isEmpty() &&  positionOptional.isPresent() && companyOptional.isEmpty() && jobTypeOptional.isEmpty() && (city == null || "".equals(city))) {
            Integer skillId = skillOptional.get().getSkillId();
            Integer positionId = positionOptional.get().getPositionId();
            pageJobEntity = jobRepository.relativeJobFilteringBySkillAndPosition(skillId,positionId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

            //filter job by skill and level and position ( Vd Senior Java Developer)
        } else if (skillOptional.isPresent() && jobLevelOptional.isPresent() &&  positionOptional.isPresent() && companyOptional.isEmpty() && jobTypeOptional.isEmpty() && (city == null || "".equals(city))) {
            Integer skillId = skillOptional.get().getSkillId();
            Integer jobLevelId = jobLevelOptional.get().getJobLevelId();
            Integer positionId = positionOptional.get().getPositionId();
            pageJobEntity = jobRepository.relativeJobFilteringBySkillAndLevelAndPosition(skillId,jobLevelId,positionId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

            //filter job by skill and position and company ( Vd Java Developer, Cty VTI)
        }else if (skillOptional.isPresent() && jobLevelOptional.isEmpty() &&  positionOptional.isPresent() && companyOptional.isPresent() && jobTypeOptional.isEmpty() && (city == null || "".equals(city))) {
            Integer skillId = skillOptional.get().getSkillId();
            Integer positionId = positionOptional.get().getPositionId();
            Integer companyId = companyOptional.get().getCompanyId();
            pageJobEntity = jobRepository.relativeJobFilteringBySkillAndPositionAndCompany(skillId,positionId,companyId,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);

             //filter job by skill and company ( Vd Java, Cty VTI)
        } else if (skillOptional.isPresent() && jobLevelOptional.isEmpty() &&  positionOptional.isEmpty() && companyOptional.isPresent() && jobTypeOptional.isEmpty() && (city == null || "".equals(city))) {
             Integer skillId = skillOptional.get().getSkillId();
             Integer companyId = companyOptional.get().getCompanyId();
             pageJobEntity = jobRepository.relativeJobFilteringBySkillAndCompany(skillId,companyId,
                     PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
             pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);
         }else if(skillOptional.isPresent() && jobLevelOptional.isPresent() &&  positionOptional.isPresent() && companyOptional.isPresent() && jobTypeOptional.isPresent() && (city != null || !"".equals(city))){
            Integer skillId = skillOptional.get().getSkillId();
            Integer jobLevelId = jobLevelOptional.get().getJobLevelId();
            Integer positionId = positionOptional.get().getPositionId();
            Integer companyId = companyOptional.get().getCompanyId();
            Integer jobTypeId= jobTypeOptional.get().getJobTypeId();

            pageJobEntity = jobRepository.relativeJobFiltering(skillId,positionId,jobLevelId,cityFinal.toLowerCase(),jobTypeId,companyId,salaryMin,salaryMax, PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));
            pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);
        }else{
             // get all Job
             pageJobEntity = jobRepository.findAllByDeleteFlagIs(1,
                     PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE, Sort.by(Sort.Direction.DESC, "jobId")));

             pageJobDTO = helper.mapToPageJobDTO(pageJobEntity);
        }

        return pageJobDTO;
    }

    @Override
    public boolean saveFavoriteJob(String id) throws JobNotFoundException {
        Integer jobId = Integer.parseInt(id);
        Optional<Job> jobOptional = jobRepository.findByJobIdAndDeleteFlagIs(jobId,1);
        Optional<User> userOptional = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(),1);
        if(userOptional.isPresent()){
            if(jobOptional.isPresent()){
                FavoriteJob favoriteJob = new FavoriteJob();
                Optional<FavoriteJob> favoriteJobOptional = favoriteJobRepository.findByUserAndJob(userOptional.get(),jobOptional.get());
                if(favoriteJobOptional.isPresent()){
                    favoriteJob = favoriteJobOptional.get();
                    favoriteJob.setDeleteFlag(1);
                    favoriteJob.setLastModifiedBy(userOptional.get().getUsername());
                    favoriteJob.setLastModifiedDate(Instant.now());
                    favoriteJobRepository.save(favoriteJob);
                    return true;
                }
                favoriteJob.setJob(jobOptional.get());
                favoriteJob.setUser(userOptional.get());
                favoriteJob.setCreatedBy(userOptional.get().getUsername());
                favoriteJob.setCreatedDate(Instant.now());
                favoriteJob.setLastModifiedBy(userOptional.get().getUsername());
                favoriteJob.setLastModifiedDate(Instant.now());
                favoriteJob.setDeleteFlag(1);
                favoriteJobRepository.save(favoriteJob);
                return true;

            }
            throw new JobNotFoundException();
        }
        return false;
    }

    @Override
    public boolean discardFavoriteJob(String id) throws JobNotFoundException {
        Integer jobId = Integer.parseInt(id);
        Optional<Job> jobOptional = jobRepository.findByJobIdAndDeleteFlagIs(jobId,1);
        Optional<User> userOptional = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(),1);
        if(userOptional.isPresent()){
            if(jobOptional.isPresent()){
                Optional<FavoriteJob> favoriteJobOptional = favoriteJobRepository.findByUserAndJob(userOptional.get(),jobOptional.get());
                if(favoriteJobOptional.isPresent()){
                    FavoriteJob  favoriteJob = favoriteJobOptional.get();
                    favoriteJob.setDeleteFlag(0);
                    favoriteJob.setLastModifiedBy(userOptional.get().getUsername());
                    favoriteJob.setLastModifiedDate(Instant.now());
                    favoriteJobRepository.save(favoriteJob);
                    return true;
                }
            }
            throw new JobNotFoundException();
        }
        return false;
    }

    @Override
    public boolean isJobSavedByCurrentUser(Integer jobId) {
        Optional<Job> jobOptional = jobRepository.findByJobIdAndDeleteFlagIs(jobId,1);
        Optional<User> userOptional = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(),1);
        Optional<FavoriteJob> favoriteJobOptional = favoriteJobRepository.findByUserAndJob(userOptional.get(),jobOptional.get());
        if(userOptional.isPresent() && jobOptional.isPresent() && favoriteJobOptional.isPresent()){
            return true;
        }
        return false;
    }

    @Override
    public int getTotalJobPostedThisMonth() {
        Optional<User> userOptional= userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(),1);
        if(userOptional.isPresent()){
            Integer companyId = userOptional.get().getCompany().getCompanyId();
            List<Job> listJob = jobRepository.getListJobPostedThisMonth(companyId);
            return listJob.size();
        }
        return 0;

    }


}
