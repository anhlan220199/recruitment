package com.project.recruitment.dto;

import com.project.recruitment.entity.Job;
import com.project.recruitment.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class DashboardCandidateDTO {
    private int userAppliedJobId;
    private User user;
    private Job job;
    private String email;
    private int deleteFlag;
    private String appliedDate;
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getAppliedDate() {
        return appliedDate;
    }

    public void setAppliedDate(String appliedDate) {
        this.appliedDate = appliedDate;
    }

    public int getUserAppliedJobId() {
        return userAppliedJobId;
    }

    public void setUserAppliedJobId(int userAppliedJobId) {
        this.userAppliedJobId = userAppliedJobId;
    }
}
