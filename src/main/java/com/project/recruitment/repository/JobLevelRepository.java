package com.project.recruitment.repository;

import com.project.recruitment.entity.JobLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface JobLevelRepository extends JpaRepository<JobLevel, Integer> {
    List<JobLevel> findAll();
    Optional<JobLevel> findByName(String jobLevel);
    JobLevel findByJobLevelId(Integer id);
}
