package com.project.recruitment.service.impl;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.dto.CompanyReviewDTO;
import com.project.recruitment.dto.ListCandidateDTO;
import com.project.recruitment.dto.ListCompanyReviewDTO;
import com.project.recruitment.entity.Company;
import com.project.recruitment.entity.Review;
import com.project.recruitment.entity.User;
import com.project.recruitment.exception.CompanyNotFoundException;
import com.project.recruitment.helper.Helper;
import com.project.recruitment.repository.CompanyRepository;
import com.project.recruitment.repository.CompanyReviewRepository;
import com.project.recruitment.repository.UserRepository;
import com.project.recruitment.service.CompanyReviewService;
import com.project.recruitment.service.HelperService;
import com.project.recruitment.util.SecurityUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class CompanyReviewServiceImpl implements CompanyReviewService {
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final CompanyReviewRepository companyReviewRepository;
    private final HelperService helperService;
    private final Helper helper;

    public CompanyReviewServiceImpl(UserRepository userRepository, CompanyRepository companyRepository, CompanyReviewRepository companyReviewRepository, HelperService helperService, Helper helper) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.companyReviewRepository = companyReviewRepository;
        this.helperService = helperService;
        this.helper = helper;
    }

    @Override
    public boolean saveCompanyReview(String id, CompanyReviewDTO companyReviewDTO) throws CompanyNotFoundException {
        Integer companyId = Integer.parseInt(id);
        Optional<User> userOptional = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(),1);
        Optional<Company> companyOptional = companyRepository.findByCompanyIdAndDeleteFlagIs(companyId,1);
        if(userOptional.isPresent()){
            if(companyOptional.isPresent()){
                Company company = companyOptional.get();
                Optional<Review> reviewOptional =companyReviewRepository.findByUserAndCompanyAndDeleteFlagIs(userOptional.get(),companyOptional.get(),1);
                if (reviewOptional.isPresent()){

                    Review review = reviewOptional.get();
                    review.setTitle(companyReviewDTO.getTitle());
                    review.setSatisfaction(companyReviewDTO.getSatisfaction());
                    review.setImprovement(companyReviewDTO.getImprovement());
                    review.setRating(companyReviewDTO.getRating());
                    review.setCompany(company);
                    review.setUser(userOptional.get());
                    review.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().get());
                    review.setLastModifiedDate(Instant.now());
                    review.setDeleteFlag(1);
                    companyReviewRepository.save(review);
                    company.setTotalRating(helperService.getTotalRating(company));
                    return true;
                }else{
                    Review review = Helper.mapToReviewEntity(companyReviewDTO);
                    review.setCompany(companyOptional.get());
                    review.setUser(userOptional.get());
                    review.setCreatedBy(SecurityUtils.getCurrentUserLogin().get());
                    review.setCreatedDate(Instant.now());
                    review.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().get());
                    review.setLastModifiedDate(Instant.now());
                    review.setDeleteFlag(1);
                    companyReviewRepository.save(review);
                    company.setTotalRating(helperService.getTotalRating(company));
                    return true;
                }
            }
            throw new CompanyNotFoundException();
        }
        return false;
    }

    @Override
    public Page<ListCompanyReviewDTO> listAllReviewByCompanyId(String id, String pageNumbers) throws CompanyNotFoundException {
        Integer pageNumber = Integer.parseInt(pageNumbers);
        Integer companyId = Integer.parseInt(id);
        Page<Review> pageCompanyReviewEntity = null;
        Page<ListCompanyReviewDTO> pageCompanyReviewDTO = null;
        Optional<Company> companyOptional =companyRepository.findByCompanyIdAndDeleteFlagIs(companyId,1);
        if(companyOptional.isPresent()){
            Company company = companyOptional.get();

            // check pageNumber
            Page<Review> allReview = companyReviewRepository.findAllByCompanyAndDeleteFlagIs(company,1,null);
            int sizeList = allReview.getSize();
            pageNumber = Helper.checkPageNumber(sizeList, pageNumber);
            pageCompanyReviewEntity = companyReviewRepository.findAllByCompanyAndDeleteFlagIs(company, 1,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE, Sort.by(Sort.Direction.DESC, "reviewId")));

            pageCompanyReviewDTO = helper.mapToPageListCompanyReviewDTO(pageCompanyReviewEntity);
            return pageCompanyReviewDTO;

        }
        throw new CompanyNotFoundException();
    }

    @Override
    public Company getCompanyToReview(String id) throws CompanyNotFoundException {
        Integer companyId = Integer.parseInt(id);
        Optional<Company> companyOptional =companyRepository.findByCompanyIdAndDeleteFlagIs(companyId,1);
        if(companyOptional.isPresent()){
            return companyOptional.get();
        }
        throw new CompanyNotFoundException();

    }
}
