package com.project.recruitment.dto;

import com.project.recruitment.entity.Review;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CompanyDetailDTO {

    private int companyId;
    private String name;
    private String city;
    private String type;
    private String website;
    private String hotline;
    private String email;
    private String aboutUs;
    private int size;
    private int openJobNumber;
    private float totalRating;
    private int deleteFlag;
    private Set<JobDetailDTO> listJob;
    private Set<Review> listReview;

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getHotline() {
        return hotline;
    }

    public void setHotline(String hotline) {
        this.hotline = hotline;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Set<JobDetailDTO> getListJob() {
        return listJob;
    }

    public void setListJob(Set<JobDetailDTO> listJob) {
        this.listJob = listJob;
    }

    public Set<Review> getListReview() {
        return listReview;
    }

    public void setListReview(Set<Review> listReview) {
        this.listReview = listReview;
    }

    public int getOpenJobNumber() {
        return openJobNumber;
    }

    public void setOpenJobNumber(int openJobNumber) {
        this.openJobNumber = openJobNumber;
    }

    public float getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(float totalRating) {
        this.totalRating = totalRating;
    }
}
