package com.project.recruitment.dto;

import com.project.recruitment.entity.Company;
import com.project.recruitment.entity.FavoriteJob;
import com.project.recruitment.entity.Skill;
import com.project.recruitment.entity.UserAppliedJob;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class JobDetailDTO {

    private int jobId;
    private String city;
    private String jobType;
    private String experience;
    private int salary;
    private String currency;
    private String requirement;
    private String description;
    private String otherBenefit;
    private String gender;
    private int quantity;
    private String createdDate;
    private String deadline;
    private int deleteFlag;
    private Set<FavoriteJob> listFavoriteJob;
    private Company company;
    private String jobTitle;
    private Skill skill;
    private Set<UserAppliedJob> listUserAppliedJob;

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getCurrency() { return currency; }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOtherBenefit() {
        return otherBenefit;
    }

    public void setOtherBenefit(String otherBenefit) {
        this.otherBenefit = otherBenefit;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Set<FavoriteJob> getListFavoriteJob() {
        return listFavoriteJob;
    }

    public void setListFavoriteJob(Set<FavoriteJob> listFavoriteJob) {
        this.listFavoriteJob = listFavoriteJob;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public Set<UserAppliedJob> getListUserAppliedJob() {
        return listUserAppliedJob;
    }

    public void setListUserAppliedJob(Set<UserAppliedJob> listUserAppliedJob) {
        this.listUserAppliedJob = listUserAppliedJob;
    }
}
