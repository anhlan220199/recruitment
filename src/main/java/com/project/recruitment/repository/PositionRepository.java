package com.project.recruitment.repository;

import com.project.recruitment.entity.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PositionRepository extends JpaRepository<Position, Integer> {
    List<Position> findAllByDeleteFlagIs(Integer deleteFlag);
    Optional<Position>findByNameAndDeleteFlag(String positionName, Integer deleteFlag);
    Position findByPositionId (Integer id);
}
