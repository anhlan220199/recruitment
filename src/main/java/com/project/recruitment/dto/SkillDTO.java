package com.project.recruitment.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class SkillDTO {

    private int skillId;
    private String name;

    public int getSkillId() {
        return skillId;
    }

    public String getName() {
        return name;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    public void setName(String name) {
        this.name = name;
    }
}
