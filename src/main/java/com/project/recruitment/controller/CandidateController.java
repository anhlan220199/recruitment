package com.project.recruitment.controller;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.constants.PathConstant;
import com.project.recruitment.constants.UrlConstant;
import com.project.recruitment.dto.*;
import com.project.recruitment.entity.FavoriteJob;
import com.project.recruitment.entity.UserAppliedJob;
import com.project.recruitment.exception.*;
import com.project.recruitment.helper.FileHelper;
import com.project.recruitment.service.*;
import com.project.recruitment.util.SecurityUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Pattern;
import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@Validated
@RequestMapping("/candidate")
public class CandidateController {

    private final CandidateService candidateService;
    private final HelperService helperService;
    private final CompanyReviewService companyReviewService;
    private final JobService jobService;
    private final EmailService emailService;

    public CandidateController(CandidateService candidateService, HelperService helperService, CompanyReviewService companyReviewService, JobService jobService, EmailService emailService) {
        this.candidateService = candidateService;
        this.helperService = helperService;
        this.companyReviewService = companyReviewService;
        this.jobService = jobService;
        this.emailService = emailService;
    }

    @GetMapping("/list")
    public String viewListCandidate(Model model,
                                    @Pattern(regexp = "^-?[0-9]\\d*(\\d+)?$", message = "Page number is number only!")
                                    @RequestParam(defaultValue = "1", value = "pageNumber", required = true) String pageNumber,
                                    @RequestParam(value = "keyword", required = true) String keyword) throws ListJobNotFoundException, ListUserNotFoundException {

        Page<ListCandidateDTO> page = candidateService.listAll(keyword, pageNumber);

        model.addAttribute("isFilter",false);
        model.addAttribute("listExpertise", jobService.getAllJobPosition());
        model.addAttribute("listSkill", jobService.getAllSkill());
        model.addAttribute("listExperienceYear", candidateService.getListExperienceYear());
        model.addAttribute("page", page);
        model.addAttribute("isEmptyListCandidate", page.isEmpty());
        model.addAttribute("pageSize", CommonConstant.PAGE_SIZE);
        model.addAttribute("keyword", keyword);
        return "candidate/candidate-listing";
    }

    @GetMapping("/filter")
    public String filterCandidate(Model model,
                                  @Pattern(regexp = "^-?[0-9]\\d*(\\d+)?$", message = "Page number is number only!")
                                  @RequestParam(defaultValue = "1", value = "pageNumber", required = true) String pageNumber,
                                  @RequestParam(value = "expertise", required = false) String expertise,
                                  @RequestParam(value = "skill", required = false) String skill,
                                  @RequestParam(value = "experience", required = false) String experienceYear) {
        Page<ListCandidateDTO> page = candidateService.filterCandidateByExpertiseAndSkillAndExperienceYear(expertise,skill,experienceYear,pageNumber);
        model.addAttribute("page", page);
        model.addAttribute("isFilter",true);
        model.addAttribute("isEmptyListCandidate", page.isEmpty());
        model.addAttribute("pageSize", CommonConstant.PAGE_SIZE);
        model.addAttribute("listExpertise", jobService.getAllJobPosition());
        model.addAttribute("listSkill", jobService.getAllSkill());
        model.addAttribute("listExperienceYear", candidateService.getListExperienceYear());
        return "candidate/candidate-listing";
    }

    @GetMapping("/detail/{id:.+}")
    public String getCandidateDetailById(Model model, @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "User id is number only!") @PathVariable("id") String userId) throws PageNotFoundException, CandidateNotFoundException {
        CandidateDetailDTO candidateDetailDTO = candidateService.getCandidateDetailById(Integer.parseInt(userId));
        model.addAttribute("candidateDetail", candidateDetailDTO);
        return "candidate/candidate-details";
    }

    @GetMapping("/delete/{candidateId}")
    public String handleDeleteCandidate(
            @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Candidate ID is number only!") @PathVariable("candidateId") String candidateId) throws CandidateNotFoundException {
        candidateService.deleteCandidate(candidateId);
        return UrlConstant.getRedirectUrl("/candidate/list?pageNumber=1&keyword=");
    }

    @GetMapping("/dashboard")
    public String getDashboard(Model model) throws CandidateNotFoundException {
        List<UserAppliedJob> userAppliedJobList = candidateService.getListUserAppliedJobByCurrentCandidate();
        model.addAttribute("sizeUserAppliedJobList", userAppliedJobList.size());
        List<ListJobDTO> favoriteJobList = candidateService.getListJobBookmark();
        model.addAttribute("sizeFavoriteJobList", favoriteJobList.size());
        return "candidate/dashboard";
    }

    @GetMapping("/dashboard/edit/profile")
    public String getInformationToEditForm(Model model) throws CandidateNotFoundException {
        UpdateCandidateDTO updateCandidateDTO = candidateService.getCandidateToUpdateForm();
        model.addAttribute("candidateEdit", updateCandidateDTO);
        return "candidate/dashboard-edit-profile";
    }

    @PostMapping("/dashboard/profile/edit")
    public String updateCandidate(@Validated @ModelAttribute("candidateEdit") UpdateCandidateDTO updateCandidateDTO, BindingResult bindingResult, Model model) throws CandidateNotFoundException {
        if (bindingResult.hasErrors()) {
            UpdateCandidateDTO updateCandidate = candidateService.getCandidateToUpdateForm();
            model.addAttribute("candidateEdit", updateCandidate);
            return "candidate/dashboard-edit-profile";
        }
        boolean isUpdateSuccess =candidateService.editProfile(updateCandidateDTO);
        model.addAttribute("isUpdateSuccess",isUpdateSuccess );
        model.addAttribute("changeUpdateSuccess", "Your profile information is updated successfully!");
        return "candidate/dashboard-edit-profile";
    }

    @GetMapping("/dashboard/change/password")
    public String getChangePasswordForm(Model model) throws CandidateNotFoundException {
        ChangePasswordDTO changePasswordDTO = candidateService.getCandidateToChangePasswordForm();
        model.addAttribute("candidateEdit", changePasswordDTO);
        return "candidate/dashboard-change-password";
    }

    @PostMapping("/dashboard/change/password")
    public String changePassword( @ModelAttribute("candidateEdit") ChangePasswordDTO changePasswordDTO, BindingResult bindingResult, Model model) throws CandidateNotFoundException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("candidateEdit", changePasswordDTO);
            return "candidate/dashboard-change-password";
        }
        BindingResult result = candidateService.changePassword(changePasswordDTO, bindingResult);
        if (!result.hasErrors()) {
            model.addAttribute("isPasswordChangedSuccess", true);
            model.addAttribute("changePasswordSuccess", CommonConstant.UserMessage.CHANGE_SUCCESS);
            model.addAttribute("candidateEdit", changePasswordDTO);
            return "candidate/dashboard-change-password";
        }
        model.addAttribute("candidateEdit", changePasswordDTO);
        return "candidate/dashboard-change-password";
    }

    @GetMapping("/dashboard/applied")
    public String getJobApplied(Model model) throws CandidateNotFoundException {
        List<UserAppliedJob> userAppliedJobList = candidateService.getListUserAppliedJobByCurrentCandidate();
        model.addAttribute("userAppliedJobList", userAppliedJobList);
        model.addAttribute("sizeUserAppliedJobList", userAppliedJobList.size());
        return "candidate/dashboard-applied";
    }

    @GetMapping("/resume/detail")
    public String getResumeInfor(Model model) throws CandidateNotFoundException {
        ResumeDetailDTO resumeDetailDTO = candidateService.getResumeDetail();
        model.addAttribute("resumeDTO", resumeDetailDTO);
        return "candidate/resume";
    }

    @GetMapping(value = "/downloadCV")
    public void downloadCVFIle(HttpServletResponse response) throws FileNotFoundException, IOException, CandidateNotFoundException {
        String cvFileName = candidateService.getCurrentUser().getCvFileName();
        if (cvFileName != null && !"".equals(cvFileName)) {
            File file = new File(PathConstant.PATH_TO_PROJECT + PathConstant.PATH_TO_CANDIDATE_CV + cvFileName);
            byte[] data = FileUtils.readFileToByteArray(file);
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + file.getName());
            response.setContentLength(data.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(data));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
        } else {
            throw new FileNotFoundException();
        }
    }

    @GetMapping("/resume/add")
    public String getInforToAddResumeForm(Model model) throws CandidateNotFoundException {
        UpdateResumeDTO updateResumeDTO = candidateService.getCandidateToAddResumeForm();
        model.addAttribute("listJobType", helperService.getAllJobType());
        model.addAttribute("updateResumeDTO", updateResumeDTO);
        String fileCVName = candidateService.getCurrentUser().getCvFilePath();
        if (fileCVName == null || "".equals(fileCVName)) {
            model.addAttribute("fileCVName", "No CV file has been uploaded yet! Please choose one!");
        } else {
            model.addAttribute("fileCVName", fileCVName);
        }
        return "candidate/add-resume";
    }

    @PostMapping("/resume/add")
    public String AddResume(@Validated @ModelAttribute("updateResumeDTO") UpdateResumeDTO updateResumeDTO, BindingResult bindingResult, Model model) throws CandidateNotFoundException, InvalidFileExtensionException, IOException, LogoutException {
        if (bindingResult.hasErrors()) {
            UpdateResumeDTO updateResume = candidateService.getCandidateToAddResumeForm();
            model.addAttribute("updateResumeDTO", updateResume);
            model.addAttribute("listJobType", helperService.getAllJobType());
            return "candidate/add-resume";
        }
        Optional<String> userName = SecurityUtils.getCurrentUserLogin();
        if (userName.get().equals("anonymousUser")) {
            throw new LogoutException();
        }
        MultipartFile multipartFile = updateResumeDTO.getMultipartFile();
        String fileName = null;

        if (!multipartFile.isEmpty()) {
            boolean isValidFileExtension = FileHelper.isValidFileExtension(multipartFile.getOriginalFilename());
            if (isValidFileExtension == false) {
                throw new InvalidFileExtensionException();
            }
            fileName = UUID.randomUUID().toString() + "_" + multipartFile.getOriginalFilename();
            candidateService.saveCVFile(multipartFile, fileName);
        }
        candidateService.addResume(updateResumeDTO, fileName);
        return UrlConstant.getRedirectUrl("/candidate/resume/detail");
    }

    @GetMapping("/resume/edit")
    public String getInforToEditResumeForm(Model model) throws CandidateNotFoundException {
        UpdateResumeDTO updateResumeDTO = candidateService.getCandidateToAddResumeForm();
        model.addAttribute("updateResumeDTO", updateResumeDTO);
        model.addAttribute("listJobType", helperService.getAllJobType());
        String fileCVName = candidateService.getCurrentUser().getCvFileName();
        if (fileCVName == null || "".equals(fileCVName)) {
            model.addAttribute("fileCVName", "No CV file has been uploaded yet! Please choose one!");
        } else {
            model.addAttribute("fileCVName", fileCVName);
        }
        return "candidate/dashboard-edit-resume";
    }

    @GetMapping("/my-account/delete")
    public String handleDeleteProfile(RedirectAttributes redirectAttributes) throws CandidateNotFoundException {
        boolean isDeleted = candidateService.deleteProfile();
        redirectAttributes.addFlashAttribute("isAccountDeleted", isDeleted);
        redirectAttributes.addFlashAttribute("message","Your account have been deleted!");
        return UrlConstant.getRedirectUrl("/login");
    }

    @GetMapping("/dashboard/bookmark")
    public String getBookmarkList(Model model) throws CandidateNotFoundException {
        List<ListJobDTO> listJobDTOS = candidateService.getListJobBookmark();
        model.addAttribute("listJobBookmark", listJobDTOS);
        model.addAttribute("sizeListJobBookmark", listJobDTOS.size());
        return "candidate/dashboard-bookmark";
    }

    @GetMapping("/company/review/{companyId}")
    public String getReviewCompanyForm(Model model,@Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Company ID is number only!") @PathVariable("companyId") String companyId) throws CompanyNotFoundException {
        model.addAttribute("companyId", companyId);
        model.addAttribute("companyReview",new CompanyReviewDTO());
        model.addAttribute("company", companyReviewService.getCompanyToReview(companyId));

        return "candidate/review-company";
    }

    @PostMapping("/company/review/{companyId}")
    public String saveReviewCompany(Model model,
                                    @Pattern(regexp = "^((?!(0))[0-9^<>'\\\"/;`%]{0,})$", message = "Company ID is number only!") @PathVariable("companyId") String companyId,
                                    @ModelAttribute("companyReview") CompanyReviewDTO companyReviewDTO,
                                    RedirectAttributes redirectAttributes) throws CompanyNotFoundException {
        boolean isReviewSaved = companyReviewService.saveCompanyReview(companyId,companyReviewDTO);
        redirectAttributes.addFlashAttribute("isReviewSaved", isReviewSaved);
        redirectAttributes.addFlashAttribute("message","Your rating and review have been recorded!");
        return "redirect:/candidate/company/review/"+companyId;
    }

    @PostMapping("/contact/{companyId}")
    String contactEmployer(
            HttpServletRequest request,
            @PathVariable("companyId") String companyId) throws CompanyNotFoundException, MessagingException {
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        String message = request.getParameter("message");
        emailService.sendEmailFromCandidateToCompany(email, name, message, Integer.parseInt(companyId));
        return UrlConstant.getRedirectUrl("/company/detail/" + companyId);
    }
}
