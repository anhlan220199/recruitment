package com.project.recruitment.repository;

import com.project.recruitment.entity.JobType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface JobTypeRepository extends JpaRepository<JobType, Integer> {
    List<JobType> findAll();

    Optional<JobType> findByName(String name);

}
