package com.project.recruitment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User extends AbstractAuditingEntity {
    private int userId;
    private String username;
    private String password;
    private int isCandidate;
    private String fullName;
    private String phone;
    private String cvFileName;
    private String cvFilePath;
    private String email;
    private String aboutMe;
    private String expertise;
    private String address;
    private String location;
    private String experience;
    private String qualification;
    private String workExperience;
    private String educationBackground;
    private String specialQualification;
    private String professionalSkill;
    private int deleteFlag;

    private Company company;
    private Gender gender;
    private JobType jobType;
    private Role role;

    private List<UserAppliedJob> listUserAppliedJob;
    private Set<FavoriteJob> listFavoriteJob;
    private Set<Review> listReview;
    private Set<ConfirmationToken> listConfirmationToken;
    private Set<Job> listJob;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "is_candidate")
    public int getIsCandidate() {
        return isCandidate;
    }

    public void setIsCandidate(int isCandidate) {
        this.isCandidate = isCandidate;
    }

    @Basic
    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "cv_file_name")
    public String getCvFileName() {
        return cvFileName;
    }

    public void setCvFileName(String cvFileName) {
        this.cvFileName = cvFileName;
    }

    @Basic
    @Column(name = "cv_file_path")
    public String getCvFilePath() {
        return cvFilePath;
    }

    public void setCvFilePath(String cvFilePath) {
        this.cvFilePath = cvFilePath;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "about_me")
    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    @Basic
    @Column(name = "expertise")
    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Basic
    @Column(name = "experience")
    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    @Basic
    @Column(name = "qualification")
    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Basic
    @Column(name = "work_experience")
    public String getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    @Basic
    @Column(name = "education_background")
    public String getEducationBackground() {
        return educationBackground;
    }

    public void setEducationBackground(String educationBackground) {
        this.educationBackground = educationBackground;
    }

    @Basic
    @Column(name = "special_qualification")
    public String getSpecialQualification() {
        return specialQualification;
    }

    public void setSpecialQualification(String specialQualification) {
        this.specialQualification = specialQualification;
    }

    @Basic
    @Column(name = "professional_skill")
    public String getProfessionalSkill() {
        return professionalSkill;
    }

    public void setProfessionalSkill(String professionalSkill) {
        this.professionalSkill = professionalSkill;
    }

    @Basic
    @Column(name = "delete_flag")
    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName = "company_id")
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @ManyToOne
    @JoinColumn(name = "gender_id", referencedColumnName = "gender_id")
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @ManyToOne
    @JoinColumn(name = "job_type_id", referencedColumnName = "job_type_id")
    public JobType getJobType() {
        return jobType;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "role_id")
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @OneToMany(mappedBy = "user")
    public List<UserAppliedJob> getListUserAppliedJob() {
        return listUserAppliedJob;
    }

    public void setListUserAppliedJob(List<UserAppliedJob> listUserAppliedJob) {
        this.listUserAppliedJob = listUserAppliedJob;
    }

    @OneToMany(mappedBy = "user")
    public Set<FavoriteJob> getListFavoriteJob() {
        return listFavoriteJob;
    }

    public void setListFavoriteJob(Set<FavoriteJob> listFavoriteJob) {
        this.listFavoriteJob = listFavoriteJob;
    }

    @OneToMany(mappedBy = "user")
    public Set<Review> getListReview() {
        return listReview;
    }

    public void setListReview(Set<Review> listReview) {
        this.listReview = listReview;
    }

    @OneToMany(mappedBy = "user")
    public Set<ConfirmationToken> getListConfirmationToken() {
        return listConfirmationToken;
    }

    public void setListConfirmationToken(Set<ConfirmationToken> listConfirmationToken) {
        this.listConfirmationToken = listConfirmationToken;
    }

    @OneToMany(mappedBy = "user")
    public Set<Job> getListJob() {
        return listJob;
    }

    public void setListJob(Set<Job> listJob) {
        this.listJob = listJob;
    }

    @Transient
    public List<GrantedAuthority> getAuthorities(Role role) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role.getName()));
        return authorities;
    }

}
