package com.project.recruitment.dto;

import com.project.recruitment.constants.CommonConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class InterviewScheduleDTO {

    private Integer userAppliedJobId;
    private Integer jobId;
    private Integer userId;
    private String email;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    private String interviewDate;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    private String interviewLocation;

    public Integer getUserAppliedJobId() {
        return userAppliedJobId;
    }

    public Integer getJobId() {
        return jobId;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getInterviewEmailTitle() {
        return interviewDate;
    }

    public String getInterviewLocation() {
        return interviewLocation;
    }

    public void setUserAppliedJobId(Integer userAppliedJobId) {
        this.userAppliedJobId = userAppliedJobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setInterviewEmailTitle(String interviewEmailTitle) {
        this.interviewDate = interviewEmailTitle;
    }

    public void setInterviewLocation(String interviewLocation) {
        this.interviewLocation = interviewLocation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInterviewDate() {
        return interviewDate;
    }

    public void setInterviewDate(String interviewDate) {
        this.interviewDate = interviewDate;
    }
}
