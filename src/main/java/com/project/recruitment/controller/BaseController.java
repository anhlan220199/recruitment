package com.project.recruitment.controller;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.constants.UrlConstant;
import com.project.recruitment.entity.User;
import com.project.recruitment.exception.CandidateNotFoundException;
import com.project.recruitment.exception.UserAppliedJobNotFoundException;
import com.project.recruitment.service.*;
import com.project.recruitment.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
@RequestMapping("")
public class BaseController {

    @Autowired
    private HttpSession httpSession;

    private final CandidateService candidateService;
    private final JobService jobService;
    private final CompanyService companyService;
    private final HelperService helperService;
    private final UserAppliedJobService userAppliedJobService;

    public BaseController(CandidateService candidateService, JobService jobService, CompanyService companyService, HelperService helperService, UserAppliedJobService userAppliedJobService) {
        this.candidateService = candidateService;
           this.jobService = jobService;
        this.companyService = companyService;
        this.helperService = helperService;
        this.userAppliedJobService = userAppliedJobService;
    }

    @GetMapping(value = {"/", "/home"})
    public String base(Model model) throws CandidateNotFoundException, UserAppliedJobNotFoundException {
        Optional<String> username = SecurityUtils.getCurrentUserLogin();
        if(!"anonymousUser".equals(username.get())) {
            Optional<User> user = candidateService.findUserByUsername(username.get());
            model.addAttribute("userInfor", user.get());
            httpSession.setAttribute("userInfor", user.get());
        }
        model.addAttribute("countJob", jobService.countJobs());
        model.addAttribute("countCandidate", candidateService.countCandidate());
        model.addAttribute("countCompany", companyService.countCompany());
        model.addAttribute("countUserAppliedJob", userAppliedJobService.countUserAppliedJob());

        model.addAttribute("listSkills", helperService.getAllSkill());
        model.addAttribute("listPosition", helperService.getAllPosition());
        model.addAttribute("listJobLevel", helperService.getAllJobLevel());

        model.addAttribute("listRecentJobFirst", jobService.getListRecentJobFirst());
        model.addAttribute("listRecentJobSecond", jobService.getListRecentJobSecond());
        model.addAttribute("listFeatureJobFirst", jobService.getListFeatureJobFirst());
        model.addAttribute("listFeatureJobSecond", jobService.getListFeatureJobSecond());
        model.addAttribute("topCompanyList", companyService.getTopListCompanies());

        return "home/home-1";
    }

    @RequestMapping("/403")
    public String accessDenied(Model model){
        model.addAttribute("httpErrorCode", "403");
        model.addAttribute("errorMsg", "You do not have access to this link!");
        return "/error/error-page";
    }

    @GetMapping("/account")
    String showAccountInfor(Model model) throws CandidateNotFoundException {
        User user = candidateService.getCurrentUser();
        if(user.getRole().getName().equals(CommonConstant.CANDIDATE)){
            return UrlConstant.getRedirectUrl("/candidate/dashboard/edit/profile");
        } else if(user.getRole().getName().equals(CommonConstant.ADMIN_COMPANY)){
            return UrlConstant.getRedirectUrl("/employer/dashboard/profile/edit");
        } else {
            return UrlConstant.getRedirectUrl("/home");
        }
    }
}
