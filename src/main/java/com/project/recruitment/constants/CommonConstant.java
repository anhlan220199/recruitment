package com.project.recruitment.constants;

public class CommonConstant {

	private CommonConstant() {
	}

	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[_A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static final Integer PAGE_SIZE = 8;
	
	public static final Integer ROLE_ADMIN_ID = 1;

	public static final String FORMAT_DATE = "yyyy-MM-dd";
	public static final String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";
	public static final String ADMIN_COMPANY = "ADMIN_COMPANY";
	public static final String ADMIN_SYSTEM = "ADMIN_SYSTEM";
	public static final String CANDIDATE = "CANDIDATE";
	public static final String ANONYMOUS_USER = "ANONYMOUS_USER";


	public static class ErrorMessage {
		private ErrorMessage() {
		}

		public static final String FORMAT = "message.error.file.format";
		public static final String DUPLICATE = "message.error.duplicate";
		public static final String MAXLENGTH = "message.error.length.max.255";
		public static final String REQUIRED = "message.error.required";
		public static final String DEL_STATUS = "message.error.account.del_status";
		public static final String ID = "message.error.identity.input";
		
		public static final String SIZE_ERROR = "Must be 5 to 100 characters !";
		public static final String FORMAT_ERROR = "Invalid special characters !";
		public static final String NOTNULL = "Please fill out this field !";
		
		public static final String REGISTER = "Must be 6 to 30 characters";
		public static final String MIN_QUANTITY = "Min size is greater than 1";
	}

	public static class UserMessage {
		private UserMessage() {
		}

		public static final String PASS_NOT_MATCH = "message.error.password.non.match";
		public static final String ACCOUNT_EXIST = "message.error.account.exist";
		public static final String REGISTER_SUCCESS = "message.success.add";
		public static final String EDIT_SUCCESS = "message.success.edit";
		public static final String RESET_SUCCESS = "message.success.password.reset";
		public static final String CURRENT_PASS = "message.error.password.current";
		public static final String UPDATE_SUCCESS = "Update success!";
		public static final String CHANGE_SUCCESS = "Change success!";
	}

}
