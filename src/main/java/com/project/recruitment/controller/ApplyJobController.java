package com.project.recruitment.controller;

import com.project.recruitment.dto.CandidateJobDTO;
import com.project.recruitment.exception.*;
import com.project.recruitment.helper.FileHelper;
import com.project.recruitment.service.CandidateJobService;
import com.project.recruitment.service.EmailService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.UUID;

@Controller
@Validated
@RequestMapping(value = "/apply")
public class ApplyJobController {
    private final CandidateJobService candidateJobService;
    private final EmailService emailService;

    public ApplyJobController(CandidateJobService candidateJobService, EmailService emailService) {
        this.candidateJobService = candidateJobService;
        this.emailService = emailService;
    }

    @PostMapping
    public String applyJob(Model model,@ModelAttribute("applyJobDTO") CandidateJobDTO candidateJobDTO,
                           @RequestParam("jobId") Integer jobId) throws IOException, ApplyJobNotFoundException, LogoutException, CandidateNotFoundException, InvalidFileExtensionException, JobNotFoundException, MessagingException {

        MultipartFile multipartFile = candidateJobDTO.getMultipartFile();

        if (!multipartFile.isEmpty()) {
            String fileName = UUID.randomUUID().toString() + "_" + multipartFile.getOriginalFilename();
            boolean isValidFileExtension = FileHelper.isValidFileExtension(multipartFile.getOriginalFilename());
            if (isValidFileExtension) {
                emailService.sendApplyJobSuccessMail(candidateJobDTO);
                candidateJobService.saveApplyJob(candidateJobDTO, jobId, fileName);
                candidateJobService.saveApplyOnlineCVFile(multipartFile, fileName);
                model.addAttribute("applySuccess",true);
                model.addAttribute("emailAddress",candidateJobDTO.getEmail());
                return"success/success-page";
            }
        }
        throw new InvalidFileExtensionException();

    }
}
