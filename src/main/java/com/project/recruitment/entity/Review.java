package com.project.recruitment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Review extends AbstractAuditingEntity {
    private int reviewId;
    private String title;
    private String improvement;
    private String satisfaction;

    private Integer rating;
    private int deleteFlag;
    private Company company;
    private User user;

    @Id
    @Column(name = "review_id")
    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {    return title; }

    public void setTitle(String title) { this.title = title; }

    @Basic
    @Column(name = "improvement")
    public String getImprovement() {return improvement; }

    public void setImprovement(String improvement) {this.improvement = improvement;}

    @Basic
    @Column(name = "satisfaction")
    public String getSatisfaction() {return satisfaction;}

    public void setSatisfaction(String satisfaction) {  this.satisfaction = satisfaction;}

    @Basic
    @Column(name = "rating")
    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    @Basic
    @Column(name = "delete_flag")
    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName = "company_id", nullable = false)
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
