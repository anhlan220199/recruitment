package com.project.recruitment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "company", schema = "recruitment_db")
public class Company extends AbstractAuditingEntity {
    private Integer companyId;
    private String name;
    private String street;
    private String district;
    private String city;
    private String hotline;
    private String email;
    private String website;
    private String aboutUs;
    private Integer size;
    private Float totalRating;
    private int deleteFlag;

    private CompanyType companyType;

    private List<Job> listJob;
    private Set<User> listUser;
    private Set<Review> listReview;

    @Id
    @Column(name = "company_id")
    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "district")
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "hotline")
    public String getHotline() {
        return hotline;
    }

    public void setHotline(String hotline) {
        this.hotline = hotline;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "website")
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Basic
    @Column(name = "about_us")
    public String getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
    }

    @Basic
    @Column(name = "size")
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Basic
    @Column(name = "total_rating")
    public Float getTotalRating() {return totalRating;}

    public void setTotalRating(Float totalRating) { this.totalRating = totalRating; }

    @Basic
    @Column(name = "delete_flag")
    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @ManyToOne
    @JoinColumn(name = "company_type_id", referencedColumnName = "company_type_id", nullable = false)
    public CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(CompanyType companyType) {
        this.companyType = companyType;
    }


    @OneToMany(mappedBy = "company")
    public List<Job> getListJob() {
        return listJob;
    }

    public void setListJob(List<Job> listJob) {
        this.listJob = listJob;
    }


    @OneToMany(mappedBy = "company")
    public Set<User> getListUser() {
        return listUser;
    }

    public void setListUser(Set<User> listUser) {
        this.listUser = listUser;
    }

    @OneToMany(mappedBy = "company")
    public Set<Review> getListReview() {
        return listReview;
    }

    public void setListReview(Set<Review> listReview) {
        this.listReview = listReview;
    }
}
