package com.project.recruitment.service;

import com.project.recruitment.dto.JobLevelDTO;
import com.project.recruitment.dto.PositionDTO;
import com.project.recruitment.dto.SkillDTO;
import com.project.recruitment.entity.Company;
import com.project.recruitment.entity.Currency;
import com.project.recruitment.entity.Gender;
import com.project.recruitment.entity.JobType;

import java.util.List;

public interface HelperService {

    List<JobType> getAllJobType();

    List<Currency> getAllCurrency();

    List<Gender> getAllGender();

    List<JobLevelDTO> getAllJobLevel();

    List<PositionDTO> getAllPosition();

    List<SkillDTO> getAllSkill();

    float getTotalRating(Company company);
}
