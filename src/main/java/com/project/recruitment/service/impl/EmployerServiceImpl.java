package com.project.recruitment.service.impl;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.dto.DashboardCandidateDTO;
import com.project.recruitment.dto.DashboardJobDTO;
import com.project.recruitment.dto.EmployerProfileEditDTO;
import com.project.recruitment.entity.Company;
import com.project.recruitment.entity.Job;
import com.project.recruitment.entity.User;
import com.project.recruitment.entity.UserAppliedJob;
import com.project.recruitment.exception.EmployerNotFoundException;
import com.project.recruitment.helper.Helper;
import com.project.recruitment.repository.CompanyRepository;
import com.project.recruitment.repository.JobRepository;
import com.project.recruitment.repository.UserAppliedJobRepository;
import com.project.recruitment.repository.UserRepository;
import com.project.recruitment.service.EmployerService;
import com.project.recruitment.util.HibernateUtils;
import com.project.recruitment.util.SecurityUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.Date;
import java.util.Optional;

@Service
public class EmployerServiceImpl implements EmployerService {
    private final UserRepository userRepository;
    private final Helper helper;
    private final PasswordEncoder employerPasswordEncoder;
    private final JobRepository jobRepository;
    private final UserAppliedJobRepository userAppliedJobRepository;
    private final CompanyRepository companyRepository;
    public EmployerServiceImpl(UserRepository userRepository, Helper helper, PasswordEncoder employerPasswordEncoder, JobRepository jobRepository,UserAppliedJobRepository userAppliedJobRepository,CompanyRepository companyRepository) {
        this.userRepository = userRepository;
        this.helper = helper;
        this.employerPasswordEncoder = employerPasswordEncoder;
        this.jobRepository=jobRepository;
        this.userAppliedJobRepository = userAppliedJobRepository;
        this.companyRepository =companyRepository;
    }


    @Override
    public EmployerProfileEditDTO getEmployerProfileToEdit() throws EmployerNotFoundException {
        Optional<User> userOpt = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(), 1);
        if (userOpt.isPresent()) {
            EmployerProfileEditDTO employerProfileEditDTO = Helper.mapToEmployerProfileEditDTO(userOpt.get());
            employerProfileEditDTO.setUserId(userOpt.get().getUserId());
            return employerProfileEditDTO;
        }
        throw new EmployerNotFoundException();
    }

    @Override
    public boolean editEmployerProfile(String id, EmployerProfileEditDTO employerProfileEditDTO) throws EmployerNotFoundException{
        int employerId =Integer.parseInt(id);
        Optional<User> employerOpt=userRepository.findByUserIdAndDeleteFlagIs(employerId,1);
        boolean isExist= employerOpt.isPresent();
        if(isExist){
            employerProfileEditDTO.setUserId(employerId);
            User employer=Helper.mapToUserEntity(employerProfileEditDTO);
            employer.setPassword(employerOpt.get().getPassword());
            employer.setUsername(employerOpt.get().getUsername());
            employer.setRole(employerOpt.get().getRole());
            employer.setCompany(employerOpt.get().getCompany());
            employer.setCreatedDate(employerOpt.get().getCreatedDate());
            employer.setCreatedBy(employerOpt.get().getCreatedBy());
            employer.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().get());
            employer.setLastModifiedDate(new Date().toInstant());
            employer.setDeleteFlag(1);
            userRepository.save(employer);
            return true;
        }
        throw new EmployerNotFoundException();
    }

    @Override
    public BindingResult changePassword(EmployerProfileEditDTO employerProfileEditDTO, BindingResult bindingResult) throws EmployerNotFoundException{
        Optional<User> employerOpt=userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(),1);
        if(employerOpt.isPresent()){
            User employer = employerOpt.get();
            String oldPassword= employer.getPassword();
            BindingResult isValid = checkPasswordDTO(oldPassword,employerProfileEditDTO,bindingResult);
            if(!isValid.hasErrors()){
                employerProfileEditDTO.setUserId(employer.getUserId());
                String newPassword= employerProfileEditDTO.getNewPassword();
                employer.setPassword(employerPasswordEncoder.encode(newPassword));
                employer.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().get());
                employer.setLastModifiedDate(new Date().toInstant());
                employer.setDeleteFlag(1);
                userRepository.save(employer);
            }
            return isValid;
        }
        throw new EmployerNotFoundException();
    }

    private BindingResult checkPasswordDTO(String oldPassword,EmployerProfileEditDTO employerProfileEditDTO, BindingResult bindingResult){
        String oldPasswordDTO = employerProfileEditDTO.getOldPassword();
        String confirmPassword = employerProfileEditDTO.getConfirmPassword();
        String newPassword = employerProfileEditDTO.getNewPassword();
        if(newPassword.equals(oldPasswordDTO)|| newPassword.equals(oldPassword)){
          bindingResult.rejectValue("newPassword","newPassword.error","Your new password have to be different from the old one!");
  //           bindingResult.addError(new ObjectError("newPassword","Your new password have to be different from the old one"));
        }
        if(!newPassword.equals(confirmPassword)){
           bindingResult.rejectValue("confirmPassword","confirmPassword.error","Confirm password does not match!");
   //         bindingResult.addError(new ObjectError("confirmPassword","confirm password does not match!"));
        }
        if(!employerPasswordEncoder.matches(oldPasswordDTO,oldPassword)){
           bindingResult.rejectValue("oldPassword","oldPassword.error","Your current password is incorrect!");
   //        bindingResult.addError(new ObjectError("oldPassword","Your current password is incorrect!"));
        }
        return bindingResult;
    }


    @Override
    public Page<DashboardJobDTO> listAllJob(String keyword, String pageNumbers) throws IllegalArgumentException {
        Integer pageNumber = Integer.parseInt(pageNumbers);

        Page<Job> pageJobEntity = null;
        Page<DashboardJobDTO> pageDashboardJobDTO = null;
        Company company = userRepository.findByUsername(SecurityUtils.getCurrentUserLogin().get()).get().getCompany();
        // check pageNumber
        Page<Job> allJob = jobRepository.findAllByCompanyAndDeleteFlagIs(company,1, null);
        int sizeList = allJob.getSize();
        pageNumber = Helper.checkPageNumber(sizeList, pageNumber);

        // get all Job
        if (keyword == null || "".equals(keyword)) {

            pageJobEntity = jobRepository.findAllByCompanyAndDeleteFlagIs(company,1,PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE, Sort.by(Sort.Direction.DESC, "jobId")));

            pageDashboardJobDTO = helper.mapToPageDashboardJobDTO(pageJobEntity);

            // get job with keyword
        } else {
            String keywordFinal = HibernateUtils.escapeSQLLikeStatement(keyword);

            pageJobEntity = jobRepository.relativeSearchingByCompanyAndDeleteFlagIs(keywordFinal.toLowerCase(),company.getCompanyId(),1,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageDashboardJobDTO = helper.mapToPageDashboardJobDTO(pageJobEntity);
        }

        return pageDashboardJobDTO;
    }

    @Override
    public Page<DashboardCandidateDTO> listAllAppliedCandidate(String keyword, String pageNumbers) {
        Integer pageNumber = Integer.parseInt(pageNumbers);

        Page<UserAppliedJob> pageCandidateEntity = null;
        Page<DashboardCandidateDTO> pageDashboardCandidateDTO = null;
        Company company = userRepository.findByUsername(SecurityUtils.getCurrentUserLogin().get()).get().getCompany();
        // check pageNumber
        Page<UserAppliedJob> allCandidate = userAppliedJobRepository.findAllUserAppliedJobByCompanyIdAndDeleteFlagIs(company.getCompanyId(),1, null);
        int sizeList = allCandidate.getSize();
        pageNumber = Helper.checkPageNumber(sizeList, pageNumber);

        // get all Job
        if (keyword == null || "".equals(keyword)) {

            pageCandidateEntity = userAppliedJobRepository.findAllUserAppliedJobByCompanyIdAndDeleteFlagIs(company.getCompanyId(),1,PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageDashboardCandidateDTO = helper.mapToPageDashboardCandidateDTO(pageCandidateEntity);

            // get job with keyword
        } else {
            String keywordFinal = HibernateUtils.escapeSQLLikeStatement(keyword);

            pageCandidateEntity = userAppliedJobRepository.relativeSearchingByCompanyIdAndDeleteFlagIs(company.getCompanyId(),keywordFinal.toLowerCase(),1,
                    PageRequest.of(pageNumber - 1, CommonConstant.PAGE_SIZE));

            pageDashboardCandidateDTO = helper.mapToPageDashboardCandidateDTO(pageCandidateEntity);
        }

        return pageDashboardCandidateDTO;
    }

    @Override
    public boolean deleteMyAccount()throws EmployerNotFoundException{
        Optional<User> userOptional =userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(),1);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            user.setDeleteFlag(0);
            userRepository.save(user);
            return true;
        }
        throw new EmployerNotFoundException();
    }


}
