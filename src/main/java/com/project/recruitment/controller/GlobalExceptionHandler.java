package com.project.recruitment.controller;

import com.project.recruitment.exception.*;
import com.project.recruitment.util.SecurityUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolationException;
import java.io.FileNotFoundException;
import java.text.ParseException;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = PageNotFoundException.class)
    public ModelAndView handleJobNotFoundException(PageNotFoundException notFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Job Not Found";
        String errorCode = "404";
        notFoundException.setErrorCode(errorCode);
        notFoundException.setErrorCode(errorMsg);
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

    @ExceptionHandler(value = DeleteJobNotFoundException.class)
    public ModelAndView handleDeleteJobNotFoundException(DeleteJobNotFoundException DeleteJobNotFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "This job no longer available !";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ModelAndView handleJobConstraintViolationException(ConstraintViolationException constraintViolationException,
                                                              Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Integer Number only!";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

    @ExceptionHandler(value = ListJobNotFoundException.class)
    public ModelAndView handleListJobNotFoundException(ListJobNotFoundException listJobNotFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Page Not Found";
        String errorCode = "404";
        listJobNotFoundException.setErrorCode(errorCode);
        listJobNotFoundException.setErrorCode(errorMsg);
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

    @ExceptionHandler(value = ListUserNotFoundException.class)
    public ModelAndView handleListUserNotFoundException(ListUserNotFoundException listUserNotFoundException,
                                                        Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "List User Not Found";
        String errorCode = "404";
        listUserNotFoundException.setErrorCode(errorCode);
        listUserNotFoundException.setErrorCode(errorMsg);
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

    @ExceptionHandler(value = CandidateNotFoundException.class)
    public ModelAndView handleCandidateNotFoundException(CandidateNotFoundException CandidateNotFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "This candidate is no longer avaiable";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

    @ExceptionHandler(value = RegisterFailException.class)
    public ModelAndView handleRegisterFailException(RegisterFailException RegisterFailException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Register fail try again !";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }
    @ExceptionHandler(value = CompanyAlreadyExistException.class)
    public ModelAndView handleCompanyAlreadyExistException(CompanyAlreadyExistException companyAlreadyExistException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Company Already Exist !";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }
    @ExceptionHandler(value = DeleteCompanyNotFoundException.class)
    public ModelAndView handleDeleteCompanyNotFoundException(DeleteCompanyNotFoundException deleteCompanyNotFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Company is Deleted before or does not exist !";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }
    @ExceptionHandler(value = CompanyNotFoundException.class)
    public ModelAndView handleCompanyNotFoundException(CompanyNotFoundException companyNotFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Company does not exist !";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }
    @ExceptionHandler(value = EmployerNotFoundException.class)
    public ModelAndView handleEmployerNotFoundException(EmployerNotFoundException employerNotFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Profile Not Found. Try login again !";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }
    @ExceptionHandler(value = UserAppliedJobNotFoundException.class)
    public ModelAndView handleUserAppliedJobNoFoundException(UserAppliedJobNotFoundException userAppliedJobNotFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "User applied job not found. Try login again !";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

    @ExceptionHandler(value = AccessDenied.class)
    public ModelAndView handleAccessDeniedException(AccessDenied accessDenied, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Access is Denied!. Try again !";
        String errorCode = "403";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

    @ExceptionHandler(value = FileNotFoundException.class)
    public ModelAndView handleFileNotFoundException(FileNotFoundException fileNotFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "File not found !";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

    @ExceptionHandler(value = JobNotFoundException.class)
    public ModelAndView handleJobNotFoundException(JobNotFoundException jobNotFoundException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Job not found !";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }
    @ExceptionHandler(value = ParseException.class)
    public ModelAndView handleParseException(ParseException parseException, Model model) {
        ModelAndView errorPage = new ModelAndView("error/error-page");
        String errorMsg = "Parse Exception!";
        String errorCode = "500";
        errorPage.addObject("errorMsg", errorMsg);
        errorPage.addObject("httpErrorCode", errorCode);
        SecurityUtils.check(model);
        return errorPage;
    }

}
