package com.project.recruitment.service.impl;

import com.project.recruitment.dto.CandidateJobDTO;
import com.project.recruitment.dto.InterviewScheduleDTO;
import com.project.recruitment.entity.*;
import com.project.recruitment.exception.CandidateNotFoundException;
import com.project.recruitment.exception.CompanyNotFoundException;
import com.project.recruitment.exception.JobNotFoundException;
import com.project.recruitment.exception.UserAppliedJobNotFoundException;
import com.project.recruitment.repository.CompanyRepository;
import com.project.recruitment.repository.JobRepository;
import com.project.recruitment.repository.UserAppliedJobRepository;
import com.project.recruitment.repository.UserRepository;
import com.project.recruitment.service.EmailService;
import com.project.recruitment.util.SecurityUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Optional;

@Service
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender javaMailSender;
    private final UserRepository userRepository;
    private final JobRepository jobRepository;
    private final CompanyRepository companyRepository;
    private final UserAppliedJobRepository userAppliedJobRepository;

    public EmailServiceImpl(JavaMailSender javaMailSender, UserRepository userRepository, JobRepository jobRepository, CompanyRepository companyRepository, UserAppliedJobRepository userAppliedJobRepository) {
        this.javaMailSender = javaMailSender;
        this.userRepository = userRepository;
        this.jobRepository = jobRepository;
        this.companyRepository = companyRepository;
        this.userAppliedJobRepository = userAppliedJobRepository;
    }

    @Override
    public void sendRegisterConfirmationMail(String emailTo, ConfirmationToken confirmationToken) throws MessagingException, UnsupportedEncodingException {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emailTo);
        message.setSubject("Complete Company Account Registration!");
        message.setText("To confirm your account, please click here : "
                + "http://localhost:8080/register/confirmAccount?confirmationToken=" + confirmationToken.getConfirmationToken());
        javaMailSender.send(message);
    }

    @Override
    public boolean sendApplyJobSuccessMail(CandidateJobDTO candidateJobDTO) throws MessagingException, JobNotFoundException {
        Optional<Job> jobOptional = jobRepository.findByJobIdAndDeleteFlagIs(candidateJobDTO.getJobId(), 1);
        Optional<User> userOptional = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(), 1);
        if (jobOptional.isPresent() && userOptional.isPresent()) {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");

            String htmlMsg = "<h3>Dear, " + userOptional.get().getFullName() + "</h3>"
                    + "<p>We are very glad to inform that you are successfully apply for " + jobOptional.get().getJobTitle() + " belong to " + jobOptional.get().getCompany().getName() + "</p>"
                    + "<p>Your CV is absolutely excellent!</p>"
                    + "<p>Hr team will contact you as soon as possible</p>"
                    + "<p>Sincerely!</p>";

            message.setContent(htmlMsg, "text/html");
            String candidateEmail =candidateJobDTO.getEmail();
            helper.setTo(candidateJobDTO.getEmail());

            helper.setSubject("[ Job Application is Successfully] ");
            javaMailSender.send(message);
            return true;
        }
        throw new JobNotFoundException();
    }

    @Override
    public boolean sendInterviewScheduleMail(InterviewScheduleDTO interviewScheduleDTO) throws MessagingException, JobNotFoundException, CandidateNotFoundException, ParseException {
        Optional<Job> jobOptional = jobRepository.findByJobIdAndDeleteFlagIs(interviewScheduleDTO.getJobId(), 1);
        Optional<User> userOptional = userRepository.findByUsernameAndDeleteFlagIs(SecurityUtils.getCurrentUserLogin().get(), 1);
        if (jobOptional.isPresent() && userOptional.isPresent()) {

            String[] dateTimes = interviewScheduleDTO.getInterviewDate().split("T");

            Optional<User> candidate = userRepository.findByUserIdAndDeleteFlagIs(interviewScheduleDTO.getUserId(), 1);
            if (candidate.isEmpty()) {
                throw new CandidateNotFoundException();
            }

            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");

            String htmlMsg = "<h3>Dear, " + candidate.get().getFullName() + "</h3>"
                    + "<p>We are very glad to inform that you are successfully apply for " + jobOptional.get().getJobTitle() + " belong to " + jobOptional.get().getCompany().getName() + "</p>"
                    + "<p>Your interview will be taken place at</p>" + interviewScheduleDTO.getInterviewLocation() + " at " + dateTimes[1] + ", " + dateTimes[0]
                    + "<p>Hr team will contact you as soon as possible</p>"
                    + "<p>Sincerely!</p>";

            message.setContent(htmlMsg, "text/html");

            helper.setTo(candidate.get().getEmail());

            helper.setSubject("[ Notice Interview Schedule] ");
            javaMailSender.send(message);
            return true;
        }
        throw new JobNotFoundException();
    }

    @Override
    public void sendEmailForgotPassword(String email, String resetPasswordLink) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");

        String content = "<p>Hello,</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><a href=\"" + resetPasswordLink + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";

        message.setContent(content, "text/html");

        helper.setTo(email);

        String subject = "Here's the link to reset your password";

        helper.setSubject(subject);

        javaMailSender.send(message);
    }

    @Override
    public void sendEmailFromCandidateToCompany(String emailCandidate, String name, String messageCandidate, Integer companyId) throws MessagingException, CompanyNotFoundException {

        Optional<Company> companyOptional = companyRepository.findByCompanyIdAndDeleteFlagIs(companyId, 1);

        if(companyOptional.isEmpty()){
            throw new CompanyNotFoundException();
        }
        String emailCompany = companyOptional.get().getEmail();

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");

        String content = "<p>Hello,</p>"
                + "<p>Candidate " + name + " sent you a message:</p>"
                + "<p>Message: " + messageCandidate + " </p>"
                + "<p>Please contact him/her soon.</p>"
                + "<br>"
                + "<p>Candidate's contact: </p>"
                + "<p>Name: " + name + " </p>"
                + "<p>Email: " + emailCandidate + " </p>"
                + "<p>Thanks!";

        message.setContent(content, "text/html");

        helper.setTo(emailCompany);

        String subject = "[Candidate Is Here!]";

        helper.setSubject(subject);

        javaMailSender.send(message);
    }

    @Override
    public void sendEmailFromCompanyToCandidate(String emailCompany, String name, String messageCompany, Integer candidateId) throws CandidateNotFoundException, MessagingException {

        Optional<User> candidatePOpt = userRepository.findByUserIdAndDeleteFlagIs(candidateId, 1);

        if(candidatePOpt.isEmpty()){
            throw new CandidateNotFoundException();
        }
        String emailCandidate = candidatePOpt.get().getEmail();

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");

        String content = "<p>Hello,</p>"
                + "<p>Company " + name + " sent you a message:</p>"
                + "<p>Message: " + messageCompany + " </p>"
                + "<p>Please contact them soon.</p>"
                + "<br>"
                + "<p>Company's contact: </p>"
                + "<p>Name: " + name + " </p>"
                + "<p>Email: " + emailCompany + " </p>"
                + "<p>Thanks!";

        message.setContent(content, "text/html");

        helper.setTo(emailCandidate);

        String subject = "[Company Is Here!]";

        helper.setSubject(subject);

        javaMailSender.send(message);
    }

    @Override
    public void sendEmailToUserAppliedJobOfCompany(String emailCompany, String name, String messageCompany, Integer userAppliedJobId) throws UserAppliedJobNotFoundException, MessagingException {

        Optional<UserAppliedJob> userAppliedJobOptional = userAppliedJobRepository.findByUserAppliedJobIdAndDeleteFlagIs(userAppliedJobId,1);

        if(userAppliedJobOptional.isEmpty()){
            throw new UserAppliedJobNotFoundException();
        }
        String emailCandidate = userAppliedJobOptional.get().getEmail();
        User candidate = userAppliedJobOptional.get().getUser();
        Job job = userAppliedJobOptional.get().getJob();
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");


        String content = "<p>Hello,"+candidate.getFullName()+"</p>"
                + "<p>Company " + name + " sent you a message:</p>"
                + "<p>Message: " + messageCompany + " </p>"
                + "<p>Please contact them soon.</p>"
                + "<br>"
                + "<p>Company's contact: </p>"
                + "<p>Name: " + name + " </p>"
                + "<p>Email: " + emailCompany + " </p>"
                + "<p>Thanks!";

        mimeMessage.setContent(content, "text/html");

        mimeMessageHelper.setTo(emailCandidate);

        String subject = "[About Your Application For "+job.getJobTitle()+" ]";

        mimeMessageHelper.setSubject(subject);

        javaMailSender.send(mimeMessage);
    }
}
