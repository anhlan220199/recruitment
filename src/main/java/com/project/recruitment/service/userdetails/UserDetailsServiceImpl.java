package com.project.recruitment.service.userdetails;

import com.project.recruitment.entity.User;
import com.project.recruitment.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsernameAndDeleteFlagIs(username, 1);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        return new org.springframework.security.core.userdetails.User(username, user.get().getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, user.get().getAuthorities(user.get().getRole()));
    }
}