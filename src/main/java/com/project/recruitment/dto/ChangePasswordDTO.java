package com.project.recruitment.dto;

import com.project.recruitment.constants.CommonConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ChangePasswordDTO {

    private int userId;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    @Size(min = 6)
    private String oldPassword;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    @Size(min = 6)
    private String newPassword;

    @NotNull(message = CommonConstant.ErrorMessage.NOTNULL)
    @Size(min = 6)
    private String confirmPassword;

    public int getUserId() {
        return userId;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
