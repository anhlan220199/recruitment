package com.project.recruitment.service;

import com.project.recruitment.dto.RegisterDTO;
import com.project.recruitment.exception.RegisterFailException;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public interface RegisterService {
    void saveNewUser(RegisterDTO registerDTO, String roleName) throws RegisterFailException, UnsupportedEncodingException, MessagingException;

    boolean confirmationAccount(String token);
}
