package com.project.recruitment.controller;

import com.project.recruitment.constants.CommonConstant;
import com.project.recruitment.dto.RegisterDTO;
import com.project.recruitment.exception.RegisterFailException;
import com.project.recruitment.service.CompanyService;
import com.project.recruitment.service.RegisterService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private final RegisterService registerService;
    private final CompanyService companyService;

    public RegisterController(RegisterService registerService, CompanyService companyService) {
        this.registerService = registerService;
        this.companyService = companyService;
    }

    @GetMapping("/candidate")
    public String toCandidate(Model model) {
        model.addAttribute("newUser", new RegisterDTO());
        return "login/register-candidate";
    }

    @GetMapping("/company")
    public String toCompany(Model model) {
        model.addAttribute("newUser", new RegisterDTO());
        model.addAttribute("lstCompany", companyService.getAllCompany());
        return "login/register-company";
    }

    @PostMapping("/add")
    public String createUser(@ModelAttribute("newUser") RegisterDTO newUser, Model model) throws RegisterFailException, UnsupportedEncodingException, MessagingException {
        registerService.saveNewUser(newUser, CommonConstant.CANDIDATE);
        model.addAttribute("email", newUser.getEmail());
        return "login/after-registration";
    }

    @PostMapping("/addHr")
    public String createHr(@ModelAttribute("newUser") RegisterDTO newUser, Model model) throws RegisterFailException, UnsupportedEncodingException, MessagingException {
        registerService.saveNewUser(newUser, CommonConstant.ADMIN_COMPANY);
        model.addAttribute("email", newUser.getEmail());
        return "login/after-registration";
    }

    @GetMapping("/confirmAccount")
    public String ConfirmUserAccount(Model model, @RequestParam(value = "confirmationToken", required = true) String confirmationToken) {
        boolean isEmailConfirmed = registerService.confirmationAccount(confirmationToken);
        if (isEmailConfirmed == true) {
            model.addAttribute("isConfirmSuccess", true);
        } else {
            model.addAttribute("isConfirmSuccess", false);
        }
        return "login/register-success";
    }

}
