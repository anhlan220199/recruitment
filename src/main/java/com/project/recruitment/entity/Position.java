package com.project.recruitment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Position extends AbstractAuditingEntity {
    private int positionId;
    private String name;
    private int deleteFlag;
    private Set<Job> listJob;

    @Id
    @Column(name = "position_id")
    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "delete_flag")
    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @OneToMany(mappedBy = "position")
    public Set<Job> getListJob() {
        return listJob;
    }

    public void setListJob(Set<Job> listJob) {
        this.listJob = listJob;
    }
}
