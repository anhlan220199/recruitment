package com.project.recruitment.exception;

public class JobNotFoundException extends NotFoundException {

    private static final long serialVersionUID = -5574540267754376826L;
}
