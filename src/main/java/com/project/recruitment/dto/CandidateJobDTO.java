package com.project.recruitment.dto;

import org.springframework.web.multipart.MultipartFile;

public class CandidateJobDTO {
    private Integer id;

    //	@Pattern(regexp = CommonConstant.EMAIL_PATTERN, message = "Email wrong format !")
    private String email;

    private String message;

    private MultipartFile multipartFile;
    private int jobId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }
}
