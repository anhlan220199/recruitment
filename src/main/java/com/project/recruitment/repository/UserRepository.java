package com.project.recruitment.repository;

import com.project.recruitment.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "select * from `user` where username = ?1 or email=?2 and delete_flag =1", nativeQuery = true)
    List<User> findByUsernameOrEmail(String username, String email);

    Optional<User> findByEmailAndDeleteFlagIs(String email, Integer deleteFlag);

    Optional<User> findByUsernameAndDeleteFlagIs(String username, Integer i);

    Page<User> findAllByIsCandidateIsAndDeleteFlagIs(Integer isCandidate, Integer deleteFlag, Pageable pageable);

    List<User> findAllByIsCandidateIsAndDeleteFlagIs(Integer isCandidate, Integer deleteFlag);

    @Query(value = "SELECT * from `user` WHERE LOWER(full_name) LIKE %?1% AND delete_flag=1", nativeQuery = true)
    Page<User> relativeSearching(String keyword, Pageable pageable);

    Optional<User> findByUserIdAndIsCandidateIsAndDeleteFlagIs(Integer userId, Integer isCandidate, Integer i);

    Optional<User> findByUserIdAndDeleteFlagIs(Integer userId, Integer i);

    Optional<User> findByUsername(String username);

    @Query(value = "SELECT * from `user` WHERE LOWER(expertise) LIKE %?1% AND delete_flag=1", nativeQuery = true)
    Page<User> relativeSearchingByExpertise(String expertise, Pageable pageable);

    @Query(value = "SELECT * from `user` WHERE LOWER(professional_skill) LIKE %?1% AND delete_flag=1", nativeQuery = true)
    Page<User> relativeSearchingByProfessionalSkill(String professionalSkill, Pageable pageable);

    @Query(value = "SELECT * from `user` WHERE LOWER(experience) LIKE %?1% AND delete_flag=1", nativeQuery = true)
    Page<User> relativeSearchingByExperienceYear(String experienceYear, Pageable pageable);

    @Query(value = "SELECT * from `user` WHERE LOWER(expertise) LIKE %?1% AND LOWER(experience) LIKE %?2% AND delete_flag=1", nativeQuery = true)
    Page<User> relativeSearchingByExpertiseAndExperienceYear(String expertise,String experienceYear, Pageable pageable);

    @Query(value = "SELECT * from `user` WHERE LOWER(expertise) LIKE %?1% AND LOWER(professional_skill) LIKE %?2% AND LOWER(experience) LIKE %?3% AND delete_flag=1", nativeQuery = true)
    Page<User> relativeSearchingByExpertiseAndProfessionalSkillAndExperienceYear(String expertise,String professionalSkill,String experienceYear, Pageable pageable);

    @Query(value = "SELECT DISTINCT experience from `user` where delete_flag=1",  nativeQuery = true)
    List<String> findDistinctExperienceYear();

    Optional<User> findByUsernameAndIsCandidateAndDeleteFlagIs(String username, int isCandidate, int deleteFlag);

}
