package com.project.recruitment.repository;

import com.project.recruitment.entity.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenderRepository extends JpaRepository<Gender, Integer> {

    Gender findByName(String name);
}
