package com.project.recruitment.repository;

import com.project.recruitment.entity.Company;
import com.project.recruitment.entity.Review;
import com.project.recruitment.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface CompanyReviewRepository extends JpaRepository<Review,Integer> {
    Optional<Review> findByUserAndCompanyAndDeleteFlagIs(User user, Company company,Integer deleteFlag);
    Page<Review> findAllByCompanyAndDeleteFlagIs(Company company, int deleteFlag, Pageable pageable);
}
