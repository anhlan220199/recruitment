package com.project.recruitment.service.impl;

import com.project.recruitment.dto.JobLevelDTO;
import com.project.recruitment.dto.PositionDTO;
import com.project.recruitment.dto.SkillDTO;
import com.project.recruitment.entity.*;
import com.project.recruitment.helper.Helper;
import com.project.recruitment.repository.*;
import com.project.recruitment.service.HelperService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class HelperServiceImpl implements HelperService {

    private final JobTypeRepository jobTypeRepository;
    private final CurrencyRepository currencyRepository;
    private final GenderRepository genderRepository;
    private final JobLevelRepository jobLevelRepository;
    private final PositionRepository positionRepository;
    private final SkillRepository skillRepository;
    private final Helper helper;
    ModelMapper modelMapper = new ModelMapper();

    public HelperServiceImpl(JobTypeRepository jobTypeRepository, CurrencyRepository currencyRepository, GenderRepository genderRepository, JobLevelRepository jobLevelRepository, PositionRepository positionRepository, SkillRepository skillRepository, Helper helper) {
        this.jobTypeRepository = jobTypeRepository;
        this.currencyRepository = currencyRepository;
        this.genderRepository = genderRepository;
        this.jobLevelRepository = jobLevelRepository;
        this.positionRepository = positionRepository;
        this.skillRepository = skillRepository;
        this.helper = helper;
    }

    @Override
    public List<JobType> getAllJobType() {
        List<JobType> lstJobType = jobTypeRepository.findAll();
        return lstJobType;
    }

    @Override
    public List<Currency> getAllCurrency() {
        List<Currency> lstCurrency = currencyRepository.findAll();
        return lstCurrency;
    }

    @Override
    public List<Gender> getAllGender() {
        List<Gender> lstGender = genderRepository.findAll();
        return lstGender;
    }

    @Override
    public List<JobLevelDTO> getAllJobLevel() {
        List<JobLevel> lstJobLevel = jobLevelRepository.findAll();
        List<JobLevelDTO> lstJobLevelDTO = lstJobLevel.stream()
                .map(jobLevel -> modelMapper.map(helper.mapToJobLevelDTO(jobLevel), JobLevelDTO.class))
                .collect(Collectors.toList());
        return lstJobLevelDTO;
    }

    @Override
    public List<PositionDTO> getAllPosition() {
        List<Position> lstPosition = positionRepository.findAll();
        List<PositionDTO> lstPositionDTO = lstPosition.stream()
                .map(position -> modelMapper.map(helper.mapToPositionDTO(position), PositionDTO.class))
                .collect(Collectors.toList());
        return lstPositionDTO;
    }

    @Override
    public List<SkillDTO> getAllSkill() {
        List<Skill> lstSkill = skillRepository.findAll();
        List<SkillDTO> lstSkillDTO = lstSkill.stream()
                .map(skill -> modelMapper.map(helper.mapToSkillDTO(skill), SkillDTO.class))
                .collect(Collectors.toList());
        return lstSkillDTO;
    }

    @Override
    public float getTotalRating(Company company){
        Set<Review> reviewList = company.getListReview();
        int sum =0,count =0;
        float totalRating=0;
        if(reviewList.size()!=0){
            for (Review review: reviewList) {
                sum+=review.getRating();
                count++;
            }
            totalRating=sum/count;
        }
        return totalRating;
    }
}
