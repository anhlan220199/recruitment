package com.project.recruitment.service;

import com.project.recruitment.dto.*;
import com.project.recruitment.entity.User;
import com.project.recruitment.entity.UserAppliedJob;
import com.project.recruitment.exception.CandidateNotFoundException;
import com.project.recruitment.exception.ListJobNotFoundException;
import com.project.recruitment.exception.ListUserNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface CandidateService {

    Page<ListCandidateDTO> listAll(String keyword, String pageNumber)
            throws IllegalArgumentException, ListUserNotFoundException, ListJobNotFoundException;

    CandidateDetailDTO getCandidateDetailById(Integer userId) throws CandidateNotFoundException;

    Optional<User> findUserByUsername(String username) throws CandidateNotFoundException;

    User getCurrentUser() throws CandidateNotFoundException;

    UpdateCandidateDTO getCandidateToUpdateForm() throws CandidateNotFoundException;

    ChangePasswordDTO getCandidateToChangePasswordForm() throws CandidateNotFoundException;

    boolean editProfile(UpdateCandidateDTO updateCandidateDTO) throws CandidateNotFoundException;

    BindingResult changePassword(ChangePasswordDTO changePasswordDTO, BindingResult bindingResult) throws CandidateNotFoundException;

    void deleteCandidate (String id) throws CandidateNotFoundException;

    Integer countCandidate();

    List<UserAppliedJob> getListUserAppliedJobByCurrentCandidate() throws CandidateNotFoundException;

    UpdateResumeDTO getCandidateToAddResumeForm() throws CandidateNotFoundException;

    void addResume(UpdateResumeDTO updateResumeDTO, String fileName) throws CandidateNotFoundException;

    boolean saveCVFile(MultipartFile multipartFile, String fileName) throws IOException;

    boolean deleteProfile() throws CandidateNotFoundException;

    List<ListJobDTO> getListJobBookmark() throws CandidateNotFoundException;

    ResumeDetailDTO getResumeDetail() throws CandidateNotFoundException;

    // reset password:

    void updateResetPasswordToken(String token, String email) throws CandidateNotFoundException;

    User getByResetPasswordToken(String token);

    boolean updatePassword(User user, String newPassword);

    Page<ListCandidateDTO> filterCandidateByExpertiseAndSkillAndExperienceYear(String expertise, String skill,String experienceYear, String pageNumber);

    List<String> getListExperienceYear();

}
