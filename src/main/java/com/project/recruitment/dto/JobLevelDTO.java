package com.project.recruitment.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class JobLevelDTO {

    private int jobLevelId;

    public int getJobLevelId() {
        return jobLevelId;
    }

    public String getName() {
        return name;
    }

    private String name;

    public void setJobLevelId(int jobLevelId) {
        this.jobLevelId = jobLevelId;
    }

    public void setName(String name) {
        this.name = name;
    }
}
