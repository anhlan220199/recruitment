package com.project.recruitment.repository;


import com.project.recruitment.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Integer> {
    List<Skill> findAll();
    Optional<Skill>findByName(String skillName);
    Optional<Skill> findBySkillId(Integer id);
}
