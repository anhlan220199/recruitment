package com.project.recruitment.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class ListCompanyDTO {

    private int companyId;
    private String name;
    private String city;
    private String type;
    private int openJobNumber;

    public int getCompanyId() {
        return companyId;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getType() {
        return type;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOpenJobNumber() {
        return openJobNumber;
    }

    public void setOpenJobNumber(int openJobNumber) {
        this.openJobNumber = openJobNumber;
    }
}
