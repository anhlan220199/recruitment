package com.project.recruitment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UpdateResumeDTO {

    private int userId;
    private String cvFileName;
    private String cvFilePath;
    private MultipartFile multipartFile;
    private String aboutMe;
    private String expertise;
    private String experience;
    private String qualification;
    private String workExperience;
    private String educationBackground;
    private String specialQualification;
    private String professionalSkill;
    private String jobType;

    public int getUserId() {
        return userId;
    }

    public String getCvFileName() {
        return cvFileName;
    }

    public String getCvFilePath() {
        return cvFilePath;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public String getExpertise() {
        return expertise;
    }

    public String getExperience() {
        return experience;
    }

    public String getQualification() {
        return qualification;
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public String getEducationBackground() {
        return educationBackground;
    }

    public String getSpecialQualification() {
        return specialQualification;
    }

    public String getProfessionalSkill() {
        return professionalSkill;
    }

    public String getJobType() {
        return jobType;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setCvFileName(String cvFileName) {
        this.cvFileName = cvFileName;
    }

    public void setCvFilePath(String cvFilePath) {
        this.cvFilePath = cvFilePath;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public void setEducationBackground(String educationBackground) {
        this.educationBackground = educationBackground;
    }

    public void setSpecialQualification(String specialQualification) {
        this.specialQualification = specialQualification;
    }

    public void setProfessionalSkill(String professionalSkill) {
        this.professionalSkill = professionalSkill;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
