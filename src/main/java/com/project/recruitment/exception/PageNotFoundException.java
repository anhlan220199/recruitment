package com.project.recruitment.exception;

public class PageNotFoundException extends NotFoundException {

    private static final long serialVersionUID = -5574540267754376823L;
}
